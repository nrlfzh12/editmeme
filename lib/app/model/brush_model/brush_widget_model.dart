import 'package:codingtest/app/helpers/canvas_helper.dart';

class BrushWidgetModel {
  BrushWidgetModel({
    this.type = CanvasItemType.BRUSH,
    this.key,
    required this.index,
    required this.url,
    required this.width,
    required this.height,
    required this.x_axis,
    required this.y_axis,
    required this.scale,
    required this.rotation,
    this.top_edge,
    this.bottom_edge,
    this.left_edge,
    this.right_edge,
    this.default_height,
    this.default_width,
    // required this.drawpoint,
    // required this.base64,
  });
  int index;
  String? key;
  // List drawpoint;
  // String base64;
  String type;
  String url;
  double width;
  double height;
  double x_axis;
  double y_axis;
  double scale;
  double rotation;

  //*untuk kebutuhan canvas
  double? top_edge;
  double? bottom_edge;
  double? left_edge;
  double? right_edge;
  double? default_height;
  double? default_width;

  factory BrushWidgetModel.fromJson(Map<String, dynamic> json) =>
      BrushWidgetModel(
        index: json['index'],
        type: json['type'],
        url: json['url'],
        key: json['key'] ?? '',
        width: json['width'].toDouble(),
        height: json['height'].toDouble(),
        x_axis: json['x_axis'].toDouble(),
        y_axis: json['y_axis'].toDouble(),
        scale: json['scale'].toDouble(),
        rotation: json['angle_rotation'].toDouble(),
        bottom_edge: json.containsKey('bottom_edge')
            ? json['bottom_edge'].toDouble() ?? 0.0
            : 0.0,
        left_edge: json['left_edge'].toDouble() ?? 0.0,
        right_edge: json['right_edge'].toDouble() ?? 0.0,
        default_height: json.containsKey('default_height')
            ? json['default_height'].toDouble() ?? 0.0
            : 0.0,
        default_width: json.containsKey('default_width')
            ? json['default_width'].toDouble() ?? 0.0
            : 0.0,
        // drawpoint: json['drawpoint'],
        // base64: json['base64'],
      );

  Map<String, dynamic> toJson() => {
        'index': index,
        'type': type,
        "url": url,
        'key': key ?? '',
        "width": width,
        "height": height,
        "x_axis": x_axis,
        "y_axis": y_axis,
        'scale': scale,
        'angle_rotation': rotation,
        // 'created_at': createdAt,
        'top_edge': top_edge,
        'bottom_edge': bottom_edge,
        'left_edge': left_edge,
        'right_edge': right_edge,
        'default_height': default_height,
        'default_width': default_width,
        // 'drawpoint': drawpoint,
        // 'base64': base64,
      };
}
