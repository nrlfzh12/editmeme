class CommentCanvasArgs {
  bool? isPost = true;
  final String? postId;
  final String? userId;

  CommentCanvasArgs({this.userId, this.isPost, required this.postId});
}
