import 'package:codingtest/app/helpers/text_align_helper.dart';

class CanvasTextWidgetModel {
  CanvasTextWidgetModel({
    required this.longText,
    required this.fontSize,
    this.lineHeight = 1,
    this.fontFamily = 'Montserrat',
    this.isBold = false,
    this.isItalic = false,
    this.isUnderline = false,
    this.textAlign = TextAlignHelper.START,
    this.color = 0xFF191508,
    this.isFirstAdd = true,
    this.isCut = false,
    this.trim_start,
  });

  String longText;
  double? fontSize = 20;
  double lineHeight;
  String? fontFamily;
  bool isBold;
  bool isItalic;
  bool isUnderline;
  String textAlign;
  int color;
  bool isFirstAdd;
  bool isCut;
  int? trim_start;

  factory CanvasTextWidgetModel.fromJson(Map<String, dynamic> json) =>
      CanvasTextWidgetModel(
        longText: json['long_text'],
        fontSize: json['fontSize'],
        lineHeight: json['lineHeight'],
        fontFamily: json['fontFamily'],
        isBold: json['isBold'],
        isItalic: json['isItalic'],
        isUnderline: json['isUnderline'],
        textAlign: json['textAlign'],
        color: json['color'],
        isFirstAdd: json['isFirstAdd'],
        isCut: json['isCut'],
        trim_start: json['isCut'] ? json['trim_start'] : 0,
      );

  Map<String, dynamic> toJson() => {
        'long_text': longText,
        'fontSize': fontSize,
        'lineHeight': lineHeight,
        'fontFamily': fontFamily,
        'isBold': isBold,
        'isItalic': isItalic,
        'isUnderline': isUnderline,
        'textAlign': textAlign,
        'color': color,
        'isFirstAdd': isFirstAdd,
        'isCut': isCut,
        'trim_start': trim_start ?? 0,
      };
}
