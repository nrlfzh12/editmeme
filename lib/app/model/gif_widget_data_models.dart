class GifWidgetDataModels {
  GifWidgetDataModels({
    required this.url,
    this.canPopUp = true,
  });
  String url;
  bool canPopUp;

  factory GifWidgetDataModels.fromJson(Map<String, dynamic> json) => GifWidgetDataModels(
        url: json['url'],
        canPopUp: json['can_pop_up'],
      );

  Map<String, dynamic> toJson() => {
        'url': url,
        'can_pop_up': canPopUp,
      };
}
