class BackgroundWidgetModel {
  BackgroundWidgetModel({
    required this.color,
    required this.url,
  });
  String color;
  String url;

  factory BackgroundWidgetModel.fromJson(Map<String, dynamic> json) => BackgroundWidgetModel(
        color: json['color'] ?? '',
        url: json['url'] ?? '',
      );

  toJson() => {
        'color': color,
        'url': url,
      };
}
