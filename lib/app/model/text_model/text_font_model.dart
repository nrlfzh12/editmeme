class TextFontModel {
  TextFontModel({
    required this.fontFamily,
    required this.fontSize,
    required this.fontWeight,
    // required this.color,
    required this.fontType,
    // required this.justify,
    required this.lineHeight,
    required this.isBold,
    required this.isItalic,
    required this.isUnderline,
    required this.textAlign,
    required this.isCut,
    this.cutStart,
  });

  String fontFamily;
  double fontSize;
  int fontWeight;
  // int color;
  String fontType;
  // String justify;
  double lineHeight;
  bool isBold;
  bool isItalic;
  bool isUnderline;
  String textAlign;
  bool isCut;
  int? cutStart;

  factory TextFontModel.fromJson(Map<String, dynamic> json) => TextFontModel(
        fontFamily: json['font_family'],
        fontSize: json['font_size'].toDouble(),
        fontWeight: json['font_weight'],
        // // color: json['color'],
        fontType: json['font_type'],
        // justify: json['justify'],
        lineHeight: json['line_height'].toDouble(),
        isBold: json['isBold'] ?? false,
        isItalic: json['isItalic'] ?? false,
        isUnderline: json['isUnderline'] ?? false,
        textAlign: json['textAlign'] ?? '',
        isCut: json['isCut'] ?? false,
        cutStart: json['isCut'] ?? false ? json['cutStart'] : 0,
      );

  Map<String, dynamic> toJson() => {
        "font_family": fontFamily,
        "font_size": fontSize,
        "font_weight": fontWeight,
        // // "color": color,
        "font_type": fontType,
        // "justify": justify,
        "line_height": lineHeight,
        'isBold': isBold,
        'isItalic': isItalic,
        'isUnderline': isUnderline,
        'textAlign': textAlign,
        'isCut': isCut,
        'cutStart': cutStart ?? 0,
      };
}
