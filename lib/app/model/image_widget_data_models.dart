class ImageWidgetDataModels {
  ImageWidgetDataModels({
    required this.path,
    this.canPopUp = true,
  });

  String path;
  bool canPopUp;

  factory ImageWidgetDataModels.fromJson(Map<String, dynamic> json) => ImageWidgetDataModels(
        path: json['path'],
        canPopUp: json['can_pop_up'],
      );

  Map<String, dynamic> toJson() => {
        'path': path,
        'can_pop_up': canPopUp,
      };
}
