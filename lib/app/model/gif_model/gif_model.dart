class GifModel {
  GifModel({
    required this.id,
    required this.url,
    required this.index,
    required this.type,
    required this.width,
    required this.height,
    this.x_axis = 0.0,
    this.y_axis = 0.0,
    this.scale = 0.0,
    this.rotation = 0.0,
  });

  String id;
  String url;
  String type;
  dynamic height;
  dynamic width;
  int index;
  double x_axis;
  double y_axis;
  double scale;
  double rotation;

  factory GifModel.fromJson(Map<String, dynamic> json) => GifModel(
      id: json['id'] ?? '',
      url: json['url'] ?? '',
      type: json['type'],
      index: json['index'],
      height: json['height'],
      width: json['width'],
      x_axis: json['x_axis'].toDouble(),
      y_axis: json['y_axis'].toDouble(),
      scale: json['scale'].toDouble(),
      rotation: json['angle_rotation'].toDouble());

  Map<String, dynamic> toJson() => {
        'id': id,
        'url': url,
        'index': index,
        'type': type,
        'width': width,
        'height': height,
        'x_axis': x_axis,
        'y_axis': y_axis,
        'scale': scale,
        'angle_rotation': rotation,
      };
}
