class GifWidgetModel {
  GifWidgetModel({
    required this.url,
    required this.index,
    required this.type,
    required this.height,
    required this.width,
    this.x_axis = 0.0,
    this.y_axis = 0.0,
    this.scale = 0.0,
    this.rotation = 0.0,
    this.canPopUp = true,
    this.top_edge,
    this.bottom_edge,
    this.left_edge,
    this.right_edge,
  });

  String url;
  int index;
  String type;
  dynamic height;
  dynamic width;
  double x_axis;
  double y_axis;
  double scale;
  double rotation;
  bool canPopUp;

  //*untuk kebutuhan canvas
  double? top_edge;
  double? bottom_edge;
  double? left_edge;
  double? right_edge;
  double? default_height;
  double? default_width;

  factory GifWidgetModel.fromJson(Map<String, dynamic> json) => GifWidgetModel(
        index: json['index'],
        url: json['url'] ?? '',
        type: json['type'],
        height: json['height'].toDouble(),
        width: json['width'].toDouble(),
        top_edge: json['top_edge'] ?? 0.0,
        bottom_edge: json.containsKey('bottom_edge')
            ? json['bottom_edge'].toDouble() ?? 0.0
            : 0.0,
        // left_edge: json['left_edge'] == null
        //     ? 0.0
        //     : json['left_edge'].toDouble() ?? 0.0,
        // right_edge: json['right_edge'] == null
        //     ? 0.0
        //     : json['right_edge'].toDouble() ?? 0.0,
        // x_axis: json['x_axis'] == null ? 0.0 : json['x_axis'].toDouble(),
        // y_axis: json['y_axis'] == null ? 0.0 : json['y_axis'].toDouble(),
        // scale: json['scale'] == null ? 0.0 : json['scale'].toDouble(),
        // rotation: json['angle_rotation'] == null
        //     ? 0.0
        //     : json['angle_rotation'].toDouble(),
        // canPopUp: json['can_pop_up'],
      );

  Map<String, dynamic> toJson() => {
        'index': index,
        'url': url,
        'type': type,
        'height': height,
        'width': width,
        'top_edge': top_edge,
        'bottom_edge': bottom_edge,
        'left_edge': left_edge,
        'right_edge': right_edge,
        'x_axis': x_axis,
        'y_axis': y_axis,
        'scale': scale,
        'angle_rotation': rotation,
        'can_pop_up': canPopUp,
      };
}
