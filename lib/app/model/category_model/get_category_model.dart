// To parse this JSON data, do
//
//     final getCategoryModel = getCategoryModelFromJson(jsonString);

import 'dart:convert';

class GetCategoryModel {
  GetCategoryModel({
    required this.id,
    required this.name,
    required this.userId,
    required this.createdAt,
    required this.v,
  });

  String id;
  String name;
  String userId;
  DateTime createdAt;
  int v;

  GetCategoryModel copyWith({
    String? id,
    String? name,
    String? userId,
    DateTime? createdAt,
    int? v,
  }) =>
      GetCategoryModel(
        id: id ?? this.id,
        name: name ?? this.name,
        userId: userId ?? this.userId,
        createdAt: createdAt ?? this.createdAt,
        v: v ?? this.v,
      );

  factory GetCategoryModel.fromRawJson(String str) =>
      GetCategoryModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GetCategoryModel.fromJson(Map<String, dynamic> json) =>
      GetCategoryModel(
        id: json["_id"],
        name: json["name"],
        userId: json["user_id"],
        createdAt: DateTime.parse(json["created_at"]),
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "user_id": userId,
        "created_at": createdAt.toIso8601String(),
        "__v": v,
      };
}
