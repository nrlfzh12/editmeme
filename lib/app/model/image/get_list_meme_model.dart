// To parse this JSON data, do
//
//     final getListMeme = getListMemeFromJson(jsonString);

import 'dart:convert';

class GetListMeme {
  GetListMeme({
    required this.success,
    required this.data,
  });

  bool success;
  Data data;

  GetListMeme copyWith({
    bool? success,
    required Data data,
  }) =>
      GetListMeme(
        success: success ?? this.success,
        data: data ?? this.data,
      );

  factory GetListMeme.fromRawJson(String str) =>
      GetListMeme.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GetListMeme.fromJson(Map<String, dynamic> json) => GetListMeme(
        success: json["success"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    required this.memes,
  });

  List<Meme> memes;

  Data copyWith({
    List<Meme>? memes,
  }) =>
      Data(
        memes: memes ?? this.memes,
      );

  factory Data.fromRawJson(String str) => Data.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        memes: List<Meme>.from(json["memes"].map((x) => Meme.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "memes": List<dynamic>.from(memes.map((x) => x.toJson())),
      };
}

class Meme {
  Meme({
    required this.id,
    required this.name,
    required this.url,
    required this.width,
    required this.height,
    required this.boxCount,
    required this.captions,
  });

  String id;
  String name;
  String url;
  int width;
  int height;
  int boxCount;
  int captions;

  Meme copyWith({
    String? id,
    String? name,
    String? url,
    int? width,
    int? height,
    int? boxCount,
    int? captions,
  }) =>
      Meme(
        id: id ?? this.id,
        name: name ?? this.name,
        url: url ?? this.url,
        width: width ?? this.width,
        height: height ?? this.height,
        boxCount: boxCount ?? this.boxCount,
        captions: captions ?? this.captions,
      );

  factory Meme.fromRawJson(String str) => Meme.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Meme.fromJson(Map<String, dynamic> json) => Meme(
        id: json["id"],
        name: json["name"],
        url: json["url"],
        width: json["width"],
        height: json["height"],
        boxCount: json["box_count"],
        captions: json["captions"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "url": url,
        "width": width,
        "height": height,
        "box_count": boxCount,
        "captions": captions,
      };
}
