import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/canvas_controller.dart';

class BaseCanvasItem extends StatefulWidget {
  const BaseCanvasItem({
    super.key,
    required this.index,
    // required this.imageFile,
    required this.child,
    required this.insertDone,
    this.useLoading = true,
  });
  final int index;
  // final File imageFile;
  final Widget child;
  final bool useLoading;
  final bool insertDone;

  @override
  State<BaseCanvasItem> createState() => _BaseCanvasItemState();
}

class _BaseCanvasItemState extends State<BaseCanvasItem> {
  var controller = Get.find<CanvasController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      if (widget.insertDone) {
        return;
      }
      //! remove dialog
      // controller.inserItemAtribute(index: widget.index);
      if (widget.useLoading) {
        // dialogLoading();
        await Future.delayed(Duration(milliseconds: 500));
        controller.inserItemAtribute(index: widget.index);
        // Get.back();
        return;
      } else {
        controller.inserItemAtribute(index: widget.index);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: controller.listGlobalKey[widget.index],
      child: widget.child,
    );
  }
}
