import 'package:codingtest/app/modules/canvas/components/default_text.dart';
import 'package:codingtest/app/modules/canvas/controllers/canvas_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import '../../../config/constants.dart';

class TextEditingLineHeight extends GetView<CanvasController> {
  const TextEditingLineHeight({
    super.key,
    required this.index,
  });
  final int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          const SizedBox(height: 15),
          DefText(
            'Line Height',
          ).semilarge,
          const SizedBox(height: 10),
          DefText(
            '${controller.widgetsData[index]['data']['lineHeight']} px',
            fontWeight: FontWeight.bold,
          ).semilarge,
          const SizedBox(height: 15),
          Slider(
            value: controller.widgetsData[index]['data']['lineHeight'],
            onChanged: (value) {
              controller.widgetsData[index]['data']['lineHeight'] =
                  value.toPrecision(2);
              controller.widgetsData.refresh();
            },
            min: 1.0,
            max: 4.0,
            thumbColor: kBgBlack,
            activeColor: kBgBlack,
            inactiveColor: kInactiveColor.withOpacity(
              0.4,
            ),
          ),
        ],
      ),
    );
  }
}
