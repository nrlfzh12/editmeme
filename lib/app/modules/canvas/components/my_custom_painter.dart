import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../model/canvas_model/draw_point.dart';

class MyCustomPainter extends CustomPainter {
  MyCustomPainter({
    required this.drawPoints,
  });
  // final List<DrawPoint?> drawPoints;
  final List drawPoints;
  List<Offset> offsetsList = [];

  @override
  void paint(Canvas canvas, Size size) async {
    List<DrawPoint?> drawPoints = [];
    for (var i = 0; i < drawPoints.length; i++) {
      if (drawPoints[i] == null) {
        drawPoints.add(null);
      } else {
        DrawPoint? dp = drawPoints[i];
        drawPoints.add(dp);
      }
      // _drawPoints.add();
      // logKey('$i', drawPoints[i]);
    }
    var paint = Paint();

    canvas.saveLayer(
        Rect.fromLTWH(0, 0, Get.width + 80, Get.height + 65), Paint());
    for (var i = 0; i < drawPoints.length - 1; i++) {
      if (drawPoints[i] != null && drawPoints[i + 1] != null) {
        if (!drawPoints[i]!.isDraw) {
          canvas.drawLine(
            Offset(drawPoints[i]!.dx, drawPoints[i]!.dy),
            Offset(drawPoints[i + 1]!.dx, drawPoints[i + 1]!.dy),
            Paint()
              ..color = Colors.brown
              ..strokeWidth = drawPoints[i]!.stroke
              ..blendMode = BlendMode.clear,
          );
        } else {
          PaintingStyle style = PaintingStyle.fill;
          if (drawPoints[i]!.style == 'stroke') {
            style = PaintingStyle.stroke;
          }
          paint.blendMode = BlendMode.srcOver;
          paint.isAntiAlias = true;
          paint.style = style;
          paint.strokeWidth = drawPoints[i]!.stroke;
          paint.color = Color(drawPoints[i]!.color);
          Path path = Path();
          path.lineTo(drawPoints[i]!.dx, drawPoints[i + 1]!.dx);
          // path..addPath(path, Offset(_drawPoints[i]!.dx, _drawPoints[i]!.dy));
          // path..addPath(path, Offset(_drawPoints[i + 1]!.dx, _drawPoints[i + 1]!.dy));
          // canvas.drawPath(path, _paint);

          canvas.drawLine(
            Offset(drawPoints[i]!.dx, drawPoints[i]!.dy),
            Offset(drawPoints[i + 1]!.dx, drawPoints[i + 1]!.dy),
            paint,
          );

          if (drawPoints[i]!.stroke >= 3) {
            canvas.drawCircle(Offset(drawPoints[i]!.dx, drawPoints[i]!.dy),
                (drawPoints[i]!.stroke / 20), paint);
          } else if (drawPoints[i]!.stroke >= 10) {
            canvas.drawCircle(Offset(drawPoints[i]!.dx, drawPoints[i]!.dy),
                (drawPoints[i]!.stroke / 30), paint);
          }
        }
      }
      // else if (_drawPoints[i] != null && _drawPoints[i + 1] == null) {
      //   var _paint = Paint();
      //   PaintingStyle _style = PaintingStyle.fill;
      //   if (_drawPoints[i]!.style == 'stroke') {
      //     _style = PaintingStyle.stroke;
      //   }
      //   _paint.style = _style;
      //   _paint.strokeWidth = _drawPoints[i]!.stroke;
      //   _paint.color = Color(_drawPoints[i]!.color);
      //   offsetsList.clear();
      //   offsetsList.add(
      //     Offset(_drawPoints[i]!.dx, _drawPoints[i]!.dy),
      //   );
      //   canvas._drawPoints(
      //     PointMode.points,
      //     offsetsList,
      //     _paint,
      //   );
      // }
    }
    canvas.restore();
    // logKey('restore');
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
