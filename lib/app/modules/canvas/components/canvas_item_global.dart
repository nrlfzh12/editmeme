import 'dart:io';

import 'package:codingtest/app/helpers/canvas_helper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:codingtest/app/helpers/text_align_helper.dart';
import 'package:codingtest/app/model/brush_model/brush_widget_model.dart';
import 'package:codingtest/app/model/gif_model/gif_widget_model.dart';
import 'package:codingtest/app/model/image_model/image_widget_model.dart';
import 'package:codingtest/app/model/text_model/text_widget_models.dart';
import 'package:codingtest/app/modules/canvas/components/default_placeholder.dart';
import 'package:codingtest/app/utils/function_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:readmore/readmore.dart';

class CanvasItemGlobal extends StatelessWidget {
  const CanvasItemGlobal({
    super.key,
    required this.data,
    this.isLocal = false,
    // required this.index,
    // required this.indexCanvas,
  });
  final Map<String, dynamic> data;
  final bool isLocal;

  @override
  Widget build(BuildContext context) {
    // logKey('taipu', data['type']);
    // logKey('taipu data', data);
    if (data['type'] == CanvasItemType.IMAGE) {
      var temp = ImageWidgeModel.fromJson(data);
      if (isLocal) {
        File imageFile = File.fromUri(Uri.parse(temp.url));
        return Container(
          constraints: BoxConstraints(
            maxHeight: 400,
            maxWidth: Get.width - 100,
            // minHeight: 400,
            // minWidth: Get.width - 100,
          ),
          child: Image.file(
            imageFile,
          ),
        );
      } else {
        return Container(
          constraints: const BoxConstraints(maxHeight: 400),
          child: CachedNetworkImage(
              imageUrl: temp.url,
              placeholder: (context, url) {
                // return loading();
                return const DefaultPlaceholder(
                  height: 100,
                  width: 100,
                );
              }),
        );
      }
    }

    if (data['type'] == CanvasItemType.TEXT) {
      var dataText = TextWidgetModels.fromJson(data);
      // logKey('dataText', dataText.toJson());
      try {
        var textStyle = GoogleFonts.getFont(
          dataText.font.fontFamily,
          textStyle: TextStyle(
            fontSize: dataText.font.fontSize,
            fontWeight:
                dataText.font.isBold ? FontWeight.bold : FontWeight.normal,
            fontStyle:
                dataText.font.isItalic ? FontStyle.italic : FontStyle.normal,
            decoration: dataText.font.isUnderline
                ? TextDecoration.underline
                : TextDecoration.none,
            height: dataText.font.lineHeight,
            // color: Color(dataText.font.color),
          ),
        );
        var textAlign = TextAlign.start;
        if (dataText.font.textAlign == TextAlignHelper.CENTER) {
          textAlign = TextAlign.center;
        } else if (dataText.font.textAlign == TextAlignHelper.LEFT) {
          textAlign = TextAlign.left;
        } else if (dataText.font.textAlign == TextAlignHelper.RIGHT) {
          textAlign = TextAlign.right;
        } else if (dataText.font.textAlign == TextAlignHelper.END) {
          textAlign = TextAlign.end;
        } else if (dataText.font.textAlign == TextAlignHelper.CENTER) {
          textAlign = TextAlign.start;
        }
        return Container(
          constraints: BoxConstraints(
            minWidth: 100,
            maxWidth: Get.width,
            // minHeight: Get.height,
          ),
          child: dataText.font.isCut
              ? ReadMoreText(
                  textAlign: textAlign,
                  style: textStyle,
                  dataText.longText,
                  // trimLines: 10,
                  trimLength: dataText.font.cutStart ?? 0,
                  trimCollapsedText: 'Show more',
                  moreStyle: const TextStyle(
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.underline,
                    fontSize: 9.8,
                  ),
                  lessStyle: const TextStyle(
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.underline,
                    fontSize: 9.8,
                  ),
                  // trimMode: TrimMode.Length,
                  // trimLength: 4,
                )
              : Container(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Text(
                    dataText.longText,
                    style: textStyle,
                    textAlign: textAlign,
                  ),
                ),
        );
      } catch (e) {
        // showToast('error font not found');
        var textStyle = GoogleFonts.getFont(
          'Montserrat',
          // fontWeight: dataText.font.fontWeight,
          height: dataText.font.lineHeight,
          fontSize: dataText.font.fontSize,
        );
        return Container(
          // color: Colors.amber,
          child: Text(
            dataText.longText,
            style: textStyle,
          ),
        );
      }
    }

    if (data['type'] == CanvasItemType.GIF) {
      var dataGif = GifWidgetModel.fromJson(data);
      return Container(
        constraints: BoxConstraints(
          maxHeight: 400,
          maxWidth: Get.width - 100,
        ),
        child: CachedNetworkImage(
          imageUrl: dataGif.url,
          httpHeaders: const {'accepnt': 'image/*'},
          placeholder: (context, url) {
            return const DefaultPlaceholder(
              height: 150,
              width: 150,
            );
          },
        ),
      );
    }

    if (data['type'] == CanvasItemType.BRUSH) {
      var temp = BrushWidgetModel.fromJson(data);
      logKey('data brush global', temp.url);
      if (isLocal) {
        File brushImageFile = File(temp.url);
        return Image.file(brushImageFile);
      } else {
        CachedNetworkImage(
          imageUrl: temp.url,
        );
      }
      // return IgnorePointer(
      //   child: Container(
      //       // child: Image.memory(
      //       //   base64Decode(
      //       //     temp.base64,
      //       //   ),
      //       // ),
      //       ),
      // );
      // return Container(
      //   child: CustomPaint(
      //     painter: MyCustomPainter(
      //       drawPoints: temp.drawpoint.cast<DrawPoint?>(),
      //     ),
      //   ),
      // );
    }

    // Opaque targets can be hit by hit tests, causing them to both receive events within their bounds and prevent targets visually behind them from also receiving events.
    // Translucent targets both receive events within their bounds and permit targets visually behind them to also receive events.
    // if (controller.testTimeline[index]['canvas'][indexCanvas]['type'] == CanvasItemType.GIF) {
    //   var data = GifWidgetDataModels.fromJson(controller.testTimeline[index]['canvas'][indexCanvas]['data']);
    //   return Container(
    //     child: CachedNetworkImage(
    //       // imageUrl: controller.widgetsData[index]['data']['url'],
    //       imageUrl: data.url,
    //       httpHeaders: {'accept': 'image/*'},
    //       placeholder: (context, url) {
    //         return DefaultPlaceholder(
    //           height: 70,
    //           width: 70,
    //         );
    //       },
    //     ),
    //   );
    // }

    // if (controller.testTimeline[index]['canvas'][indexCanvas]['type'] == CanvasItemType.BRUSH_BASE) {
    //   return Container(
    //     child: CustomPaint(
    //       painter: MyCustomPainter(
    //         drawPoints: controller.testTimeline[index]['canvas'][indexCanvas]['data']['drawpoint'],
    //       ),
    //     ),
    //   );
    // }
    return Container();
  }
}
