import 'package:codingtest/app/model/canvas_widget_model.dart';
import 'package:codingtest/app/modules/canvas/controllers/canvas_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../config/constants.dart';

class RotateIcon extends GetView<CanvasController> {
  const RotateIcon({
    required this.index,
    required this.positionConstant,
    required this.scale,
    super.key,
  });
  final int index;
  final int positionConstant;
  final double scale;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        var widgetData =
            CanvasItemModels.fromJson(controller.widgetsData[index]).obs;
        return Transform.scale(
          scale: 1 / scale,
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onPanStart: (details) {
              var scaleImage = controller.widgetsData[index]['scale'];
              var centerOffset = Offset(
                (controller.widgetsData[index]['default_width'] *
                            scaleImage /
                            2 -
                        positionConstant) *
                    -1,
                controller.widgetsData[index]['default_height'] *
                        scaleImage /
                        2 -
                    positionConstant,
              );
              var touchPositionFromCenter =
                  details.localPosition - centerOffset;
              controller.angleDelta.value = controller.widgetsData[index]
                      ['rotation'] -
                  touchPositionFromCenter.direction;
            },
            onPanUpdate: (details) {
              var scaleImage = controller.widgetsData[index]['scale'];
              var centerOffset = Offset(
                (controller.widgetsData[index]['default_width'] *
                            scaleImage /
                            2 -
                        positionConstant) *
                    -1,
                controller.widgetsData[index]['default_height'] *
                        scaleImage /
                        2 -
                    positionConstant,
              );
              var touchPositionFromCenter =
                  details.localPosition - centerOffset;
              controller.angle.value = touchPositionFromCenter.direction +
                  controller.angleDelta.value;
            },
            onPanEnd: (details) {
              controller.widgetsData[index]['rotation'] =
                  controller.angle.value;
              controller.saveState();
              controller.widgetsData.refresh();
            },
            child: Material(
              elevation: 5,
              shape: const CircleBorder(),
              child: Container(
                alignment: Alignment.center,
                height: 35,
                width: 35,
                padding: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                  color: controller.widgetsData[index]['can_rotate']
                      ? kBgWhite
                      : kInactiveColor,
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  'assets/icons/rotate.png',
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
