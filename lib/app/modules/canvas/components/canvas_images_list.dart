import 'package:codingtest/app/config/constants.dart';
import 'package:codingtest/app/helpers/canvas_helper.dart';
import 'package:codingtest/app/modules/canvas/components/default_text.dart';
import 'package:codingtest/app/modules/canvas/controllers/canvas_controller.dart';
import 'package:codingtest/app/utils/function_utils.dart';
import 'package:cyclop/cyclop.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'color_item.dart';

class CanvasImagesList extends GetView<CanvasController> {
  const CanvasImagesList({
    super.key,
    this.isBackGround = false,
    // this.pageController,
    // required this.scrollController,
  });
  final bool isBackGround;
  // final PageController? pageController;
  // final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    PanelController panelController = isBackGround
        ? controller.panelBgController
        : controller.panelImageController;
    return Container(
      decoration: const BoxDecoration(
        color: kBgWhite,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          GestureDetector(
            onTap: () {
              if (panelController.panelPosition == 0) {
                panelController.animatePanelToPosition(1);
              } else {
                panelController.animatePanelToPosition(0);
              }
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              width: double.infinity,
              child: Center(
                child: Container(
                  height: 10,
                  width: 100,
                  decoration: const BoxDecoration(
                    color: kInactiveColor,
                    borderRadius: kDefaultBorderRadius10,
                  ),
                ),
              ),
            ),
          ),
          Visibility(
            visible: isBackGround,
            child: Row(
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: () async {
                      await controller.pageBgController.animateToPage(
                        0,
                        duration: kDefaultFastDuration,
                        curve: kDefaultCurve,
                      );
                      controller.bgPageViewIndex.value = 0;
                    },
                    child: GetX<CanvasController>(
                      init: CanvasController(),
                      builder: (ctrl) {
                        return Container(
                          color: kBgWhite,
                          padding: const EdgeInsets.all(10),
                          child: Center(
                            child: DefText(
                              'Image',
                              color: ctrl.bgPageViewIndex.value == 0
                                  ? kBgBlack
                                  : kInactiveColor,
                            ).normal,
                          ),
                        );
                      },
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () async {
                      await controller.pageBgController.animateToPage(
                        1,
                        duration: kDefaultFastDuration,
                        curve: kDefaultCurve,
                      );
                      controller.bgPageViewIndex.value = 1;
                    },
                    child: GetX<CanvasController>(
                      init: CanvasController(),
                      builder: (ctrl) {
                        return Container(
                          color: kBgWhite,
                          padding: const EdgeInsets.all(10),
                          child: Center(
                            child: DefText(
                              'Color',
                              color: ctrl.bgPageViewIndex.value == 1
                                  ? kBgBlack
                                  : kInactiveColor,
                            ).normal,
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
          Visibility(
            visible: !isBackGround,
            replacement: Expanded(
              child: Obx(
                () => PageView(
                  controller: controller.pageBgController,
                  physics: const NeverScrollableScrollPhysics(),
                  children: [
                    GridView.builder(
                      shrinkWrap: true,
                      physics: const AlwaysScrollableScrollPhysics(),
                      controller: controller.bgScrollController,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 4,
                      ),
                      // itemCount: isBackGround ? controller.listImagesBg.length + 1 : controller.listImages.length + 1,
                      itemCount: controller.listImagesBg.length,
                      itemBuilder: (context, i) {
                        if (i == 0) {
                          return GestureDetector(
                            onTap: () async {
                              var path = await getImageCamera();
                              if (path == null) {
                                return;
                              }
                              await controller.closeBg();
                              controller.selectBgImage(path: path.path);
                              // controller.botNavIndex.value = -1;
                            },
                            child: Container(
                              decoration: const BoxDecoration(
                                color: Colors.black,
                              ),
                              child: const Icon(
                                Icons.photo_camera,
                                color: kBgWhite,
                                size: 15,
                              ),
                            ),
                          );
                        } else if (i == 1) {
                          return GestureDetector(
                            onTap: () {
                              controller.deleteBackground();
                            },
                            child: Container(
                              decoration: const BoxDecoration(),
                              child: Image.asset(
                                'assets/icons/remove.png',
                              ),
                            ),
                          );
                        } else {
                          var index = i - 2;
                          return GestureDetector(
                            onTap: () async {
                              var file =
                                  await controller.listImagesBg[index].file;
                              await controller.closeBg();
                              controller.selectBgImage(path: file?.path ?? '');
                              // controller.botNavIndex.value = -1;
                            },
                            child: Container(
                              decoration: const BoxDecoration(
                                border: Border(
                                  top: BorderSide(
                                    color: kBgBlack,
                                  ),
                                  left: BorderSide(
                                    color: kBgBlack,
                                  ),
                                ),
                              ),
                              child: AssetEntityImage(
                                isBackGround
                                    ? controller.listImagesBg[index]
                                    : controller.listImages[index],
                                isOriginal: false,
                                fit: BoxFit.cover,
                              ),
                            ),
                          );
                        }
                      },
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        GridView.builder(
                          padding:
                              const EdgeInsets.only(top: 5, left: 5, right: 5),
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          controller: controller.imageScrollController,
                          itemCount: colorList.length,
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 8,
                            mainAxisSpacing: 5,
                            crossAxisSpacing: 5,
                            childAspectRatio: 1,
                          ),
                          itemBuilder: (context, index) {
                            return ColorItem(
                              color: colorList[index],
                              onTap: () async {
                                await controller.closeBg();
                                controller.latestSelectedColor.value =
                                    colorList[index].value;
                                controller.selectBgColor(
                                  colorCode:
                                      colorList[index].value.toRadixString(16),
                                );
                                // controller.botNavIndex.value = -1;
                              },
                            );
                          },
                        ),
                        const SizedBox(height: 20),
                        Container(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Row(
                            children: [
                              EyedropperButton(
                                onTap: () async {
                                  controller.isEyeDrop.value = true;
                                  logKey('eye drop pressed',
                                      controller.isEyeDrop.value);
                                },
                                onColor: (color) async {
                                  logKey('EyedropperButton', color.value);
                                  controller.latestSelectedColor.value =
                                      color.value;
                                  controller.selectedColorsList.add(color);
                                  controller.isEyeDrop.value = false;
                                  await controller.panelBgController
                                      .animatePanelToPosition(
                                    0,
                                    duration: kDefaultFastDuration,
                                    curve: kDefaultCurve,
                                  );
                                  controller.selectBgColor(
                                    colorCode: color.value.toRadixString(16),
                                  );
                                },
                                child: Image.asset(
                                  'assets/icons/eyedropper.png',
                                ),
                              ),
                              const SizedBox(width: 5),
                              InkWell(
                                onTap: () async {
                                  await controller.panelBgController
                                      .animatePanelToSnapPoint();
                                  controller.showColorPicker();
                                },
                                child: SizedBox(
                                  height: 38,
                                  width: 38,
                                  child: Image.asset(
                                    'assets/icons/color-wheel.png',
                                  ),
                                ),
                              ),
                              const SizedBox(width: 5),
                              Obx(
                                () => Expanded(
                                  child: SizedBox(
                                    height: 38,
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount:
                                          controller.selectedColorsList.length,
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder: (context, index) {
                                        return ColorItem(
                                          color: controller
                                              .selectedColorsList[index],
                                          onTap: () {
                                            controller.latestSelectedColor
                                                .value = colorList[index].value;
                                          },
                                        );
                                      },
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            child: Expanded(
              child: Obx(
                () => GridView.builder(
                  shrinkWrap: true,
                  physics: const AlwaysScrollableScrollPhysics(),
                  controller: controller.imageScrollController,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                  ),
                  itemCount: isBackGround
                      ? controller.listImagesBg.length + 1
                      : controller.listImages.length + 1,
                  itemBuilder: (context, i) {
                    if (i == 0) {
                      return GestureDetector(
                        onTap: () async {
                          var path = await getImageCamera();
                          if (path == null) {
                            return;
                          }
                          controller.addWidget(
                            type: CanvasItemType.IMAGE,
                            data: path.path,
                          );
                        },
                        child: Container(
                          decoration: const BoxDecoration(
                            color: Colors.black,
                          ),
                          child: const Icon(
                            Icons.photo_camera,
                            color: kBgWhite,
                            size: 15,
                          ),
                        ),
                      );
                    } else {
                      var index = i - 1;
                      return GestureDetector(
                        onTap: () async {
                          if (isBackGround) {
                            var file =
                                await controller.listImagesBg[index].file;
                            await controller.closeBg();
                            // controller.botNavIndex.value = -1;
                            controller.bgImagePath.value = file!.path;
                          } else {
                            var path = await controller.listImages[index].file;
                            // await controller.closeImage();
                            // controller.botNavIndex.value = -1;
                            controller.addWidget(
                              type: CanvasItemType.IMAGE,
                              data: path!.path,
                            );
                          }
                        },
                        child: Container(
                          decoration: const BoxDecoration(
                            border: Border(
                              top: BorderSide(
                                color: kBgBlack,
                              ),
                              left: BorderSide(
                                color: kBgBlack,
                              ),
                            ),
                          ),
                          child: AssetEntityImage(
                            isBackGround
                                ? controller.listImagesBg[index]
                                : controller.listImages[i - 1],
                            isOriginal: false,
                            fit: BoxFit.cover,
                          ),
                        ),
                      );
                    }
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
