import 'dart:developer';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:codingtest/app/config/constants.dart';
import 'package:codingtest/app/helpers/canvas_helper.dart';
import 'package:codingtest/app/helpers/text_align_helper.dart';
import 'package:codingtest/app/model/canvas_model/draw_point.dart';
import 'package:codingtest/app/model/canvas_text_widget_model.dart';
import 'package:codingtest/app/model/gif_widget_data_models.dart';
import 'package:codingtest/app/model/image_widget_data_models.dart';
import 'package:codingtest/app/modules/canvas/components/BaseCanvasItem.dart';
import 'package:codingtest/app/modules/canvas/components/default_placeholder.dart';
import 'package:codingtest/app/modules/canvas/components/default_text.dart';
import 'package:codingtest/app/modules/canvas/components/my_custom_painter.dart';
import 'package:codingtest/app/modules/canvas/components/rotate_icon.dart';
import 'package:codingtest/app/modules/canvas/components/scale_icon.dart';
import 'package:codingtest/app/modules/canvas/controllers/canvas_controller.dart';
import 'package:codingtest/app/utils/function_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:readmore/readmore.dart';

class CanvasItem extends GetView<CanvasController> {
  const CanvasItem({
    super.key,
    required this.index,
    this.scale = 1.0,
    this.showRotate = false,
    this.showScale = false,
  });
  final int index;
  final double scale;
  final bool showRotate;
  final bool showScale;

  @override
  Widget build(BuildContext context) {
    // if (controller.widgetsData[index]['insert_done'] == false) {
    //   return Container(
    //     child: SpinKitCircle(
    //       color: Colors.amber,
    //     ),
    //   );
    // }
    //? ini untuk position rotate icon
    //* ini nilainya dari heigt Icon - positioned
    const positionConstant = 25;
    if (controller.widgetsData[index]['type'] == CanvasItemType.BACKGROUND) {
      return SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: DefText(
          controller.widgetsData[index]['data'],
        ).normal,
      );
    }
    if (controller.widgetsData[index]['type'] == CanvasItemType.IMAGE) {
      return GetX<CanvasController>(
        init: CanvasController(),
        builder: (ctrl) {
          var data =
              ImageWidgetDataModels.fromJson(ctrl.widgetsData[index]['data']);
          File imageFile = File.fromUri(Uri.parse(data.path));
          // var a = imageFile.readAsBytesSync();
          return Offstage(
            offstage: !controller.widgetsData[index]['insert_done'],
            child: IgnorePointer(
              // ignoring: ctrl.botNavIndex.value == 4,
              ignoring: controller.isBrushMode.value,
              // ignoring: false,
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  BaseCanvasItem(
                    index: index,
                    insertDone: ctrl.widgetsData[index]['insert_done'] ?? false,
                    child: Container(
                      constraints: BoxConstraints(
                        maxHeight: 400,
                        maxWidth: Get.width - 100,
                      ),
                      child: Image.file(
                        imageFile,
                        gaplessPlayback: true,
                        // fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  GetX<CanvasController>(
                    init: CanvasController(),
                    builder: (ctrl) {
                      return Visibility(
                        // visible: ctrl.widgetsData[index]['edit_mode'],
                        // visible: ctrl.indexEditMode.value == index || ctrl.widgetsData[index]['edit_mode'],
                        visible: ctrl.indexEditMode.value == index,
                        child: Positioned(
                          right: -10,
                          top: -10,
                          child: RotateIcon(
                            index: index,
                            positionConstant: positionConstant,
                            scale: scale,
                          ),
                        ),
                      );
                    },
                  ),
                  Obx(
                    () {
                      return Visibility(
                        // visible: ctrl.widgetsData[index]['edit_mode'],
                        visible: controller.indexEditMode.value == index,
                        child: Positioned(
                          right: -10,
                          bottom: -10,
                          child: ScaleIcon(
                            index: index,
                            positionConstant: positionConstant,
                            scale: scale,
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          );
        },
      );
    }
    // Opaque targets can be hit by hit tests, causing them to both receive events within their bounds and prevent targets visually behind them from also receiving events.
    // Translucent targets both receive events within their bounds and permit targets visually behind them to also receive events.
    if (controller.widgetsData[index]['type'] == CanvasItemType.GIF) {
      var data =
          GifWidgetDataModels.fromJson(controller.widgetsData[index]['data']);
      return Offstage(
        offstage: !controller.widgetsData[index]['insert_done'],
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            BaseCanvasItem(
              index: index,
              insertDone: controller.widgetsData[index]['insert_done'] ?? false,
              child: Container(
                // key: controller.listGlobalKey[index],
                constraints: BoxConstraints(
                  maxHeight: 400,
                  maxWidth: Get.width - 100,
                ),
                child: CachedNetworkImage(
                  imageUrl: data.url,
                  httpHeaders: const {'accept': 'image/*'},
                  placeholder: (context, url) {
                    return const DefaultPlaceholder(
                      height: 268,
                      width: 480,
                    );
                  },

                  // progressIndicatorBuilder: (context, url, progress) {
                  //   var a = progress.downloaded;
                  //   var b = progress.totalSize;
                  //   logKey('azzd', a);
                  //   logKey('bzzd', b);
                  //   return Container();
                  // },
                ),
              ),
            ),
            Obx(
              () => Visibility(
                visible: controller.widgetsData[index]['edit_mode'],
                child: Positioned(
                  right: -10,
                  top: -10,
                  child: RotateIcon(
                    scale: scale,
                    index: index,
                    positionConstant: positionConstant,
                  ),
                ),
              ),
            ),
            Obx(
              () => Visibility(
                visible: controller.indexEditMode.value == index,
                child: Positioned(
                  right: -10,
                  bottom: -10,
                  child: ScaleIcon(
                    index: index,
                    positionConstant: positionConstant,
                    scale: scale,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
    if (controller.widgetsData[index]['type'] == CanvasItemType.TEXT) {
      // var canvas = CanvasItemModels.fromJson(controller.widgetsData[index]);
      var data =
          CanvasTextWidgetModel.fromJson(controller.widgetsData[index]['data'])
              .obs;
      return GetX<CanvasController>(
        init: CanvasController(),
        builder: (ctrl) {
          var textStyle = GoogleFonts.getFont(
            data.value.fontFamily!,
            textStyle: TextStyle(
              fontSize: data.value.fontSize,
              fontWeight:
                  data.value.isBold ? FontWeight.bold : FontWeight.normal,
              fontStyle:
                  data.value.isItalic ? FontStyle.italic : FontStyle.normal,
              decoration: data.value.isUnderline
                  ? TextDecoration.underline
                  : TextDecoration.none,
              height: data.value.lineHeight,
              color: Color(data.value.color),
            ),
          );
          var textAlign = TextAlign.start;
          if (data.value.textAlign == TextAlignHelper.CENTER) {
            textAlign = TextAlign.center;
          } else if (data.value.textAlign == TextAlignHelper.LEFT) {
            textAlign = TextAlign.left;
          } else if (data.value.textAlign == TextAlignHelper.RIGHT) {
            textAlign = TextAlign.right;
          } else if (data.value.textAlign == TextAlignHelper.END) {
            textAlign = TextAlign.end;
          } else if (data.value.textAlign == TextAlignHelper.CENTER) {
            textAlign = TextAlign.start;
          }
          if (controller.widgetsData[index]['edit_mode'] &&
              controller.indexEditMode.value == index &&
              controller.isTextEditMode.value) {
            // controller.isTextEditMode.value = true;
            // var a = TextSelectionControls();
            return Offstage(
              offstage: !controller.widgetsData[index]['insert_done'],
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  IntrinsicWidth(
                    child: BaseCanvasItem(
                      index: index,
                      insertDone:
                          controller.widgetsData[index]['insert_done'] ?? false,
                      child: GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: Container(
                          constraints: BoxConstraints(
                            minWidth: 100,
                            maxWidth: Get.width,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: kInactiveColor,
                            ),
                          ),
                          child: TextFormField(
                            minLines: 1,
                            maxLines: null,
                            keyboardType: TextInputType.multiline,
                            style: textStyle,
                            textAlign: textAlign,
                            enableInteractiveSelection: true,
                            // controller: controller.testcontroller,
                            controller: controller.rcontroller,
                            onChanged: (value) {
                              controller.saveText(value, index);
                              log('pelue$value');
                            },
                            decoration: const InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Tap to Edit',
                            ),
                            onFieldSubmitted: (value) {},
                          ),
                          // child: FlutterMentions(
                          //   mentions: [
                          //     Mention(
                          //       trigger: '@',
                          //       data: [
                          //         {
                          //           'id': '61as61fsa',
                          //           'display': 'fayeedP',
                          //           'full_name': 'Fayeed Pawaskar',
                          //           'photo': 'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'
                          //         },
                          //       ],
                          //     )
                          //   ],
                          // ),
                          // child: TextFormField(
                          //   minLines: 1,
                          //   maxLines: null,
                          //   keyboardType: TextInputType.multiline,
                          //   style: textStyle,
                          //   textAlign: textAlign,
                          //   enableInteractiveSelection: true,
                          //   controller: controller.textEditingController.value,
                          //   onChanged: (value) {
                          //     controller.saveText(value, index);
                          //     logKey('pelue', value);
                          //   },
                          //   decoration: InputDecoration(
                          //     border: InputBorder.none,
                          //     hintText: 'Edit',
                          //   ),
                          //   onFieldSubmitted: (value) {},
                          // ),
                        ),
                      ),
                    ),
                  ),
                  //* rotate & scale text
                  Visibility(
                    visible: showRotate && controller.isTextEditMode.value,
                    child: Positioned(
                      right: -10,
                      top: -10,
                      child: RotateIcon(
                        scale: scale,
                        index: index,
                        positionConstant: positionConstant,
                      ),
                    ),
                  ),
                  Visibility(
                    visible:
                        showScale && controller.widgetsData[index]['edit_mode'],
                    child: Positioned(
                      right: -10,
                      bottom: -10,
                      child: Transform.scale(
                        scale: 1 / controller.widgetsData[index]['scale'],
                        child: GestureDetector(
                          onTap: () {
                            // controller.widgetsData[index]['can_resize'] = !controller.widgetsData[index]['can_resize'];
                            // controller.widgetsData.refresh();
                          },
                          child: Material(
                            elevation: 5,
                            shape: const CircleBorder(),
                            child: Container(
                              height: 35,
                              width: 35,
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                color: controller.widgetsData[index]
                                        ['can_resize']
                                    ? kBgWhite
                                    : kInactiveColor,
                                shape: BoxShape.circle,
                              ),
                              child: Image.asset(
                                'assets/icons/resize.png',
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else {
            return Offstage(
              offstage: !controller.widgetsData[index]['insert_done'],
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  BaseCanvasItem(
                    index: index,
                    insertDone:
                        controller.widgetsData[index]['insert_done'] ?? false,
                    child: Container(
                      // key: controller.listGlobalKey[index],
                      constraints: const BoxConstraints(
                        minWidth: 100,
                        maxWidth: 500,
                        // minHeight: Get.height,
                      ),
                      // child: Text(
                      //   data.value.longText,
                      //   textAlign: textAlign,
                      //   style: textStyle,
                      // ),
                      child: !data.value.isCut
                          ? Container(
                              padding: const EdgeInsets.symmetric(vertical: 20),
                              child: Text(
                                data.value.longText,
                                textAlign: textAlign,
                                style: textStyle,
                              ),
                            )
                          : ReadMoreText(
                              textAlign: textAlign,
                              style: textStyle,
                              data.value.longText,
                              // trimLines: 10,
                              trimLength: controller.widgetsData[index]['data']
                                  ['trim_start'],
                              trimCollapsedText: 'Show more',
                              moreStyle: const TextStyle(
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline,
                                fontSize: 9.8,
                              ),
                              lessStyle: const TextStyle(
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline,
                                fontSize: 9.8,
                              ),
                              // trimMode: TrimMode.Length,
                              // trimLength: 4,
                            ),
                    ),
                  ),
                  Visibility(
                    visible: showRotate && controller.isObjectEditMode.value,
                    child: Positioned(
                      right: -10,
                      top: -10,
                      child: RotateIcon(
                        scale: scale,
                        index: index,
                        positionConstant: positionConstant,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: showScale && controller.isObjectEditMode.value,
                    child: Positioned(
                      right: -10,
                      bottom: -10,
                      child: ScaleIcon(
                        index: index,
                        positionConstant: positionConstant,
                        scale: scale,
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
        },
      );
    }
    if (controller.widgetsData[index]['type'] == CanvasItemType.BRUSH_BASE) {
      var a = 0;
      List drawPoint =
          controller.widgetsData[index]['data']['brush_data'] ?? [];
      return GetX<CanvasController>(
        init: CanvasController(),
        builder: (_) {
          return Visibility(
            visible: controller.isBrushMode.isFalse ||
                !controller.widgetsData[index]['edit_mode'],
            replacement: IgnorePointer(
              ignoring:
                  controller.isBrushMode.isFalse || controller.isEyeDrop.isTrue,
              child: GestureDetector(
                onPanStart: controller.botNavIndex.value == 4
                    ? (details) {
                        logKey('gesture pan start brush base');
                      }
                    : null,
                onPanUpdate: controller.botNavIndex.value == 4
                    ? (details) {
                        a++;
                        if (a % 4 == 0) {
                          controller.yBrush.add(details.localPosition.dy);
                          controller.xBrush.add(details.localPosition.dx);
                          var yMin =
                              getMin(controller.yBrush.cast<num>()).toDouble();
                          var xMin =
                              getMin(controller.xBrush.cast<num>()).toDouble();
                          controller.yMin.value = yMin;
                          controller.xMin.value = xMin;
                          controller.drawPoint.add(
                            DrawPoint(
                              isDraw: controller.isDraw.value,
                              dx: details.localPosition.dx,
                              dy: details.localPosition.dy,
                              style: 'stroke',
                              stroke: controller.stroke.value,
                              color: controller.latestSelectedColor.value,
                            ).toJson(),
                          );
                        }
                      }
                    : null,
                onPanEnd: controller.botNavIndex.value == 4
                    ? (details) async {
                        controller.drawPoint.add(null);
                        controller.saveState();
                      }
                    : null,
                child: Wrap(
                  children: [
                    Container(
                      //* yMin
                      margin: EdgeInsets.only(
                        top: controller.widgetsData[index]['data']['y_min'] ??
                            0.0,
                      ),
                      // height: controller.heightBrush2.value,
                      height: controller.canvasHeight.value,
                      width: Get.width,
                      // color: Colors.lightGreen.withOpacity(0.3),
                      child: RepaintBoundary(
                        child: CustomPaint(
                          isComplex: true,
                          willChange: false,
                          painter: MyCustomPainter(
                            // drawPoints: controller.drawPoint.value.cast<DrawPoint?>(),
                            drawPoints: controller.isBrushMode.value
                                ? controller.drawPoint.value
                                : drawPoint,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            // visible: controller.isBrushMode.isFalse,
            // visible: controller.widgetsData[index]['edit_mode'],
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                BaseCanvasItem(
                  index: index,
                  useLoading: false,
                  insertDone:
                      controller.widgetsData[index]['insert_done'] ?? false,
                  child: Container(
                    // color: Colors.amber,
                    child: Image.file(
                      File(
                        controller.widgetsData[index]['data']
                                ['image_brush_path'] ??
                            '',
                      ),
                    ),
                  ),
                ),
                Obx(
                  () => Visibility(
                    visible: controller.indexEditMode.value == index,
                    // visible: controller.widgetsData[index]['edit_mode'],
                    child: Positioned(
                      right: -10,
                      top: -10,
                      child: RotateIcon(
                        scale: scale,
                        index: index,
                        positionConstant: positionConstant,
                      ),
                    ),
                  ),
                ),
                Obx(
                  () => Visibility(
                    visible: controller.indexEditMode.value == index,
                    child: Positioned(
                      right: -10,
                      bottom: -10,
                      child: ScaleIcon(
                        index: index,
                        positionConstant: positionConstant,
                        scale: scale,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      );
    }
    return Container(
      height: Get.height - 75,
      width: Get.width,
      // height: 25,
      // width: 25,
      color: Color(
        controller.widgetsData[index]['color'],
      ).withOpacity(1),
      child: Center(
        child: Text('$index asd'),
      ),
    );
  }
}
