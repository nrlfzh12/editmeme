import 'package:codingtest/app/modules/canvas/components/default_text.dart';
import 'package:codingtest/app/modules/canvas/components/primary_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class DefaultDialog extends StatelessWidget {
  const DefaultDialog({
    super.key,
    required this.title,
    this.message = '',
    this.confirmText = 'Confirm',
    required this.onConfirm,
    this.onCancel,
    this.cancelText = 'Cancel',
    this.showCancelButton = true,
  });
  final String title;
  final String message;
  final String confirmText;
  final Function() onConfirm;
  final Function()? onCancel;
  final String cancelText;

  final bool showCancelButton;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(7),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            DefText(title).large,
            const SizedBox(height: 10),
            DefText(
              message,
              textAlign: TextAlign.center,
            ).normal,
            const SizedBox(height: 25),
            Row(
              // mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  child: PrimaryButton(
                    text: confirmText,
                    press: onConfirm,
                    // formBlock: true,
                    // fontSize: 14,
                  ),
                ),
                const SizedBox(width: 10),
                Visibility(
                  visible: showCancelButton,
                  child: Expanded(
                    child: PrimaryButton(
                      text: cancelText,
                      fontSize: 14,
                      press: onCancel ??
                          () {
                            Get.back();
                          },
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
