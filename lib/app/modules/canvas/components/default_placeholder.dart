import 'package:codingtest/app/config/constants.dart';
import 'package:flutter/material.dart';

class DefaultPlaceholder extends StatelessWidget {
  const DefaultPlaceholder({
    this.borderRadius,
    this.imageAsset,
    required this.height,
    required this.width,
    this.fit = BoxFit.cover,
  });

  final BorderRadius? borderRadius;
  final String? imageAsset;
  final double height;
  final double width;
  final BoxFit? fit;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: ClipRRect(
        borderRadius: borderRadius ?? kDefaultBorderRadius10,
        child: Image.asset(
          imageAsset ?? kDefaultPicture,
          fit: fit,
        ),
      ),
    );
  }
}
