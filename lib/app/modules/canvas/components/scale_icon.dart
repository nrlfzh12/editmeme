import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../config/constants.dart';
import '../controllers/canvas_controller.dart';

class ScaleIcon extends GetView<CanvasController> {
  const ScaleIcon({
    required this.index,
    required this.positionConstant,
    required this.scale,
    super.key,
  });
  final int index;
  final int positionConstant;
  final double scale;

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: 1 / controller.widgetsData[index]['scale'],
      child: GestureDetector(
        onPanStart: (details) {
          // logKey('xxx start', controller.xxx.value);
          // logKey('yyy start', controller.yyy.value);
          // controller.xxx.value = 0;
          // controller.yyy.value = 0;
          controller.heightx.value = controller.widgetsData[index]['height'];
          controller.widthx.value = controller.widgetsData[index]['width'];
          controller.xxx.value = details.globalPosition.dx;
          controller.yyy.value = details.globalPosition.dy;
        },
        onPanUpdate: (details) {
          // controller.xxx.value += details.delta.dx;
          // controller.yyy.value += details.delta.dy;
          // controller.mid.value = (controller.xxx.value + controller.yyy.value) / 2;
          // controller.xxx.value = details.globalPosition.dx;
          // controller.yyy.value = details.globalPosition.dy;
          var dx = details.globalPosition.dx - controller.xxx.value;
          var dy = details.globalPosition.dy - controller.yyy.value;

          // controller.mid.value = (details.localPosition.dx + details.localPosition.dy) / 2;
          controller.mid.value = (dx + dy) / 2;
          var newHeight = controller.heightx.value + 2 * controller.mid.value;
          var newWidth = controller.widthx.value + 2 * controller.mid.value;
          // var newScale = (newHeight * newWidth) / (controller.widgetsData[index]['default_height'] * controller.widgetsData[index]['default_width']);
          var newScale = (newHeight * newWidth) / (controller.widgetsData[index]['default_height'] * controller.widgetsData[index]['default_width']);
          controller.scale.value = newScale;
          controller.widgetsData[index]['scale'] = newScale;
          controller.widgetsData[index]['height'] = newHeight;
          controller.widgetsData[index]['width'] = newWidth;
        },
        onPanEnd: (details) {
          controller.widgetsData.refresh();
        },
        child: Material(
          elevation: 5,
          shape: CircleBorder(),
          child: Container(
            height: 35,
            width: 35,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: controller.widgetsData[index]['can_resize'] ? kBgWhite : kInactiveColor,
              shape: BoxShape.circle,
            ),
            child: Image.asset(
              'assets/icons/resize.png',
            ),
          ),
        ),
      ),
    );
  }
}
