import 'package:codingtest/app/model/canvas_text_widget_model.dart';
import 'package:codingtest/app/modules/canvas/controllers/canvas_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../config/constants.dart';

class TextEditingFontStyle extends GetView<CanvasController> {
  const TextEditingFontStyle({
    super.key,
    required this.idx,
  });

  final int idx;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        var data =
            CanvasTextWidgetModel.fromJson(controller.widgetsData[idx]['data'])
                .obs;
        return Container(
          padding: const EdgeInsets.only(top: 50),
          child: Center(
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                GestureDetector(
                  onTap: () {
                    controller.widgetsData[idx]['data']['isBold'] =
                        !controller.widgetsData[idx]['data']['isBold'];
                    controller.widgetsData.refresh();
                  },
                  child: SizedBox(
                    height: 50,
                    width: 50,
                    child: Image.asset(
                      'assets/icons/bold.png',
                      color: !data.value.isBold ? kInactiveColor : null,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    controller.widgetsData[idx]['data']['isItalic'] =
                        !controller.widgetsData[idx]['data']['isItalic'];
                    controller.widgetsData.refresh();
                  },
                  child: SizedBox(
                    height: 50,
                    width: 50,
                    child: Image.asset(
                      'assets/icons/italic.png',
                      color: !data.value.isItalic ? kInactiveColor : null,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    controller.widgetsData[idx]['data']['isUnderline'] =
                        !controller.widgetsData[idx]['data']['isUnderline'];
                    controller.widgetsData.refresh();
                  },
                  child: SizedBox(
                    height: 50,
                    width: 50,
                    child: Image.asset(
                      'assets/icons/underline.png',
                      color: !data.value.isUnderline ? kInactiveColor : null,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
