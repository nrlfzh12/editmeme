import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:codingtest/app/config/constants.dart';
import 'package:codingtest/app/helpers/canvas_helper.dart';
import 'package:codingtest/app/model/canvas_model/snap_res_model.dart';
import 'package:codingtest/app/model/canvas_text_widget_model.dart';
import 'package:codingtest/app/model/canvas_widget_model.dart';
import 'package:codingtest/app/model/gif_widget_data_models.dart';
import 'package:codingtest/app/model/image_widget_data_models.dart';
import 'package:codingtest/app/modules/canvas/components/brush_item.dart';
import 'package:codingtest/app/modules/canvas/components/color_item.dart';
import 'package:codingtest/app/modules/canvas/components/default_text.dart';
import 'package:codingtest/app/modules/canvas/components/primary_button.dart';
import 'package:codingtest/app/modules/canvas/components/text_editing_alignment.dart';
import 'package:codingtest/app/modules/canvas/components/text_editing_color.dart';
import 'package:codingtest/app/modules/canvas/components/text_editing_font_size.dart';
import 'package:codingtest/app/modules/canvas/components/text_editing_font_style.dart';
import 'package:codingtest/app/modules/canvas/components/text_editing_fonts.dart';
import 'package:codingtest/app/modules/canvas/components/text_editing_items.dart';
import 'package:codingtest/app/modules/canvas/components/text_editing_line_height.dart';
import 'package:cyclop/cyclop.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../../helpers/text_size_helper.dart';
import '../../../utils/function_utils.dart';
import '../components/canvas_botnavbar_item.dart';
import '../components/canvas_images_list.dart';
import '../components/canvas_item.dart';
import '../controllers/canvas_controller.dart';

class CanvasView extends GetView<CanvasController> {
  const CanvasView();
  @override
  Widget build(BuildContext context) {
    // CommentCanvasArgs data = Get.arguments;

    return WillPopScope(
      onWillPop: () async {
        controller.exitCanvas();
        return false;
      },
      child: EyeDrop(
        child: Scaffold(
          appBar: AppBar(
            leading: Obx(
              () => InkWell(
                onTap: () {
                  if (controller.isTextEditMode.value ||
                      controller.isObjectEditMode.value) {
                    logKey('delete');
                    var index = controller.getIndexActiveEditMode();
                    controller.deleteWidget(index: index);
                  } else {
                    controller.exitCanvas();
                  }
                },
                child: controller.isTextEditMode.value ||
                        controller.isObjectEditMode.value
                    ? Container(
                        child: Image.asset(
                          'assets/icons/remove.png',
                        ),
                      )
                    : const Icon(
                        Icons.arrow_back,
                        color: kBgBlack,
                      ),
              ),
            ),
            backgroundColor: kBgWhite,
            title: Row(
              children: [
                Obx(
                  () => InkWell(
                    onTap: () {
                      // controller.undoBrush();
                      // controller.undo();
                      if (controller.undoStates.isNotEmpty) {
                        controller.undo();
                      }
                    },
                    child: Icon(
                      Icons.undo_rounded,
                      color: controller.undoStates.isEmpty
                          ? kInactiveColor
                          : kBgBlack,
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                // Text(data.isPost.toString()),
                Obx(
                  () => InkWell(
                    onTap: () {
                      // controller.redoBrush();
                      // controller.redo();
                      if (controller.redoStates.isNotEmpty) {
                        controller.redo();
                      }
                    },
                    child: Icon(
                      Icons.redo_rounded,
                      color: controller.redoStates.isEmpty
                          ? kInactiveColor
                          : kBgBlack,
                    ),
                  ),
                )
              ],
            ),
            actions: [
              Row(
                children: [
                  // Obx(
                  //   () => Visibility(
                  //     visible: controller.widgetsData.isNotEmpty,
                  //     child: Material(
                  //       child: InkWell(
                  //         onTap: () {
                  //           // controller.testPublish();
                  //           // Get.toNamed(Routes.CANVAS_PREVIEW);
                  //           controller.publish();
                  //         },
                  //         child: Container(
                  //           height: 28,
                  //           width: 75,
                  //           decoration: BoxDecoration(
                  //             border: Border.all(
                  //               color: kBgBlack,
                  //             ),
                  //           ),
                  //           child: Center(
                  //             child: Row(
                  //               mainAxisSize: MainAxisSize.min,
                  //               children: [
                  //                 DefText('Publish').normal,
                  //                 // DefText('Bytes : ${controller.bytes.value}').normal,
                  //                 const SizedBox(width: 5),
                  //                 const Icon(
                  //                   Icons.check,
                  //                   color: kBgBlack,
                  //                   size: 10,
                  //                 ),
                  //               ],
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  // ),

                  Obx(
                    () => Visibility(
                      visible: controller.isTextEditMode.value ||
                          controller.isObjectEditMode.value ||
                          controller.isBrushMode.value,
                      child: const SizedBox(width: 10),
                    ),
                  ),
                  Obx(
                    () => Visibility(
                      visible: controller.isBrushMode.value,
                      child: Material(
                        child: InkWell(
                          onTap: () {
                            controller.doneBrush();
                          },
                          child: Container(
                            height: 28,
                            width: 75,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: kBgBlack,
                              ),
                            ),
                            child: Center(
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  DefText('Done').normal,
                                  const SizedBox(width: 5),
                                  const Icon(
                                    Icons.check,
                                    color: kBgBlack,
                                    size: 10,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  // Obx(
                  //   () => Visibility(
                  //     visible: controller.isTextEditMode.value || controller.objectEditMode.value,
                  //     child: Material(
                  //       child: InkWell(
                  //         onTap: () {
                  //           if (controller.isTextEditMode.value) {
                  //             controller.saveText();
                  //             return;
                  //           }
                  //           controller.exitEditMode();
                  //         },
                  //         child: Container(
                  //           height: 28,
                  //           width: 75,
                  //           decoration: BoxDecoration(
                  //             border: Border.all(
                  //               color: kBgBlack,
                  //             ),
                  //           ),
                  //           child: Center(
                  //             child: Row(
                  //               mainAxisSize: MainAxisSize.min,
                  //               children: [
                  //                 DefText('Done').normal,
                  //                 SizedBox(width: 5),
                  //                 Icon(
                  //                   Icons.check,
                  //                   color: kBgBlack,
                  //                   size: 10,
                  //                 ),
                  //               ],
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  // ),
                ],
              ),
              const SizedBox(width: 16),
            ],
          ),
          body: SizedBox(
            width: Get.width,
            height: Get.height,
            child: GetX<CanvasController>(
              init: CanvasController(),
              builder: (controller) {
                var tempBaseCanvasScale = 1.0;
                return Column(
                  children: [
                    Expanded(
                      child: Stack(
                        children: [
                          //* base Canvas
                          Positioned.fill(
                            child: Stack(
                              children: [
                                GetX<CanvasController>(
                                  init: CanvasController(),
                                  builder: (ctrl) {
                                    return GestureDetector(
                                      onTap: () {
                                        if (controller.widgetsData.isEmpty) {
                                          return;
                                        }
                                        if (controller.isTextEditMode.value) {
                                          // logKey('controller.tempEditingText.value', controller.tempEditingText.value);
                                          // controller.saveText();
                                          // return;
                                          // var index = controller.indexEditMode.value;
                                          // var dataText = CanvasTextWidgetModel.fromJson(controller.widgetsData[index]);
                                          if (isEmpty(controller
                                              .tempEditingText.value)) {
                                            controller.deleteWidget(
                                                index: controller
                                                    .indexEditMode.value);
                                          }
                                          controller.panelTextEditing.hide();
                                          controller.widgetsData.refresh();
                                        }
                                        controller.exitEditMode();
                                        controller.isTouchActive.value = false;
                                      },
                                      onScaleStart: (details) {
                                        // controller.widgetsData.refresh();
                                        if (controller.widgetsData.isEmpty) {
                                          return;
                                        }
                                        logKey('base onScaleStart',
                                            controller.indexEditMode.value);
                                        controller.isBaseCanvasScale.value =
                                            true;
                                        if (controller.indexEditMode.value !=
                                            -1) {
                                          tempBaseCanvasScale =
                                              controller.widgetsData[controller
                                                  .indexEditMode
                                                  .value]['scale'];
                                          controller.baseCanvasScale.value =
                                              controller.widgetsData[controller
                                                  .indexEditMode
                                                  .value]['scale'];
                                        }
                                      },
                                      onScaleUpdate: (details) {
                                        controller.baseCanvasScale.value =
                                            tempBaseCanvasScale * details.scale;
                                        controller.scale.value =
                                            tempBaseCanvasScale * details.scale;
                                      },
                                      onScaleEnd: (details) {
                                        if (controller.widgetsData.isEmpty) {
                                          return;
                                        }
                                        controller.isBaseCanvasScale.value =
                                            false;
                                        if (controller.indexEditMode.value !=
                                            -1) {
                                          controller.widgetsData[controller
                                                  .indexEditMode
                                                  .value]['scale'] =
                                              controller.baseCanvasScale.value;
                                          var index =
                                              controller.indexEditMode.value;
                                          var newSize =
                                              controller.getSize(index);
                                          controller.widgetsData[index]
                                              ['height'] = newSize['height'];
                                          controller.widgetsData[index]
                                              ['width'] = newSize['width'];
                                        }
                                        if (controller
                                            .isCanvasItemTouched.isFalse) {
                                          controller.widgetsData.refresh();
                                        }
                                      },
                                      child: Container(
                                        key: controller.canvasKey,
                                        //* ini untuk Background canvasnya
                                        decoration: BoxDecoration(
                                          //* jika background menggunakan warna
                                          color: isNotEmpty(controller
                                                  .backgroundData.value.color)
                                              ? hexToColor(
                                                  controller.backgroundData
                                                      .value.color,
                                                )
                                              : kBgWhite,
                                          //* jika background menggunakan gambar
                                          image: controller.backgroundData.value
                                                      .url !=
                                                  ''
                                              ? DecorationImage(
                                                  image: FileImage(
                                                    File(controller
                                                        .backgroundData
                                                        .value
                                                        .url),
                                                  ),
                                                  fit: BoxFit.cover,
                                                )
                                              : null,
                                        ),
                                        child: Stack(
                                          fit: StackFit.expand,
                                          children: controller.widgetsData
                                              .asMap()
                                              .map(
                                                (idx, value) {
                                                  var index = idx;
                                                  var baseScaleFactor = 1.0;
                                                  var baseRotationFactor = 0.0;
                                                  // var _offset = Offset.zero;
                                                  var initialFocalPoint =
                                                      Offset.zero;
                                                  var sessionOffet =
                                                      Offset.zero;
                                                  bool snapTrigger = false;

                                                  //* ini boolean agar textcontroller dibuild sekali saja di onScaleUpdate
                                                  var isCalled = false;

                                                  return MapEntry(
                                                    index,
                                                    Obx(
                                                      () {
                                                        var widgetData =
                                                            CanvasItemModels.fromJson(
                                                                    controller
                                                                            .widgetsData[
                                                                        index])
                                                                .obs;
                                                        widgetData.refresh();
                                                        return Positioned(
                                                          left: widgetData
                                                              .value.dx,
                                                          top: widgetData
                                                              .value.dy,
                                                          child:
                                                              Transform.rotate(
                                                            // angle: _widgetData.value.rotation,
                                                            angle: widgetData
                                                                    .value
                                                                    .editMode
                                                                ? controller
                                                                    .angle.value
                                                                : widgetData
                                                                    .value
                                                                    .rotation,
                                                            child:
                                                                Transform.scale(
                                                              scale: widgetData
                                                                      .value
                                                                      .editMode
                                                                  ? controller
                                                                      .scale
                                                                      .value
                                                                  : controller.isBaseCanvasScale
                                                                              .value &&
                                                                          widgetData
                                                                              .value
                                                                              .editMode
                                                                      ? controller
                                                                          .baseCanvasScale
                                                                          .value
                                                                      : widgetData
                                                                          .value
                                                                          .scale,

                                                              // scale: controller.scale.value,
                                                              child:
                                                                  GestureDetector(
                                                                //* on doubletap
                                                                onDoubleTap:
                                                                    () async {
                                                                  logKey(
                                                                      'on doubletap canvas item');
                                                                  if (controller
                                                                              .widgetsData[index]
                                                                          [
                                                                          'type'] !=
                                                                      CanvasItemType
                                                                          .TEXT) {
                                                                    return;
                                                                  }
                                                                  // controller.isTextEditMode.value = true;
                                                                  // controller.isObjectEditMode.value = false;
                                                                  controller
                                                                      .exitEditMode();
                                                                  if (controller
                                                                      .panelTextEditing
                                                                      .isPanelClosed) {
                                                                    controller
                                                                        .openTextEditing();
                                                                  }
                                                                  bool
                                                                      isEditMode =
                                                                      controller
                                                                          .checkEditMode();
                                                                  if (!isEditMode) {
                                                                    controller
                                                                        .indexEditMode
                                                                        .value = index;
                                                                    controller
                                                                        .rcontroller
                                                                        .text = widgetData.value.data['long_text'] ==
                                                                                'Tap to edit' &&
                                                                            widgetData.value.data[
                                                                                'isFirstAdd']
                                                                        ? ''
                                                                        : widgetData
                                                                            .value
                                                                            .data['long_text'];
                                                                    controller
                                                                        .isTextEditMode
                                                                        .value = true;
                                                                    controller
                                                                        .tempEditingText
                                                                        .value = widgetData
                                                                            .value
                                                                            .data[
                                                                        'long_text'];
                                                                    // controller.isObjectEditMode.value = true;
                                                                    baseScaleFactor =
                                                                        controller.widgetsData[index]
                                                                            [
                                                                            'scale'];
                                                                    baseRotationFactor =
                                                                        controller.widgetsData[index]
                                                                            [
                                                                            'rotation'];
                                                                    controller
                                                                            .angle
                                                                            .value =
                                                                        widgetData
                                                                            .value
                                                                            .rotation;
                                                                    controller.widgetsData[
                                                                            index]
                                                                        [
                                                                        'edit_mode'] = true;
                                                                    //* untuk scale
                                                                    controller
                                                                        .scale
                                                                        .value = controller
                                                                            .widgetsData[index]
                                                                        [
                                                                        'scale'];
                                                                  }

                                                                  controller
                                                                      .widgetsData
                                                                      .refresh();
                                                                },
                                                                //*on tap
                                                                onTap:
                                                                    () async {
                                                                  logKey(
                                                                      'on tap canvas item');
                                                                  widgetData
                                                                      .refresh();
                                                                  if (controller
                                                                      .isBaseCanvasScale
                                                                      .value) {
                                                                    return;
                                                                  }
                                                                  // if (controller.isTouchActive.value && controller.indexEditMode.value != index) {
                                                                  //   logKey('touch already active onTap');
                                                                  //   return;
                                                                  // }

                                                                  //! ontap nya masih be lum hide textediting panel
                                                                  controller
                                                                      .exitEditMode();
                                                                  controller
                                                                      .isTouchActive
                                                                      .value = false;
                                                                  controller
                                                                          .angle
                                                                          .value =
                                                                      0.0;
                                                                  controller
                                                                      .angleDelta
                                                                      .value = 0.0;
                                                                  //* false previous edit mode (kalau ada)
                                                                  if (controller
                                                                              .indexEditMode
                                                                              .value !=
                                                                          -1 &&
                                                                      controller
                                                                          .widgetsData
                                                                          .isNotEmpty) {
                                                                    controller
                                                                            .widgetsData[
                                                                        controller
                                                                            .indexEditMode
                                                                            .value]['edit_mode'] = false;
                                                                  }

                                                                  // controller.widgetsData.refresh();
                                                                  controller
                                                                      .isTextEditMode
                                                                      .value = false;
                                                                  controller
                                                                      .isObjectEditMode
                                                                      .value = false;

                                                                  //* untuk scale
                                                                  controller
                                                                      .scale
                                                                      .value = controller
                                                                              .widgetsData[
                                                                          index]
                                                                      ['scale'];
                                                                  // controller.heightx.value = controller.widgetsData[index]['height'];
                                                                  // controller.widthx.value = controller.widgetsData[index]['width'];

                                                                  widgetData
                                                                      .refresh();
                                                                  bool
                                                                      isEditMode =
                                                                      controller
                                                                          .checkEditMode();
                                                                  if (!isEditMode) {
                                                                    controller
                                                                        .indexEditMode
                                                                        .value = index;
                                                                    //* if text widget
                                                                    if (controller.widgetsData[index]
                                                                            [
                                                                            'type'] ==
                                                                        CanvasItemType
                                                                            .TEXT) {
                                                                      controller
                                                                          .rcontroller
                                                                          .text = widgetData.value.data['long_text'] == 'Tap to edit' &&
                                                                              widgetData.value.data[
                                                                                  'isFirstAdd']
                                                                          ? ''
                                                                          : widgetData
                                                                              .value
                                                                              .data['long_text'];
                                                                    }
                                                                    // if (controller.widgetsData[index]['type'] == CanvasItemType.TEXT) {
                                                                    //   if (controller.panelTextEditing.isPanelClosed) {
                                                                    //     controller.openTextEditing();
                                                                    //   }
                                                                    //   controller.isTextEditMode.value = true;
                                                                    //   controller.tempEditingText.value = _widgetData.value.data['long_text'];
                                                                    // } else {
                                                                    // controller.isObjectEditMode.value = true;
                                                                    // }
                                                                    controller
                                                                        .isObjectEditMode
                                                                        .value = true;
                                                                    baseScaleFactor =
                                                                        controller.widgetsData[index]
                                                                            [
                                                                            'scale'];
                                                                    baseRotationFactor =
                                                                        controller.widgetsData[index]
                                                                            [
                                                                            'rotation'];
                                                                    controller
                                                                            .angle
                                                                            .value =
                                                                        widgetData
                                                                            .value
                                                                            .rotation;
                                                                    controller.widgetsData[
                                                                            index]
                                                                        [
                                                                        'edit_mode'] = true;
                                                                    //* untuk scale
                                                                    // logKey('height asd', controller.widgetsData[index]);
                                                                    controller
                                                                        .heightx
                                                                        .value = controller
                                                                            .widgetsData[index]
                                                                        [
                                                                        'height'];
                                                                    controller
                                                                        .widthx
                                                                        .value = controller
                                                                            .widgetsData[index]
                                                                        [
                                                                        'width'];
                                                                  }
                                                                  controller
                                                                      .widgetsData
                                                                      .refresh();
                                                                },
                                                                onScaleStart:
                                                                    (details) async {
                                                                  widgetData
                                                                      .refresh();
                                                                  if (controller
                                                                      .isBaseCanvasScale
                                                                      .value) {
                                                                    return;
                                                                  }
                                                                  if (controller
                                                                          .isTouchActive
                                                                          .value &&
                                                                      controller
                                                                              .indexEditMode
                                                                              .value !=
                                                                          index) {
                                                                    logKey(
                                                                        'touch already active onScaleUpdate $index');
                                                                    return;
                                                                  }
                                                                  logKey(
                                                                      'msk sini zxx');
                                                                  controller
                                                                      .isTouchActive
                                                                      .value = true;
                                                                  controller
                                                                          .angle
                                                                          .value =
                                                                      0.0;
                                                                  controller
                                                                      .angleDelta
                                                                      .value = 0.0;
                                                                  //* untuk scale
                                                                  // logKey('controller.widgetsData[index]', controller.widgetsData[index]);
                                                                  controller
                                                                      .scale
                                                                      .value = controller
                                                                              .widgetsData[
                                                                          index]
                                                                      ['scale'];
                                                                  // controller.heightx.value = controller.widgetsData[index]['height'];
                                                                  controller
                                                                      .widthx
                                                                      .value = controller
                                                                              .widgetsData[
                                                                          index]
                                                                      ['width'];
                                                                  // controller.heightx.value = controller.widgetsData[index]['height'];
                                                                  // controller.widthx.value = controller.widgetsData[index]['width'];
                                                                  //* false previous edit mode (kalau ada)
                                                                  if (controller
                                                                              .indexEditMode
                                                                              .value !=
                                                                          -1 &&
                                                                      controller
                                                                          .widgetsData
                                                                          .isNotEmpty) {
                                                                    controller
                                                                            .widgetsData[
                                                                        controller
                                                                            .indexEditMode
                                                                            .value]['edit_mode'] = false;
                                                                  }

                                                                  // controller.widgetsData.refresh();
                                                                  // controller.isTextEditMode.value = false;
                                                                  // controller.isObjectEditMode.value = false;

                                                                  widgetData
                                                                      .refresh();
                                                                  //* exit from text
                                                                  if (controller
                                                                              .indexEditMode
                                                                              .value !=
                                                                          -1 &&
                                                                      controller.widgetsData[controller.indexEditMode.value]
                                                                              [
                                                                              'type'] ==
                                                                          CanvasItemType
                                                                              .TEXT &&
                                                                      widgetData
                                                                              .value
                                                                              .type !=
                                                                          CanvasItemType
                                                                              .TEXT) {
                                                                    // controller.saveText(value, controller.indexEditMode.value);
                                                                    logKey(
                                                                        'exit from text',
                                                                        controller.widgetsData[controller
                                                                            .indexEditMode
                                                                            .value]);
                                                                    controller
                                                                        .panelTextEditing
                                                                        .hide();
                                                                    widgetData
                                                                        .refresh();
                                                                  }
                                                                  // controller.indexEditMode.value = -1;
                                                                  controller
                                                                      .indexEditMode
                                                                      .value = index;
                                                                  controller
                                                                      .isCanvasItemTouched
                                                                      .value = true;
                                                                  controller.widgetsData[
                                                                          index]
                                                                      [
                                                                      'edit_mode'] = true;
                                                                  controller
                                                                      .isObjectEditMode
                                                                      .value = true;
                                                                  baseScaleFactor =
                                                                      controller
                                                                              .widgetsData[index]
                                                                          [
                                                                          'scale'];
                                                                  baseRotationFactor =
                                                                      controller
                                                                              .widgetsData[index]
                                                                          [
                                                                          'rotation'];
                                                                  widgetData
                                                                          .value
                                                                          .editMode =
                                                                      true;
                                                                  controller
                                                                          .angle
                                                                          .value =
                                                                      widgetData
                                                                          .value
                                                                          .rotation;
                                                                  initialFocalPoint =
                                                                      details
                                                                          .focalPoint;
                                                                  widgetData
                                                                      .refresh();
                                                                },
                                                                onScaleUpdate:
                                                                    (details) {
                                                                  widgetData
                                                                      .refresh();
                                                                  if (controller
                                                                      .isBaseCanvasScale
                                                                      .value) {
                                                                    return;
                                                                  }
                                                                  if (controller
                                                                          .isTouchActive
                                                                          .value &&
                                                                      controller
                                                                              .indexEditMode
                                                                              .value !=
                                                                          index) {
                                                                    logKey(
                                                                        'touch already active onScaleUpdate $index');
                                                                    return;
                                                                  }
                                                                  if (widgetData
                                                                          .value
                                                                          .type ==
                                                                      CanvasItemType
                                                                          .TEXT) {
                                                                    //* isCalled supaya build sekali
                                                                    if (!isCalled) {
                                                                      isCalled =
                                                                          true;
                                                                      controller
                                                                          .rcontroller
                                                                          .text = widgetData.value.data['long_text'] == 'Tap to edit' &&
                                                                              widgetData.value.data[
                                                                                  'isFirstAdd']
                                                                          ? ''
                                                                          : widgetData
                                                                              .value
                                                                              .data['long_text'];
                                                                    }
                                                                  }
                                                                  if (widgetData
                                                                      .value
                                                                      .editMode) {
                                                                    if (initialFocalPoint ==
                                                                        Offset
                                                                            .zero) {
                                                                      initialFocalPoint =
                                                                          details
                                                                              .focalPoint;
                                                                    }
                                                                    sessionOffet =
                                                                        details.focalPoint -
                                                                            initialFocalPoint;
                                                                    if (!controller
                                                                        .shouldSnap
                                                                        .value) {
                                                                      if (details
                                                                              .pointerCount ==
                                                                          1) {
                                                                        controller.widgetsData[index]
                                                                            [
                                                                            'dx'] = sessionOffet
                                                                                .dx +
                                                                            widgetData.value.configDx;
                                                                        controller.widgetsData[index]
                                                                            [
                                                                            'dy'] = sessionOffet
                                                                                .dy +
                                                                            widgetData.value.configDy;
                                                                      } else {
                                                                        // logKey('msk sini pointer 2');
                                                                        controller.widgetsData[index]['scale'] =
                                                                            baseScaleFactor *
                                                                                details.scale;
                                                                        controller
                                                                            .scale
                                                                            .value = baseScaleFactor * details.scale;
                                                                        logKey(
                                                                            'scale',
                                                                            controller.widgetsData[index]['scale']);
                                                                        if (widgetData.value.canResize &&
                                                                            widgetData.value.editMode) {}
                                                                      }
                                                                      // controller.widgetsData.refresh();
                                                                      widgetData
                                                                          .refresh();
                                                                      return;
                                                                    }
                                                                    //* if should snap
                                                                    controller
                                                                        .calcEdge(
                                                                            index);
                                                                    var edge = controller
                                                                        .checkEdge(
                                                                            index);
                                                                    var res = SnapResModel
                                                                        .fromJson(
                                                                            edge);
                                                                    if (!res
                                                                        .trigger) {
                                                                      if (details
                                                                              .pointerCount ==
                                                                          1) {
                                                                        controller.widgetsData[index]
                                                                            [
                                                                            'dx'] = sessionOffet
                                                                                .dx +
                                                                            widgetData.value.configDx;
                                                                        controller.widgetsData[index]
                                                                            [
                                                                            'dy'] = sessionOffet
                                                                                .dy +
                                                                            widgetData.value.configDy;
                                                                      } else {
                                                                        if (widgetData.value.canResize &&
                                                                            widgetData.value.editMode) {
                                                                          // logKey('_sessionOffet', _sessionOffet);
                                                                          controller
                                                                              .scale
                                                                              .value = baseScaleFactor * details.scale;
                                                                          controller.widgetsData[index]['scale'] =
                                                                              baseScaleFactor * details.scale;
                                                                        }
                                                                      }
                                                                    } else {
                                                                      if (res.isLeft ||
                                                                          res.isRight) {
                                                                        if (sessionOffet.dx > controller.snapeBrakeAt ||
                                                                            sessionOffet.dx <=
                                                                                -controller.snapeBrakeAt) {
                                                                          controller.widgetsData[index]
                                                                              [
                                                                              'dx'] = sessionOffet
                                                                                  .dx +
                                                                              widgetData.value.configDx;
                                                                        }
                                                                        controller.widgetsData[index]
                                                                            [
                                                                            'dy'] = sessionOffet
                                                                                .dy +
                                                                            widgetData.value.configDy;
                                                                      } else if (res
                                                                              .isTop ||
                                                                          res.isBottom) {
                                                                        if (sessionOffet.dy > controller.snapeBrakeAt ||
                                                                            sessionOffet.dy <=
                                                                                -controller.snapeBrakeAt) {
                                                                          controller.widgetsData[index]
                                                                              [
                                                                              'dy'] = sessionOffet
                                                                                  .dy +
                                                                              widgetData.value.configDy;
                                                                        }
                                                                        controller.widgetsData[index]
                                                                            [
                                                                            'dx'] = sessionOffet
                                                                                .dx +
                                                                            widgetData.value.configDx;
                                                                      }
                                                                    }
                                                                    widgetData
                                                                        .refresh();
                                                                  }
                                                                },
                                                                onScaleEnd:
                                                                    (details) {
                                                                  if (controller
                                                                      .isBaseCanvasScale
                                                                      .value) {
                                                                    return;
                                                                  }
                                                                  if (controller
                                                                          .isTouchActive
                                                                          .value &&
                                                                      controller
                                                                              .indexEditMode
                                                                              .value !=
                                                                          index) {
                                                                    logKey(
                                                                        'touch already active onScaleEnd $index');
                                                                    return;
                                                                  }
                                                                  controller
                                                                      .isTouchActive
                                                                      .value = false;
                                                                  if (widgetData
                                                                              .value
                                                                              .type ==
                                                                          CanvasItemType
                                                                              .TEXT &&
                                                                      isEmpty(widgetData
                                                                              .value
                                                                              .data['long_text'] ==
                                                                          '')) {
                                                                    return;
                                                                  }
                                                                  controller
                                                                      .isCanvasItemTouched
                                                                      .value = true;
                                                                  if (widgetData
                                                                      .value
                                                                      .editMode) {
                                                                    if (!snapTrigger) {
                                                                      controller.widgetsData[index]
                                                                              [
                                                                              'configDx'] +=
                                                                          sessionOffet
                                                                              .dx;
                                                                      controller.widgetsData[index]
                                                                              [
                                                                              'configDy'] +=
                                                                          sessionOffet
                                                                              .dy;
                                                                    }
                                                                    var newSize =
                                                                        controller
                                                                            .getSize(index);
                                                                    controller.widgetsData[index]
                                                                            [
                                                                            'height'] =
                                                                        newSize[
                                                                            'height'];
                                                                    controller.widgetsData[index]
                                                                            [
                                                                            'width'] =
                                                                        newSize[
                                                                            'width'];
                                                                    controller
                                                                        .widgetsData
                                                                        .refresh();
                                                                    widgetData
                                                                        .refresh();
                                                                    controller
                                                                        .saveState();
                                                                  }
                                                                },
                                                                child:
                                                                    Container(
                                                                  child:
                                                                      CanvasItem(
                                                                    index:
                                                                        index,
                                                                    scale: widgetData
                                                                        .value
                                                                        .scale,
                                                                    showRotate:
                                                                        widgetData
                                                                            .value
                                                                            .editMode,
                                                                    showScale:
                                                                        widgetData
                                                                            .value
                                                                            .editMode,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                    ),
                                                  );
                                                },
                                              )
                                              .values
                                              .toList(),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                                LinearProgressIndicator(
                                  value: calcProgression(
                                    a: controller.megaBytes.value,
                                    b: controller.maxImageSize,
                                  ),
                                  // value: 0.44763984680175783,
                                  color: Colors.red,
                                  backgroundColor: kInactiveColor,
                                ),
                                Obx(
                                  () => Visibility(
                                    visible: controller.showHash.value,
                                    child: Container(
                                        constraints:
                                            const BoxConstraints(maxHeight: 50),
                                        width: double.infinity,
                                        // color: Colors.amber,
                                        child: ListView.separated(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 5),
                                          shrinkWrap: true,
                                          itemCount: controller.hashtags.length,
                                          scrollDirection: Axis.horizontal,
                                          separatorBuilder: (context, index) {
                                            return const SizedBox(width: 10);
                                          },
                                          itemBuilder: (context, index) {
                                            return GestureDetector(
                                              onTap: () {
                                                // controller.rcontroller.text = controller.rcontroller.text.replaceRange(12, 16, 'replacement');
                                                // var a = controller.rcontroller.text.substring(8, 11);
                                                // logKey('avv', a);
                                                var index = controller
                                                    .hashtagPosition(controller
                                                        .onMatchIndex);
                                                logKey('_index', index);
                                                // logKey('_index', _index);
                                                // return;
                                                // controller.replaceHashtag(
                                                //     controller.hashtags[index]
                                                //         ['name'],
                                                //     _index);
                                                controller.showHash.value =
                                                    false;
                                                controller.canSearch.value =
                                                    false;
                                                controller.hashtags.clear();
                                              },
                                              child: Container(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 10,
                                                        vertical: 5),
                                                decoration: BoxDecoration(
                                                  color: kInactiveColor
                                                      .withOpacity(0.6),
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                ),
                                                child: Center(
                                                  child: DefText(
                                                    '#${controller.hashtags[index]['name']}',
                                                    fontWeight: FontWeight.bold,
                                                    color: kBgWhite,
                                                  ).normal,
                                                ),
                                              ),
                                            );
                                          },
                                        )),
                                  ),
                                ),
                                Obx(
                                  () => Visibility(
                                    visible: false,
                                    child: Container(
                                        constraints: const BoxConstraints(
                                            maxHeight: 100),
                                        width: double.infinity,
                                        // color: Colors.amber,
                                        child: ListView.separated(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 5),
                                          shrinkWrap: true,
                                          itemCount: controller.user.length,
                                          scrollDirection: Axis.horizontal,
                                          separatorBuilder: (context, index) {
                                            return const SizedBox(width: 10);
                                          },
                                          itemBuilder: (context, index) {
                                            return GestureDetector(
                                              onTap: () {
                                                var index = controller
                                                    .mentionPosition(controller
                                                        .onMatchIndex);
                                                logKey('_index', index);
                                                // controller.replaceMention(
                                                //     controller.user[index]
                                                //         ['username'],
                                                //     _index);
                                                // controller.showHash.value = false;
                                                controller.canSearch.value =
                                                    false;
                                                controller.user.clear();
                                              },
                                              child: SizedBox(
                                                width: 125,
                                                // color: Colors.amber,
                                                child: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Container(
                                                      height: 50,
                                                      width: 50,
                                                      decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        // color: Colors.amber,
                                                        image: DecorationImage(
                                                          fit: BoxFit.cover,
                                                          image:
                                                              CachedNetworkImageProvider(
                                                            controller
                                                                    .user[index]
                                                                ['avatar'],
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    const SizedBox(height: 10),
                                                    DefText(
                                                      '${controller.user[index]['username']}',
                                                      textAlign:
                                                          TextAlign.center,
                                                    ).normal,
                                                  ],
                                                ),
                                              ),
                                              // child: Container(
                                              //   padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                              //   decoration: BoxDecoration(
                                              //     color: kInactiveColor.withOpacity(0.6),
                                              //     borderRadius: BorderRadius.circular(5),
                                              //   ),
                                              //   child: Center(
                                              //     child: DefText(
                                              //       '${controller.user[index]['username']}',
                                              //       fontWeight: FontWeight.bold,
                                              //       color: kBgWhite,
                                              //     ).normal,
                                              //   ),
                                              // ),
                                            );
                                          },
                                        )),
                                  ),
                                ),
                                //* top canvas selection
                                Obx(
                                  () => Visibility(
                                    visible:
                                        controller.isObjectEditMode.isFalse,
                                    child: Container(
                                      height: controller.topHeight.value,
                                      decoration: DottedDecoration(),
                                    ),
                                  ),
                                ),
                                Obx(
                                  () => Visibility(
                                    visible:
                                        controller.isObjectEditMode.isFalse,
                                    child: Positioned(
                                      right: 50,
                                      top: controller.topHeight.value - 12.5,
                                      child: GestureDetector(
                                        onPanEnd: (details) {
                                          controller.topHeightEnd.value =
                                              controller.topHeight.value;
                                        },
                                        onPanUpdate: (details) {
                                          var height =
                                              controller.topHeightEnd.value +
                                                  details.localPosition.dy;
                                          var temp = controller
                                                  .canvasHeight.value -
                                              controller.bottomHeight.value -
                                              height;
                                          if (height <= 0) {
                                            return;
                                          }
                                          if (temp <=
                                              controller.minimumHeight) {
                                            return;
                                          }

                                          controller.topHeight.value = height;
                                          controller.calcCanvasSize();
                                        },
                                        child: Container(
                                          height: 25,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            // color: Colors.amber,
                                          ),
                                          // child: Icon(
                                          //   Icons.arrow_downward,
                                          // ),
                                          child: Image.asset(
                                            'assets/icons/arrow_height_3.png',
                                            height: 25,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Obx(
                                    () => Visibility(
                                      visible:
                                          controller.isObjectEditMode.isFalse,
                                      child: Container(
                                        height: controller.bottomHeight.value,
                                        // color: Colors.white.withOpacity(0.65),
                                        decoration: DottedDecoration(
                                          linePosition: LinePosition.top,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                //* Bottom slide height
                                Obx(
                                  () => Visibility(
                                    visible:
                                        controller.isObjectEditMode.isFalse,
                                    child: Positioned(
                                      bottom:
                                          controller.bottomHeight.value - 12.5,
                                      right: 50,
                                      child: GestureDetector(
                                        onPanEnd: (details) {
                                          controller.bottomHeightEnd.value =
                                              controller.bottomHeight.value;
                                        },
                                        onPanUpdate: (details) {
                                          var height =
                                              controller.bottomHeightEnd.value -
                                                  details.localPosition.dy;
                                          var temp =
                                              controller.canvasHeight.value -
                                                  controller.topHeight.value -
                                                  height;
                                          if (height <= 0) {
                                            return;
                                          }
                                          if (height <= controller.maxBottom) {
                                            return;
                                          }
                                          if (temp <=
                                              controller.minimumHeight) {
                                            return;
                                          }

                                          controller.bottomHeight.value =
                                              height;
                                          controller.calcCanvasSize();
                                        },
                                        child: Container(
                                          height: 25,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                          ),
                                          // child: Icon(
                                          //   Icons.arrow_upward,
                                          // ),
                                          child: Image.asset(
                                            'assets/icons/arrow_height_3.png',
                                            height: 25,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),

                          //* Sliding up panel
                          //* image panel
                          Obx(
                            () => Opacity(
                              opacity: controller.slidingOpacity.value,
                              child: SlidingUpPanel(
                                minHeight: controller.isObjectEditMode.isFalse
                                    ? 30
                                    : controller.tempMinHeigh.value,
                                // minHeight: controller.isObjectEditMode.value ? 100 : controller.tempMinHeigh.value,
                                maxHeight: Get.height * 0.89,
                                controller: controller.panelImageController,
                                snapPoint: 0.4,
                                panel: const CanvasImagesList(),
                              ),
                            ),
                          ),

                          //* background image panel
                          Obx(
                            () => Opacity(
                              opacity: controller.slidingOpacity.value,
                              child: Opacity(
                                // opacity: controller.isEyeDrop.value ? 0 : 1,
                                opacity: controller.isEyeDrop.value ? 0 : 1,
                                child: SlidingUpPanel(
                                  // minHeight: controller.tempMinHeigh.value,
                                  minHeight: controller.isObjectEditMode.isFalse
                                      ? 30
                                      : controller.tempMinHeigh.value,
                                  maxHeight: Get.height * 0.89,
                                  controller: controller.panelBgController,
                                  snapPoint: 0.4,
                                  panel: Container(
                                    child: const CanvasImagesList(
                                      isBackGround: true,
                                      // pageController: controller.pageBgController,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //* text panel
                          Obx(
                            () => Opacity(
                              opacity: controller.slidingOpacity.value,
                              child: Offstage(
                                // visible: !controller.isKeybordShowed.value,
                                offstage: controller.isKeybordShowed.value,
                                child: SlidingUpPanel(
                                  minHeight: controller.tempMinHeigh.value,
                                  // maxHeight: Get.height * 0.89,
                                  maxHeight: Get.height * 0.65,
                                  controller: controller.panelTextEditing,
                                  snapPoint: 0.6,
                                  panel: Column(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          if (controller.panelTextEditing
                                                  .panelPosition ==
                                              0) {
                                            controller.panelTextEditing
                                                .animatePanelToPosition(1);
                                          } else {
                                            controller.panelTextEditing
                                                .animatePanelToPosition(0);
                                          }
                                        },
                                        child: Container(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 10),
                                          width: double.infinity,
                                          child: Center(
                                            child: Container(
                                              height: 10,
                                              width: 100,
                                              decoration: const BoxDecoration(
                                                color: kInactiveColor,
                                                borderRadius:
                                                    kDefaultBorderRadius10,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: TextEditingItems(
                                                assets: 'assets/icons/font.png',
                                                isActive: controller
                                                        .textEditIndex.value ==
                                                    0,
                                                onTap: () {
                                                  controller
                                                      .changedTextEditing(0);
                                                },
                                              ),
                                            ),
                                            Expanded(
                                              child: TextEditingItems(
                                                assets:
                                                    'assets/icons/font-size.png',
                                                isActive: controller
                                                        .textEditIndex.value ==
                                                    1,
                                                onTap: () {
                                                  controller
                                                      .changedTextEditing(1);
                                                },
                                              ),
                                            ),
                                            Expanded(
                                              child: TextEditingItems(
                                                assets: 'assets/icons/bold.png',
                                                isActive: controller
                                                        .textEditIndex.value ==
                                                    2,
                                                onTap: () {
                                                  controller
                                                      .changedTextEditing(2);
                                                },
                                              ),
                                            ),
                                            Expanded(
                                              child: TextEditingItems(
                                                assets:
                                                    'assets/icons/align-left.png',
                                                isActive: controller
                                                        .textEditIndex.value ==
                                                    3,
                                                onTap: () {
                                                  controller
                                                      .changedTextEditing(3);
                                                },
                                              ),
                                            ),
                                            Expanded(
                                              child: TextEditingItems(
                                                assets:
                                                    'assets/icons/line-height.png',
                                                isActive: controller
                                                        .textEditIndex.value ==
                                                    4,
                                                onTap: () {
                                                  controller
                                                      .changedTextEditing(4);
                                                },
                                              ),
                                            ),
                                            Expanded(
                                              child: TextEditingItems(
                                                assets:
                                                    'assets/icons/color.png',
                                                isActive: controller
                                                        .textEditIndex.value ==
                                                    5,
                                                onTap: () {
                                                  controller
                                                      .changedTextEditing(5);
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      //* List fonts
                                      Obx(
                                        () => Visibility(
                                          visible: !controller
                                                  .isKeybordShowed.value &&
                                              controller.textEditIndex.value ==
                                                  0 &&
                                              controller.isTextEditMode.value,
                                          child: const Expanded(
                                            child: TextEditingFonts(),
                                          ),
                                        ),
                                      ),

                                      //* Font size
                                      Obx(
                                        () {
                                          // int idx = controller.getIndexActiveTextEdit();
                                          return Visibility(
                                            visible: !controller
                                                    .isKeybordShowed.value &&
                                                controller
                                                        .textEditIndex.value ==
                                                    1 &&
                                                controller.isTextEditMode.value,
                                            child: TextEditingFontSize(
                                              index: controller
                                                  .indexEditMode.value,
                                            ),
                                          );
                                        },
                                      ),

                                      //* Font Style
                                      Obx(
                                        () {
                                          int idx = controller
                                              .getIndexActiveTextEdit();
                                          return Visibility(
                                            visible: !controller
                                                    .isKeybordShowed.value &&
                                                controller
                                                        .textEditIndex.value ==
                                                    2,
                                            child:
                                                TextEditingFontStyle(idx: idx),
                                          );
                                        },
                                      ),

                                      //* Text align
                                      Obx(
                                        () {
                                          int idx = controller
                                              .getIndexActiveTextEdit();
                                          return Visibility(
                                            visible: !controller
                                                    .isKeybordShowed.value &&
                                                controller
                                                        .textEditIndex.value ==
                                                    3,
                                            child:
                                                TextEditingAlignment(idx: idx),
                                            // child: Expanded(
                                            //   child: TextEditingAlignment(
                                            //     idx: idx,
                                            //   ),
                                            // ),
                                          );
                                        },
                                      ),

                                      //* Text line height
                                      Obx(
                                        () {
                                          int idx = controller
                                              .getIndexActiveTextEdit();
                                          return Visibility(
                                            visible: !controller
                                                    .isKeybordShowed.value &&
                                                controller
                                                        .textEditIndex.value ==
                                                    4 &&
                                                controller
                                                        .indexEditMode.value !=
                                                    -1,
                                            child: Expanded(
                                              child: TextEditingLineHeight(
                                                index: controller
                                                    .indexEditMode.value,
                                              ),
                                            ),
                                          );
                                        },
                                      ),

                                      //* Text color
                                      Obx(
                                        () {
                                          int idx = controller
                                              .getIndexActiveTextEdit();
                                          return Visibility(
                                            visible: !controller
                                                    .isKeybordShowed.value &&
                                                controller
                                                        .textEditIndex.value ==
                                                    5,
                                            child: Expanded(
                                              child: TextEditingColor(idx: idx),
                                            ),
                                          );
                                        },
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //* Edit Mode
                          Obx(
                            () => Visibility(
                              visible: controller.isObjectEditMode.value ||
                                  controller.isTextEditMode.value,
                              // visible: true,
                              child: Positioned.fill(
                                // bottom: 64,
                                child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Material(
                                    elevation: 10,
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        border: Border(
                                          bottom: BorderSide(
                                            color: kInactiveColor,
                                          ),
                                        ),
                                      ),
                                      height: 70,
                                      child: Row(
                                        children: [
                                          const SizedBox(width: 10),
                                          Expanded(
                                            child: GestureDetector(
                                              onTap: () {
                                                controller.moveDownWidget();
                                              },
                                              child: Container(
                                                child: Image.asset(
                                                  'assets/icons/layer-up.png',
                                                ),
                                              ),
                                            ),
                                          ),
                                          const SizedBox(width: 10),
                                          Expanded(
                                            child: GestureDetector(
                                              onTap: () {
                                                controller.moveUpWidget();
                                              },
                                              child: Container(
                                                child: Image.asset(
                                                  'assets/icons/layer-down.png',
                                                ),
                                              ),
                                            ),
                                          ),
                                          const SizedBox(width: 10),
                                          const VerticalDivider(
                                            indent: 10,
                                            endIndent: 10,
                                            color: kBgBlack,
                                          ),
                                          Obx(
                                            () {
                                              // var idx = controller.getIndexActiveEditMode();
                                              var canRorate =
                                                  controller.widgetsData[
                                                      controller.indexEditMode
                                                          .value]['can_rotate'];
                                              return Expanded(
                                                child: GestureDetector(
                                                  onTap: () {
                                                    // controller.widgetsData[idx]['can_rotate'] = !controller.widgetsData[idx]['can_rotate'];
                                                    // controller.widgetsData.refresh();
                                                    controller
                                                        .rotation45Degree();
                                                  },
                                                  child: Container(
                                                    child: Image.asset(
                                                      'assets/icons/rotate-object.png',
                                                      color: !canRorate
                                                          ? kInactiveColor
                                                          : kBgBlack,
                                                    ),
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                          const SizedBox(width: 10),
                                          Expanded(
                                            child: GestureDetector(
                                              onTap: () {
                                                controller.resetRotation();
                                              },
                                              child: Container(
                                                child: Image.asset(
                                                  'assets/icons/reset-rotate.png',
                                                ),
                                              ),
                                            ),
                                          ),
                                          const SizedBox(width: 10),
                                          const VerticalDivider(
                                            indent: 10,
                                            endIndent: 10,
                                            color: kBgBlack,
                                          ),
                                          Obx(
                                            () => Expanded(
                                              child: GestureDetector(
                                                onTap: () {
                                                  controller.shouldSnap.value =
                                                      !controller
                                                          .shouldSnap.value;
                                                },
                                                child: Container(
                                                  child: Image.asset(
                                                    'assets/icons/magnet.png',
                                                    color: !controller
                                                            .shouldSnap.value
                                                        ? kInactiveColor
                                                        : kBgBlack,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          const SizedBox(width: 10),
                                          Obx(
                                            () => Visibility(
                                              visible: controller
                                                  .isObjectEditMode.value,
                                              replacement: Expanded(
                                                child: Obx(() {
                                                  var index = controller
                                                      .indexEditMode.value.obs;
                                                  // var data = CanvasTextWidgetModel.fromJson(controller.widgetsData[index]);
                                                  // logKey('daz', data);
                                                  return GestureDetector(
                                                    onTap: () {
                                                      controller.cutText();
                                                      index.refresh();
                                                    },
                                                    child: Container(
                                                      child: Image.asset(
                                                        'assets/icons/text_cut.png',
                                                        color: controller
                                                                        .widgetsData[
                                                                    index.value]
                                                                [
                                                                'data']['isCut']
                                                            ? kBgBlack
                                                            : kInactiveColor,
                                                      ),
                                                    ),
                                                  );
                                                }),
                                              ),
                                              child: Expanded(
                                                child: GestureDetector(
                                                  onTap: () {
                                                    controller.cropImage();
                                                  },
                                                  child: Container(
                                                    child: Image.asset(
                                                      'assets/icons/crop.png',
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          const SizedBox(width: 10),
                                          const VerticalDivider(
                                            indent: 10,
                                            endIndent: 10,
                                            color: kBgBlack,
                                          ),
                                          Obx(
                                            () {
                                              // var idx = controller.getIndexActiveEditMode();
                                              var idx = controller
                                                  .indexEditMode.value;
                                              var temp =
                                                  CanvasItemModels.fromJson(
                                                          controller
                                                              .widgetsData[idx])
                                                      .obs;
                                              return Expanded(
                                                child: GestureDetector(
                                                  onTap: () {
                                                    if (!temp.value.canResize &&
                                                        !temp.value.canRotate &&
                                                        !temp.value
                                                            .canTranslate) {
                                                      controller
                                                              .widgetsData[idx]
                                                          ['can_rotate'] = true;
                                                      controller
                                                              .widgetsData[idx]
                                                          ['can_resize'] = true;
                                                      controller.widgetsData[
                                                                  idx][
                                                              'can_translate'] =
                                                          true;
                                                    } else {
                                                      controller.widgetsData[
                                                                  idx]
                                                              ['can_rotate'] =
                                                          false;
                                                      controller.widgetsData[
                                                                  idx]
                                                              ['can_resize'] =
                                                          false;
                                                      controller.widgetsData[
                                                                  idx][
                                                              'can_translate'] =
                                                          false;
                                                    }
                                                    controller.widgetsData
                                                        .refresh();
                                                  },
                                                  child: Container(
                                                    child: Image.asset(
                                                      !temp.value.canTranslate &&
                                                              !temp.value
                                                                  .canResize &&
                                                              !temp.value
                                                                  .canRotate
                                                          ? 'assets/icons/lock.png'
                                                          : 'assets/icons/unlock.png',
                                                    ),
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                          const SizedBox(width: 10),
                                          Obx(
                                            () {
                                              // var idx = controller.getIndexActiveEditMode();
                                              var idx = controller
                                                  .indexEditMode.value;
                                              var temp =
                                                  CanvasItemModels.fromJson(
                                                          controller
                                                              .widgetsData[idx])
                                                      .obs;
                                              logKey('temp.value.type',
                                                  temp.value.type);
                                              return Visibility(
                                                visible: temp.value.type ==
                                                        CanvasItemType.IMAGE ||
                                                    temp.value.type ==
                                                        CanvasItemType.GIF,
                                                child: Expanded(
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      controller.widgetsData[
                                                                  idx]['data']
                                                              ['can_pop_up'] =
                                                          !controller.widgetsData[
                                                                  idx]['data']
                                                              ['can_pop_up'];
                                                      controller.widgetsData
                                                          .refresh();
                                                    },
                                                    child: Container(
                                                      child: Obx(
                                                        () {
                                                          dynamic temp;
                                                          if (temp.value.type ==
                                                              CanvasItemType
                                                                  .GIF) {
                                                            temp = GifWidgetDataModels
                                                                    .fromJson(temp
                                                                        .value
                                                                        .data)
                                                                .obs;
                                                          } else {
                                                            temp = ImageWidgetDataModels
                                                                    .fromJson(temp
                                                                        .value
                                                                        .data)
                                                                .obs;
                                                          }
                                                          return Image.asset(
                                                            temp.value.canPopUp
                                                                ? 'assets/icons/show-preview.png'
                                                                : 'assets/icons/remove-preview.png',
                                                          );
                                                        },
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                          const SizedBox(width: 10),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //* stroke slider
                          Obx(
                            () => Visibility(
                              visible: controller.isStroke.value,
                              child: Positioned.fill(
                                bottom: 60,
                                child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Material(
                                    elevation: 15,
                                    child: Container(
                                      height: 60,
                                      decoration: const BoxDecoration(
                                        color: kBgWhite,
                                        border: Border(
                                          bottom: BorderSide(
                                            color: kInactiveColor,
                                          ),
                                        ),
                                      ),
                                      child: Slider(
                                        value: controller.stroke.value,
                                        activeColor: kBgBlack,
                                        inactiveColor:
                                            kInactiveColor.withOpacity(0.4),
                                        thumbColor: kBgBlack,
                                        min: 1.0,
                                        max: 20.0,
                                        onChanged: (value) {
                                          controller.stroke.value = value;
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          //* Color picker
                          Obx(
                            () => Visibility(
                              visible: controller.isColorPicker.value &&
                                  controller.isBrushMode.value,
                              child: Positioned.fill(
                                bottom: 60,
                                child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Obx(
                                    () => Opacity(
                                      opacity:
                                          controller.isEyeDrop.value ? 0 : 1,
                                      child: Material(
                                        elevation: 15,
                                        child: Container(
                                          height: 60,
                                          decoration: const BoxDecoration(
                                            color: kBgWhite,
                                            border: Border(
                                              bottom: BorderSide(
                                                color: kInactiveColor,
                                              ),
                                            ),
                                          ),
                                          child: Row(
                                            // mainAxisSize: MainAxisSize.min,
                                            children: [
                                              const SizedBox(width: 20),
                                              EyedropperButton(
                                                onTap: () {
                                                  controller.isEyeDrop.value =
                                                      true;
                                                  // logKey('eye drop pressed', controller.isEyeDrop.value);
                                                },
                                                onColor: (color) {
                                                  // logKey('EyedropperButton', color.value);
                                                  controller.latestSelectedColor
                                                      .value = color.value;
                                                  controller.isEyeDrop.value =
                                                      false;
                                                },
                                                child: Image.asset(
                                                  'assets/icons/eyedropper.png',
                                                ),
                                              ),
                                              const SizedBox(width: 10),
                                              InkWell(
                                                onTap: () {
                                                  controller.showColorPicker();
                                                },
                                                child: SizedBox(
                                                  height: 38,
                                                  width: 38,
                                                  child: Image.asset(
                                                    'assets/icons/color-wheel.png',
                                                  ),
                                                ),
                                              ),
                                              const SizedBox(width: 8),
                                              Expanded(
                                                child: SizedBox(
                                                  height: 38,
                                                  child: ListView.separated(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 8, right: 8),
                                                    shrinkWrap: true,
                                                    // itemCount: controller.colorsList.length,
                                                    itemCount: colorList.length,
                                                    scrollDirection:
                                                        Axis.horizontal,
                                                    separatorBuilder:
                                                        (context, index) {
                                                      return const SizedBox(
                                                          width: 5);
                                                    },
                                                    itemBuilder:
                                                        (context, index) {
                                                      return ColorItem(
                                                        // color: controller.colorsList[index],
                                                        color: colorList[index],
                                                        onTap: () {
                                                          controller
                                                              .latestSelectedColor
                                                              .value = colorList[
                                                                  index]
                                                              .value;
                                                        },
                                                      );
                                                    },
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          //* Brush items
                          Obx(
                            () => Visibility(
                              visible: controller.isBrushMode.value,
                              child: Positioned.fill(
                                // bottom: 64,
                                child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Material(
                                    elevation: 15,
                                    child: Container(
                                      height: 60,
                                      decoration: const BoxDecoration(
                                        border: Border(
                                          bottom: BorderSide(
                                            color: kInactiveColor,
                                          ),
                                        ),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          Obx(
                                            () => BrushItem(
                                              image:
                                                  'assets/icons/brush-tool.png',
                                              color: controller.isDraw.value
                                                  ? kInactiveColor
                                                      .withOpacity(0.4)
                                                  : kBgWhite,
                                              onTap: () {
                                                controller.isDraw.value = true;
                                              },
                                            ),
                                          ),
                                          Obx(
                                            () => BrushItem(
                                              image: 'assets/icons/erase.png',
                                              color: !controller.isDraw.value
                                                  ? kInactiveColor
                                                      .withOpacity(0.4)
                                                  : kBgWhite,
                                              onTap: () {
                                                controller.isDraw.value = false;
                                              },
                                            ),
                                          ),
                                          Obx(
                                            () => BrushItem(
                                              image: 'assets/icons/stroke.png',
                                              color: controller.isStroke.value
                                                  ? kInactiveColor
                                                      .withOpacity(0.4)
                                                  : kBgWhite,
                                              onTap: () {
                                                controller.isColorPicker.value =
                                                    false;
                                                controller.isStroke.value =
                                                    !controller.isStroke.value;
                                              },
                                            ),
                                          ),
                                          Obx(
                                            () => BrushItem(
                                              color: Color(controller
                                                  .latestSelectedColor.value),
                                              border: Border.all(
                                                  color: kInactiveColor),
                                              onTap: () {
                                                controller.isStroke.value =
                                                    false;
                                                controller.isColorPicker.value =
                                                    !controller
                                                        .isColorPicker.value;
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //* Bottom navigation

                          //*text editing
                          // Obx(
                          //   () => Visibility(
                          //     visible: controller.isTextEditMode.value,
                          //     child: Positioned.fill(
                          //       child: Align(
                          //         alignment: Alignment.bottomCenter,
                          //         child: Container(
                          //           constraints: BoxConstraints(
                          //             maxHeight: controller.isKeybordShowed.value ? 60 : 310,
                          //           ),
                          //           color: kBgWhite,
                          //           child: Column(
                          //             children: [
                          //               Container(
                          //                 child: Row(
                          //                   children: [
                          //                     Expanded(
                          //                       child: TextEditingItems(
                          //                         assets: 'assets/icons/font.png',
                          //                         isActive: controller.textEditIndex.value == 0,
                          //                         onTap: () {
                          //                           controller.changedTextEditing(0);
                          //                         },
                          //                       ),
                          //                     ),
                          //                     Expanded(
                          //                       child: TextEditingItems(
                          //                         assets: 'assets/icons/font-size.png',
                          //                         isActive: controller.textEditIndex.value == 1,
                          //                         onTap: () {
                          //                           controller.changedTextEditing(1);
                          //                         },
                          //                       ),
                          //                     ),
                          //                     Expanded(
                          //                       child: TextEditingItems(
                          //                         assets: 'assets/icons/bold.png',
                          //                         isActive: controller.textEditIndex.value == 2,
                          //                         onTap: () {
                          //                           controller.changedTextEditing(2);
                          //                         },
                          //                       ),
                          //                     ),
                          //                     Expanded(
                          //                       child: TextEditingItems(
                          //                         assets: 'assets/icons/align-left.png',
                          //                         isActive: controller.textEditIndex.value == 3,
                          //                         onTap: () {
                          //                           controller.changedTextEditing(3);
                          //                         },
                          //                       ),
                          //                     ),
                          //                     Expanded(
                          //                       child: TextEditingItems(
                          //                         assets: 'assets/icons/line-height.png',
                          //                         isActive: controller.textEditIndex.value == 4,
                          //                         onTap: () {
                          //                           controller.changedTextEditing(4);
                          //                         },
                          //                       ),
                          //                     ),
                          //                     Expanded(
                          //                       child: TextEditingItems(
                          //                         assets: 'assets/icons/color.png',
                          //                         isActive: controller.textEditIndex.value == 5,
                          //                         onTap: () {
                          //                           controller.changedTextEditing(5);
                          //                         },
                          //                       ),
                          //                     ),
                          //                   ],
                          //                 ),
                          //               ),
                          //               //* List fonts
                          //               Obx(
                          //                 () => Visibility(
                          //                   visible: !controller.isKeybordShowed.value && controller.textEditIndex.value == 0,
                          //                   child: Expanded(
                          //                     child: TextEditingFonts(),
                          //                   ),
                          //                 ),
                          //               ),

                          //               //* Font size
                          //               Obx(
                          //                 () {
                          //                   int idx = controller.getIndexActiveTextEdit();
                          //                   return Visibility(
                          //                     visible: !controller.isKeybordShowed.value && controller.textEditIndex.value == 1,
                          //                     child: Expanded(
                          //                       child: TextEditingFontSize(
                          //                         index: idx,
                          //                       ),
                          //                     ),
                          //                   );
                          //                 },
                          //               ),

                          //               //* Font Style
                          //               Obx(
                          //                 () {
                          //                   int idx = controller.getIndexActiveTextEdit();
                          //                   return Visibility(
                          //                     visible: !controller.isKeybordShowed.value && controller.textEditIndex.value == 2,
                          //                     child: Expanded(
                          //                       child: TextEditingFontStyle(idx: idx),
                          //                     ),
                          //                   );
                          //                 },
                          //               ),

                          //               //* Text align
                          //               Obx(
                          //                 () {
                          //                   int idx = controller.getIndexActiveTextEdit();
                          //                   return Visibility(
                          //                     visible: !controller.isKeybordShowed.value && controller.textEditIndex.value == 3,
                          //                     child: Expanded(
                          //                       child: TextEditingAlignment(
                          //                         idx: idx,
                          //                       ),
                          //                     ),
                          //                   );
                          //                 },
                          //               ),

                          //               //* Text line height
                          //               Obx(
                          //                 () {
                          //                   int idx = controller.getIndexActiveTextEdit();
                          //                   return Visibility(
                          //                     visible: !controller.isKeybordShowed.value && controller.textEditIndex.value == 4,
                          //                     child: Expanded(
                          //                       child: TextEditingLineHeight(
                          //                         index: idx,
                          //                       ),
                          //                     ),
                          //                   );
                          //                 },
                          //               ),

                          //               //* Text color
                          //               Obx(
                          //                 () {
                          //                   int idx = controller.getIndexActiveTextEdit();
                          //                   return Visibility(
                          //                     visible: !controller.isKeybordShowed.value && controller.textEditIndex.value == 5,
                          //                     child: Expanded(
                          //                       child: TextEditingColor(idx: idx),
                          //                     ),
                          //                   );
                          //                 },
                          //               )
                          //             ],
                          //           ),
                          //         ),
                          //       ),
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                    Obx(
                      () => Visibility(
                        visible: controller.isTextEditMode.isFalse,
                        child: Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              height: controller.bottomNavigationHeight,
                              width: Get.width,
                              decoration: const BoxDecoration(
                                  color: kBgWhite,
                                  border: Border(
                                    top: BorderSide(
                                      color: kBgBlack,
                                    ),
                                  )),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Obx(
                                      () => CanvasBotnavbarItem(
                                        color: controller.botNavIndex.value == 0
                                            ? kBgBlack
                                            : kInactiveColor,
                                        asset: 'assets/icons/image.png',
                                        text: 'Image',
                                        onTap: () async {
                                          // if (controller.objectEditMode.value) {
                                          //   return;
                                          // }

                                          await controller.botNavTap(0);
                                          // if (controller.panelImageController.isPanelClosed) {
                                          //   controller.openImage();
                                          // } else {
                                          //   controller.closeImage();
                                          // }
                                          // if (controller.botNavIndex.value == 0) {
                                          //   controller.openImage();
                                          // }
                                        },
                                      ),
                                    ),
                                  ),
                                  // Expanded(
                                  //   child: CanvasBotnavbarItem(
                                  //     color: controller.botNavIndex.value == 1
                                  //         ? kBgBlack
                                  //         : kInactiveColor,
                                  //     asset: 'assets/icons/art.png',
                                  //     text: 'Art',
                                  //     onTap: () async {
                                  //       // if (controller.objectEditMode.value) {
                                  //       //   return;
                                  //       // }
                                  //       await controller.botNavTap(1);
                                  //       // controller.getGiphy();
                                  //     },
                                  //   ),
                                  // ),
                                  Expanded(
                                    child: CanvasBotnavbarItem(
                                      color: controller.botNavIndex.value == 2
                                          ? kBgBlack
                                          : kInactiveColor,
                                      asset: 'assets/icons/text.png',
                                      text: 'Text',
                                      onTap: () async {
                                        // if (controller.isObjectEditMode.value) {
                                        //   return;
                                        // }
                                        if (controller.isTextEditMode.value) {
                                          return;
                                        }
                                        await controller.botNavTap(2);
                                        controller.addWidget(
                                          type: CanvasItemType.TEXT,
                                          data: CanvasTextWidgetModel(
                                            longText: 'Tap to edit',
                                            // longText: '',
                                            fontSize: TextSizeHelper.ExtraLarge,
                                          ),
                                        );
                                        // if (controller.botNavIndex.value == 2) {}
                                      },
                                    ),
                                  ),
                                  // Expanded(
                                  //   child: CanvasBotnavbarItem(
                                  //     color: controller.botNavIndex.value == 3
                                  //         ? kBgBlack
                                  //         : kInactiveColor,
                                  //     asset: 'assets/icons/background.png',
                                  //     text: 'Background',
                                  //     onTap: () async {
                                  //       // if (controller.isObjectEditMode.value) {
                                  //       //   return;
                                  //       // }
                                  //       await controller.botNavTap(3);
                                  //       if (controller.botNavIndex.value == 3) {
                                  //         controller.openBg();
                                  //       }
                                  //     },
                                  //   ),
                                  // ),
                                  // Expanded(
                                  //   child: CanvasBotnavbarItem(
                                  //     color: controller.isBrushMode.value
                                  //         ? kBgBlack
                                  //         : kInactiveColor,
                                  //     asset: 'assets/icons/brush.png',
                                  //     text: 'Brush',
                                  //     onTap: () async {
                                  //       // if (controller.isObjectEditMode.value) {
                                  //       //   return;
                                  //       // }
                                  //       await controller.botNavTap(4);
                                  //       var isBrushExist =
                                  //           controller.checkBrushExist();
                                  //       controller.addWidget(
                                  //         type: CanvasItemType.BRUSH_BASE,
                                  //       );
                                  //       // logKey('isBrushExist', isBrushExist);
                                  //       // if (!isBrushExist) {
                                  //       //   controller.addWidget(
                                  //       //     type: CanvasItemType.BRUSH_BASE,
                                  //       //   );
                                  //       // }
                                  //     },
                                  //   ),
                                  // ),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.all(10),
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              height: controller.bottomNavigationHeight,
                              width: Get.width,
                              decoration: const BoxDecoration(
                                  color: kBgWhite,
                                  border: Border(
                                    top: BorderSide(
                                      color: kBgBlack,
                                    ),
                                  )),
                              child: Row(
                                children: [
                                  Expanded(
                                      child: PrimaryButton(
                                    text: 'Simpan',
                                    press: () {
                                      controller.publish();
                                    },
                                  )),
                                  Expanded(
                                      child: PrimaryButton(
                                    text: 'Share',
                                    press: () {},
                                  )),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

// Positioned.fill(
                  //   child: Container(
                  //     child: Stack(
                  //       fit: StackFit.expand,
                  //       children: controller.widgetsData
                  //           .asMap()
                  //           .map(
                  //             (idx, value) {
                  //               var index = idx;
                  //               var widget = CanvasItemModels.fromJson(controller.widgetsData[index]);
                  //               var _scale = 1.0.obs;
                  //               var _angle = 0.0.obs;
                  //               var _angelDelta = 0.0.obs;
                  //               var _oldAngel = 0.0.obs;

                  //               var ddx = 0.0.obs;
                  //               var ddy = 0.0.obs;

                  //               var baseScaleFactor = 1.0;

                  //               _angle.value = widget.rotation;
                  //               if (widget.scale != 0) {
                  //                 _scale.value = widget.scale;
                  //               }

                  //               if (widget.dx != 0) {
                  //                 ddx.value = widget.dx;
                  //               }
                  //               if (widget.dy != 0) {
                  //                 ddy.value = widget.dy;
                  //               }
                  //               if (controller.widgetsData[0]['type'] == CanvasItemType.BACKGROUND) {
                  //                 index += 1;
                  //               }

                  //               Matrix4 _tempMatrix = Matrix4.identity();
                  //               Matrix4 _tempTM = Matrix4.identity();
                  //               Matrix4 _tempSM = Matrix4.identity();
                  //               Matrix4 _tempRM = Matrix4.identity();

                  //               return MapEntry(
                  //                 index,
                  //                 Obx(
                  //                   () {
                  //                     return Positioned(
                  //                       top: ddy.value,
                  //                       left: ddx.value,
                  //                       child: Transform.rotate(
                  //                         angle: _angle.value,
                  //                         alignment: FractionalOffset.center,
                  //                         // angle: pi / 12,
                  //                         child: Transform.scale(
                  //                           scale: _scale.value,
                  //                           child: Container(
                  //                             color: Colors.amber,
                  //                             child: LayoutBuilder(
                  //                               builder: (context, constraints) {
                  //                                 Offset centerOfGestureDetector = Offset(constraints.maxWidth / 2, 130);
                  //                                 logKey('centerOfGestureDetector', centerOfGestureDetector);
                  //                                 return Stack(
                  //                                   clipBehavior: Clip.none,
                  //                                   children: [
                  //                                     OverlayWidget(
                  //                                       index: index,
                  //                                       onTap: () {
                  //                                         bool isEditMode = controller.checkEditMode();
                  //                                         if (!isEditMode) {
                  //                                           //* if text widget
                  //                                           if (controller.widgetsData[index]['type'] == CanvasItemType.TEXT) {
                  //                                             if (controller.checkTextEditActive()) {
                  //                                               Get.snackbar('Alert', 'Please save your current text');
                  //                                               return;
                  //                                             }
                  //                                             if (!controller.widgetsData[index]['edit_mode']) {
                  //                                               controller.widgetsData[index]['edit_mode'] = true;
                  //                                               controller.isTextEditMode.value = !controller.isTextEditMode.value;
                  //                                               controller.widgetsData.refresh();
                  //                                             }
                  //                                             return;
                  //                                           }

                  //                                           controller.objectEditMode.value = true;
                  //                                           controller.widgetsData[index]['edit_mode'] = !controller.widgetsData[index]['edit_mode'];
                  //                                         }
                  //                                         controller.widgetsData.refresh();
                  //                                       },
                  //                                       // shouldRotate: controller.widgetsData[index]['can_rotate'] ?? false,
                  //                                       shouldRotate: true,
                  //                                       shouldScale: controller.widgetsData[index]['can_resize'] ?? false,
                  //                                       shouldTranslate: controller.widgetsData[index]['can_translate'] ?? true,
                  //                                       newCallback: (dx, dy, scale, rotation) {
                  //                                         ddx.value = dx;
                  //                                         ddy.value = dy;
                  //                                         logKey('scale newCallback,', scale);
                  //                                       },
                  //                                       callBack: (m) {
                  //                                         logKey('callback', m);
                  //                                         var zxc = MxGestureDetector.decomposeToValues(_tempMatrix);
                  //                                         _angle.value = zxc.rotation;
                  //                                         _scale.value = zxc.scale;
                  //                                       },
                  //                                       onScaleEnd: (details) {
                  //                                         var decompose = MxGestureDetector.decomposeToValues(_tempMatrix);
                  //                                         if (controller.redoStates.isNotEmpty) {
                  //                                           controller.redoStates.clear();
                  //                                         }
                  //                                         //* for detect is it on the undo state. Needed to control the matrix widget
                  //                                         var _undoState = controller.widgetsData[index]['imageWidgetBool'];
                  //                                         controller.widgetsData[index]['dx'] = decompose.translation.dx;
                  //                                         controller.widgetsData[index]['dy'] = decompose.translation.dy;
                  //                                         controller.widgetsData[index]['scale'] = decompose.scale;
                  //                                         controller.widgetsData[index]['rotation'] = decompose.rotation;
                  //                                         controller.widgetsData[index]['matrix'] = _tempMatrix.storage;
                  //                                         controller.widgetsData[index]['matrix_translation'] = _tempTM.storage;
                  //                                         controller.widgetsData[index]['matrix_scale'] = _tempSM.storage;
                  //                                         controller.widgetsData[index]['matrix_rotation'] = _tempRM.storage;
                  //                                         controller.widgetsData[index]['imageWidgetBool'] = details['bool'];

                  //                                         // logKey('rotation', decompose.rotation);
                  //                                         logKey('ddx', decompose.translation.dx);
                  //                                         logKey('ddy', decompose.translation.dy);

                  //                                         var _size = controller.getSize(index);
                  //                                         controller.widgetsData[index]['height'] = _size['height'];
                  //                                         controller.widgetsData[index]['width'] = _size['width'];
                  //                                         controller.widgetsData[index]['top_edge'] = decompose.translation.dy - (_size['height'] / 2);
                  //                                         controller.widgetsData[index]['bottom_edge'] = decompose.translation.dy + (_size['height'] / 2);
                  //                                         controller.widgetsData[index]['right_edge'] = decompose.translation.dy + (_size['width'] / 2);
                  //                                         controller.widgetsData[index]['right_edge'] = decompose.translation.dy - (_size['width'] / 2);
                  //                                         if (_undoState) {
                  //                                           controller.widgetsData.refresh();
                  //                                         }
                  //                                         controller.saveState();
                  //                                       },
                  //                                       child: Align(
                  //                                         // alignment: Alignment.center,
                  //                                         // widthFactor: 1,
                  //                                         // heightFactor: 1,
                  //                                         child: Container(
                  //                                           height: controller.widgetsData[index]['type'] == CanvasItemType.BRUSH_BASE ? Get.height + 65 : null,
                  //                                           width: controller.widgetsData[index]['type'] == CanvasItemType.BRUSH_BASE ? Get.width + 80 : null,
                  //                                           child: CanvasItem(index: index),
                  //                                         ),
                  //                                       ),
                  //                                     ),
                  //                                     Positioned(
                  //                                       top: -10,
                  //                                       right: -10,
                  //                                       child: Transform.scale(
                  //                                         scale: 1.0 / _scale.value,
                  //                                         child: GestureDetector(
                  //                                           onTap: () {
                  //                                             logKey('tombol merah');
                  //                                           },
                  //                                           onPanStart: (details) {
                  //                                             var touchPositionFromCenter = details.localPosition - centerOfGestureDetector;
                  //                                             _angelDelta.value = _oldAngel.value - touchPositionFromCenter.direction;
                  //                                           },
                  //                                           onPanEnd: (details) {
                  //                                             _oldAngel.value = _angle.value;
                  //                                           },
                  //                                           onPanUpdate: (details) {},
                  //                                           child: Container(
                  //                                             height: 40,
                  //                                             width: 40,
                  //                                             color: Colors.red,
                  //                                           ),
                  //                                         ),
                  //                                       ),
                  //                                     ),
                  //                                   ],
                  //                                 );
                  //                               },
                  //                             ),
                  //                           ),
                  //                         ),
                  //                       ),
                  //                     );
                  //                   },
                  //                 ),
                  //               );
                  //             },
                  //           )
                  //           .values
                  //           .toList(),
                  //     ),
                  //   ),
                  // ),

// bottomNavigationBar: Obx(
//   () => BottomNavigationBar(
//     type: BottomNavigationBarType.fixed,
//     currentIndex: controller.selected.value,
//     selectedItemColor: kBgBlack,
//     unselectedItemColor: kInactiveColor,
//     onTap: (value) {
//       controller.selected.value = value;
//     },
//     items: [
//       BottomNavigationBarItem(
//         label: 'Image',
//         icon: ImageIcon(
//           AssetImage(
//             'assets/icons/image.png',
//           ),
//           color: controller.selected.value == 0 ? kBgBlack : kInactiveColor,
//         ),
//       ),
//       BottomNavigationBarItem(
//         label: 'Art',
//         icon: ImageIcon(
//           AssetImage(
//             'assets/icons/art.png',
//           ),
//           color: controller.selected.value == 1 ? kBgBlack : kInactiveColor,
//         ),
//       ),
//       BottomNavigationBarItem(
//         label: 'Text',
//         icon: ImageIcon(
//           AssetImage(
//             'assets/icons/text.png',
//           ),
//           color: controller.selected.value == 2 ? kBgBlack : kInactiveColor,
//         ),
//       ),
//       BottomNavigationBarItem(
//         label: 'Background',
//         icon: ImageIcon(
//           AssetImage(
//             'assets/icons/background.png',
//           ),
//           color: controller.selected.value == 3 ? kBgBlack : kInactiveColor,
//         ),
//       ),
//       BottomNavigationBarItem(
//         label: 'Brush',
//         icon: ImageIcon(
//           AssetImage(
//             'assets/icons/brush.png',
//           ),
//           color: controller.selected.value == 4 ? kBgBlack : kInactiveColor,
//         ),
//       ),
//     ],
//   ),
// ),
