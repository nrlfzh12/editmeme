import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:codingtest/app/helpers/canvas_helper.dart';
import 'package:codingtest/app/model/background_model/background_widget_model.dart';
import 'package:codingtest/app/model/brush_model/brush_widget_model.dart';
import 'package:codingtest/app/model/canvas_model/draw_point.dart';
import 'package:codingtest/app/model/canvas_text_widget_model.dart';
import 'package:codingtest/app/model/canvas_widget_model.dart';
import 'package:codingtest/app/model/create_post_model.dart';
import 'package:codingtest/app/model/gif_model/gif_widget_model.dart';
import 'package:codingtest/app/model/gif_widget_data_models.dart';
import 'package:codingtest/app/model/image_model/image_widget_model.dart';
import 'package:codingtest/app/model/image_widget_data_models.dart';
import 'package:codingtest/app/model/text_model/text_font_model.dart';
import 'package:codingtest/app/model/text_model/text_widget_models.dart';
import 'package:codingtest/app/modules/canvas/components/default_dialog.dart';
import 'package:codingtest/app/modules/canvas/components/default_text.dart';
import 'package:codingtest/app/routes/app_pages.dart';
import 'package:cyclop/cyclop.dart' as cyc;
import 'package:dio/dio.dart' as dio;
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:giphy_get/giphy_get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart' as syspaths;
import 'package:permission_handler/permission_handler.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:rich_text_controller/rich_text_controller.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:uuid/uuid.dart';
import 'package:vector_math/vector_math.dart' as vm;

import '../../../config/constants.dart';
import '../../../config/environtment.dart';
import '../../../utils/function_utils.dart';
import '../../../utils/network_utils.dart';

class CanvasController extends GetxController {
  //? getStorage
  var box = GetStorage();

  //? globalKey
  List<GlobalKey> listGlobalKey = [];
  GlobalKey keyRed = GlobalKey();
  GlobalKey canvasKey = GlobalKey();
  var netUtil = NetworkUtil.internal();

  var isTouchActive = false.obs;
  var listPost = [].obs;

  //* constanta
  double snapeBrakeAt = 50;
  double bottomNavigationHeight = 64;

  //? batas atas untuk canvas yang terlihat
  var topHeight = 0.0.obs;
  var topHeightEnd = 0.0.obs;
  //? batas bawah untuk canvas yang terlihat
  var bottomHeight = 100.0.obs;
  var bottomHeightEnd = 100.0.obs;
  //? batas minimal tinggi canvas (pixel)
  var minimumHeight = 300.0;
  var maxBottom = 100.0;

  //? tinggi canvas
  var canvasHeight = 0.0.obs;
  //? tinggi canvas yang terlihat
  var showedCanvasHeight = 0.0.obs;

  //? dataCanvas
  var widgetsData = [].obs;
  //? state canvas sebelumnya
  var undoStates = [].obs;
  //? state canvas setelahnya
  var redoStates = [].obs;

  //? untuk redo brush
  RxList<DrawPoint?> redoStateBrush = (List<DrawPoint?>.of([])).obs;

  //? warna brush
  var latestSelectedColor = 0xfff44336.obs;
  //? check draw mode
  var isDraw = true.obs;
  //? check mode ubah stroke
  var isStroke = false.obs;
  //? check mode color picker
  var isColorPicker = false.obs;
  //? check mode eyedrop
  var isEyeDrop = false.obs;
  //? besar stroke brush
  var stroke = 1.0.obs;
  //? daftar warna constant
  var colorsList = [
    const Color(0xffF5222D),
    const Color(0xffFA8C16),
    const Color(0xffFADB14),
    const Color(0xff389E0D),
    const Color(0xff13C2C2),
    const Color(0xff2F54EB),
    const Color(0xff722ED1),
  ].obs;
  var selectedColorsList = [].obs;

  //? index bottomNavigation. -1 untk default supaya tidak memilih manapun
  var botNavIndex = (0 - 1).obs;
  //? boolean magnet
  var shouldSnap = true.obs;

  //? untuk total bytes gambar
  var megaBytes = 0.0.obs;

  //? maxImaegSize dalam MegaByte
  var maxImageSize = 5.0;

  //* Text variable
  PanelController panelTextEditing = PanelController();
  late KeyboardVisibilityController keyboardController;
  late StreamSubscription<bool> keyboardSubscription;
  var isKeybordShowed = false.obs;
  var tempEditingText = ''.obs;
  var textEditIndex = 0.obs;

  var botNavBar = [];

  @override
  void onInit() {
    super.onInit();
    indexEditMode.listen((p0) {
      rcontroller.text = '';
    });
    rcontroller = RichTextController(
      patternMatchMap: {
        //
        //* Returns every Hashtag with red color
        //
        RegExp(r"\B#[a-zA-Z0-9]+\b"): const TextStyle(color: Colors.red),
        //
        //* Returns every Mention with blue color and bold style.
        //
        RegExp(r"\B@[a-zA-Z0-9]+\b"): const TextStyle(
          fontWeight: FontWeight.w800,
          color: Colors.blue,
        ),
        //
        //* Returns every word after '!' with yellow color and italic style.
        //
        RegExp(r"\B![a-zA-Z0-9]+\b"):
            const TextStyle(color: Colors.yellow, fontStyle: FontStyle.italic),
        // add as many expressions as you need!
      },
      //* starting v1.2.0
      // Now you have the option to add string Matching!
      // stringMatchMap: {
      //   "String1": TextStyle(color: Colors.red),
      //   "String2": TextStyle(color: Colors.yellow),
      // },
      //! Assertion: Only one of the two matching options can be given at a time!

      //* starting v1.1.0
      //* Now you have an onMatch callback that gives you access to a List<String>
      //* which contains all matched strings
      onMatch: (List<String> matches) {
        // logKey('rcontroller.text substr', rcontroller.text.substring(rcontroller.text.length - 1));
        return;
        if (timer != null) {
          timer!.cancel();
        }
        timer = Timer(
          const Duration(seconds: 1),
          () {
            if (canSearch.isFalse) {
              return;
            }
            if (matches.last[0] == '#') {
              // getHashtags(matches.last.replaceAll('#', ''));
            } else {
              getUserByMention(matches.last.replaceAll('@', ''));
            }
          },
        );
        // Do something with matches.
        //! P.S
        // as long as you're typing, the controller will keep updating the list.
      },
      onMatchIndex: (p0) {
        onMatchIndex.assignAll(p0);
        var indexHash = hashtagPosition(p0);
        if (indexHash == -1) {
          return;
        }
        if (timer != null) {
          timer!.cancel();
        }
        timer = Timer(
          const Duration(seconds: 1),
          () {
            logKey('masuk sini', canSearch.value);
            if (canSearch.isFalse && indexHash == -1) {
              return;
            }
            var key = p0[indexHash].keys.last;
            var index = p0[indexHash][key]?.first ?? -1;
            if (index == -1) {
              return;
            }
            var char = rcontroller.text[index - 1];
            if (char == '#') {
              getHashtags(key);
            } else {
              getUserByMention(key);
            }
          },
        );
      },

      // deleteOnBack: true,
      // You can control the [RegExp] options used:
      regExpUnicode: true,
    );
    rcontroller.addListener(() {
      showHash.value = false;
      // canSearch.value = false;
      var cursorPosition = rcontroller.selection.base.offset;
      var text = rcontroller.text;
      // var _tes = text[cursorPosition];
      // logKey('cursorPosition', cursorPosition);
      // logKey('_tes', _tes);

      var tempz = text.substring(text.length - 1);
      bool temp = tempz == '#' || tempz == '@';

      //! bug cansearch masih false
      //! case edit hashtag, pilih suggestion hashtag terus buat hashtag baru
      if (temp) {
        canSearch.value = true;
      }
    });
  }

  int hashtagPosition(List hashtags) {
    var cursorPosition = rcontroller.selection.base.offset;
    var index = -1;
    for (var i = 0; i < hashtags.length; i++) {
      var key = hashtags[i].keys.last;
      var rangeData = hashtags[i][key];
      if (rangeData.first <= cursorPosition &&
          cursorPosition <= (rangeData.last + 1)) {
        // logKey('msk', i);
        index = i;
        break;
      }
    }
    return index;
  }

  int mentionPosition(List usernames) {
    var cursorPosition = rcontroller.selection.base.offset;
    var index = -1;
    for (var i = 0; i < usernames.length; i++) {
      var key = usernames[i].keys.last;
      logKey('keyz', key);
      var rangeData = usernames[i][key];
      if (rangeData.first <= cursorPosition &&
          cursorPosition <= (rangeData.last + 1)) {
        index = i;
        break;
      }
    }
    return index;
  }

  var hashtags = [].obs;
  var onMatchIndex = [].obs;
  var showHash = false.obs;
  var canSearch = false.obs;

  void getHashtags(String text) async {
    logKey('getHashtags text', text);
    hashtags.clear();
    try {
      dio.Response res = await netUtil.get('hashtag?q=$text&page=1&size=10');
      if (isNotEmpty(res)) {
        showHash.value = true;
        hashtags.addAll(res.data);
      }
      // logKey('hashtags', hashtags);
    } on dio.DioError catch (e) {
      logKey('error getHashtags', e.message);
    }
  }

  // void replaceHashtag(String hashtag, int index) {
  //   var key = onMatchIndex[index].keys.first;
  //   rcontroller.text = rcontroller.text.replaceRange(
  //       onMatchIndex[index][key].first,
  //       onMatchIndex[index][key].last + 1,
  //       hashtag);
  //   var index = indexEditMode.value;
  //   widgetsData[index]['data']['long_text'] = rcontroller.text;
  //   widgetsData[index]['data']['isFirstAdd'] = false;
  // }

  // void replaceMention(String username, int index) {
  //   logKey('masuk replace mention');
  //   var key = onMatchIndex[index].keys.first;
  //   logKey('_zzxx', key);
  //   rcontroller.text = rcontroller.text.replaceRange(
  //       onMatchIndex[index][key].first,
  //       onMatchIndex[index][key].last + 1,
  //       username);
  //   var index = indexEditMode.value;
  //   widgetsData[index]['data']['long_text'] = rcontroller.text;
  //   widgetsData[index]['data']['isFirstAdd'] = false;
  // }

  var user = [].obs;
  void getUserByMention(String text) async {
    text = text.replaceFirst('@', '');
    logKey('getUserByMention text', text);
    user.clear();
    try {
      dio.Response res = await netUtil.get('/user?q=$text&page=1&size10');
      logKey('res getUserByMention', res);
      user.assignAll(res.data);
    } on dio.DioError catch (e) {
      logKey('error getUserByMention', e.message);
    }
  }

  @override
  void onReady() {
    super.onReady();
    initFunction();
    var canvasSize = getSizeByKey(canvasKey);
    canvasHeight.value = canvasSize['height'];
    calcCanvasSize();
  }

  @override
  void onClose() {
    super.onClose();
    logKey('bgScrollController', bgScrollController);
    textScrolLController.dispose();
    keyboardSubscription.cancel();
    bgScrollController.dispose();
    rcontroller.removeListener(() {});
    rcontroller.dispose();
  }

  Future<File> fileFromImageUrl(String url) async {
    final response = await http.get(Uri.parse(url));

    final documentDirectory = await syspaths.getApplicationDocumentsDirectory();

    final file = File(p.join(documentDirectory.path, 'imagetest.png'));

    file.writeAsBytesSync(response.bodyBytes);

    return file;
  }
  //* common functions

  void initFunction() async {
    var res = await syspaths.getTemporaryDirectory();
    appDir.value = res.path;
    widgetsData.listen((element) async {
      var tempBytes = 0.0;
      for (var e in element) {
        var data = CanvasItemModels.fromJson(e);
        if (data.type == CanvasItemType.IMAGE) {
          var imageData = ImageWidgetDataModels.fromJson(data.data);
          var file = File(imageData.path);
          // var file2 = await fileFromImageUrl(imageData.path);
          tempBytes += file.readAsBytesSync().length;
        }
      }
      var tempKiloBytes = tempBytes / 1024;
      var tempMegaBytes = tempKiloBytes / 1024;
      megaBytes.value = tempMegaBytes;
      // var tes = calcPercentage(a: megaBytes.value, b: 5);
      // logKey('tes', tes / 100);
    });
    // drawPoint.listen((element) {
    // var tes = json.encode(drawPoint);
    // ffi.sizeOf();
    // var bytes = utf8.encode(tes);
    // var a = Int8List.fromList(bytes);
    // logKey('zxczxc', a.elementSizeInBytes);
    // });
    keyboardController = KeyboardVisibilityController();
    keyboardSubscription = keyboardController.onChange.listen((event) {
      isKeybordShowed.value = event;
      // if (!event && panelTextEditing.isPanelShown) {
      //   closeTextEditing();
      // }
      // logKey('keyboard', event);
    });
    // showPanelInitial.value = true;
    await panelImageController.hide();
    await panelBgController.hide();
    await panelTextEditing.hide();
    slidingOpacity.value = 1.0;

    getGoogleFonts();
  }

  void exitCanvas() {
    Get.dialog(
      barrierDismissible: false,
      DefaultDialog(
        title: 'Unsaved Changes',
        message:
            'Are you sure want to go back? All Canvas Changes will not be saved',
        confirmText: 'Discard Canvas',
        cancelText: 'Cancel',
        onConfirm: () {
          logKey('confirm');
          Get.close(2);
        },
      ),
    );
  }

  void calcCanvasSize() {
    showedCanvasHeight.value =
        canvasHeight.value - topHeight.value - bottomHeight.value;
  }

//? publish canvas, menuju screen Routes.PREVIEW_CANVAS_VIEW
  void publish() async {
    List<ImageWidgeModel> images = [];
    List<TextWidgetModels> texts = [];
    List<GifWidgetModel> gifs = [];
    // BrushWidgetModel? brush;
    List<BrushWidgetModel> brush = [];
    var canvasSize = getSizeByKey(canvasKey);
    for (var i = 0; i < widgetsData.length; i++) {
      var dataCanvas = CanvasItemModels.fromJson(widgetsData[i]);
      var size = getSize(i);
      //* image
      if (dataCanvas.type == CanvasItemType.IMAGE) {
        var dataImageCanvas = ImageWidgetDataModels.fromJson(dataCanvas.data);
        images.add(
          ImageWidgeModel(
            index: i,
            type: dataCanvas.type,
            url: dataImageCanvas.path,
            canPopUp: dataImageCanvas.canPopUp,
            x_axis: dataCanvas.dx,
            y_axis: dataCanvas.dy,
            rotation: dataCanvas.rotation,
            scale: dataCanvas.scale,
            width: size['width'],
            height: size['height'],
            createdAt: DateTime.now().toString(),
            bottom_edge: dataCanvas.bottomEdge,
            top_edge: dataCanvas.topEdge,
            left_edge: dataCanvas.leftEdge,
            right_edge: dataCanvas.rightEdge,
            default_width: widgetsData[i]['default_width'],
            default_height: widgetsData[i]['default_height'],
          ),
        );
      }
      //* text
      else if (dataCanvas.type == CanvasItemType.TEXT) {
        var dataText = CanvasTextWidgetModel.fromJson(widgetsData[i]['data']);
        var fontData = TextFontModel(
          fontFamily: dataText.fontFamily!,
          fontSize: dataText.fontSize!,
          fontWeight: 0,
          fontType: 'normal',
          // justify: dataText.textAlign,
          lineHeight: dataText.lineHeight,
          // fontColor: dataText.color,
          isBold: dataText.isBold,
          isItalic: dataText.isItalic,
          isUnderline: dataText.isUnderline,
          textAlign: dataText.textAlign,
          isCut: dataText.isCut,
          cutStart: dataText.trim_start,
        );
        texts.add(
          TextWidgetModels(
            type: dataCanvas.type,
            index: i,
            font: fontData,
            x_axis: dataCanvas.dx,
            y_axis: dataCanvas.dy,
            rotation: dataCanvas.rotation,
            scale: dataCanvas.scale,
            longText: dataText.longText,
            shortText: dataText.longText,
          ),
        );
      } else if (dataCanvas.type == CanvasItemType.GIF) {
        logKey('giff dataz', widgetsData[i]);
        var dataGifCanvas = GifWidgetDataModels.fromJson(
          widgetsData[i]['data'],
        );
        var dataGifPost = GifWidgetModel(
          index: i,
          url: dataGifCanvas.url,
          type: dataCanvas.type,
          rotation: dataCanvas.rotation,
          scale: dataCanvas.scale,
          x_axis: dataCanvas.dx,
          y_axis: dataCanvas.dy,
          width: size['width'],
          height: size['height'],
          bottom_edge: dataCanvas.bottomEdge,
          top_edge: dataCanvas.topEdge,
          left_edge: dataCanvas.leftEdge,
          right_edge: dataCanvas.rightEdge,
          canPopUp: dataGifCanvas.canPopUp,
        );
        gifs.add(dataGifPost);
      } else if (dataCanvas.type == CanvasItemType.BRUSH_BASE) {
        // var _base64 = await drawerToImage(drawPoint, height: 1920, width: 1080);
        var temp = BrushWidgetModel(
          index: i,
          height: widgetsData[i]['default_height'] * dataCanvas.scale,
          width: widgetsData[i]['default_width'] * dataCanvas.scale,
          default_height: widgetsData[i]['default_height'],
          default_width: widgetsData[i]['default_width'],
          rotation: dataCanvas.rotation,
          scale: dataCanvas.scale,
          url: dataCanvas.data['image_brush_path'],
          x_axis: dataCanvas.dx,
          y_axis: dataCanvas.dy,
          bottom_edge: dataCanvas.bottomEdge,
          left_edge: dataCanvas.leftEdge,
          right_edge: dataCanvas.rightEdge,
          top_edge: dataCanvas.topEdge,
          // drawpoint: drawPoint,
          // base64: _base64,
        );
        brush.add(temp);
      }
    }
    var temp = CreatePostModel(
      width: canvasSize['width'],
      height: showedCanvasHeight.value,
      topHeight: topHeight.value,
      bottomHeight: bottomHeight.value,
      // gifs: [],
      gifs: gifs,
      images: images,
      texts: texts,
      backgroud: backgroundData.value,
      // brush: brush != null ? brush.toJson() : {},
      brushes: brush,
      hashtags: [],
      peoples: [],
    ).toJson();

    // if (GetStorage().read('post') != null) {
    //   listPost.value.add(GetStorage().read('post'));
    // }
    // //  else{
    // //    listPost.value.add(GetStorage().read('post'));
    // //  }

    // listPost.add(temp);
    // GetStorage().write('post', listPost);

    // log("${GetStorage().read('post')}listt post");
    Get.toNamed(
      Routes.CANVAS_PREVIEW,
      arguments: temp,
    );
  }

//? tap bottom navigation
  Future<void> botNavTap(int index) async {
    // if (panelImageController.isPanelShown) {
    //   await closeImage();
    // }
    // if (panelBgController.isPanelShown) {
    //   await closeBg();
    // }
    // if (panelTextEditing.isPanelShown) {
    //   await closeTextEditing();
    // }
    // isDraw.value = true;
    // isStroke.value = false;
    // isColorPicker.value = false;
    // isEyeDrop.value = false;

    // if (index == botNavIndex.value) {
    //   botNavIndex.value = -1;
    // } else {
    //   botNavIndex.value = index;
    // }

    botNavIndex.value = index;

    if (index == 0) {
      await closeBg();
      await closeTextEditing();
      if (panelImageController.isPanelClosed) {
        openImage();
      } else {
        await closeImage();
      }
    } else if (index == 1) {
      await closeBg();
      await closeTextEditing();
      await closeImage();
      getGiphy();
    } else if (index == 2) {
      await closeBg();
      await closeImage();
      if (panelTextEditing.isPanelClosed) {
        openTextEditing();
      } else {
        await closeTextEditing();
      }
    } else if (index == 3) {
      await closeImage();
      await closeTextEditing();
      if (panelBgController.isPanelClosed) {
        openBg();
      } else {
        closeBg();
      }
    } else if (index == 4) {
      await closeBg();
      await closeImage();
      await closeTextEditing();
      isBrushMode.value = true;
      isDraw.value = true;
      isStroke.value = false;
      isColorPicker.value = false;
      isEyeDrop.value = false;
    }
  }

  //? menyimpan state canvas
  void saveState({String? type, bool clearRedo = true}) async {
    // if (type == CanvasItemType.IMAGE) {
    //   var _tempWidget = {
    //     'type': type,
    //     'state': widgetsData,
    //   };
    //   undoStates.add(json.encode(_tempWidget));
    //   return;
    // }
    // if (type == CanvasItemType.BRUSH_BASE) {
    //   var _tempBrushBase = {
    //     'type': type,
    //     'background': backgroundData.value.toJson(),
    //     'state': widgetsData,
    //   };
    //   undoStates.add(json.encode(_tempBrushBase));
    //   return;
    // }
    // if (type == CanvasItemType.BRUSH) {
    //   var _tempBrush = {
    //     'type': type,
    //     'drawpoint': drawPoint,
    //     'background': backgroundData.value.toJson(),
    //     // 'state': json.decode(drawPoint.toString()),
    //     'state': json.decode(drawPoint.toString()),
    //   };
    //   var encode = json.encode(_tempBrush);
    //   undoStates.add(encode);
    //   return;
    // }

    var state = {
      'type': type,
      'background': backgroundData.value.toJson(),
      'state': widgetsData,
      'brush': {
        'drawpoint': drawPoint,
        'y_brush': yBrush,
        'y_min': yMin.value,
        'x_brush': xBrush,
        'x_min': xMin.value,
      },
    };
    // logKey('save State', state);
    undoStates.add(json.encode(state));
    if (clearRedo) {
      redoStates.clear();
    }
  }

  //? kembali ke state canvas sebelumnya
  void undo() {
    redoStates.add(undoStates.last);
    undoStates.removeLast();
    if (undoStates.isEmpty) {
      widgetsData.clear();
      backgroundData.value = BackgroundWidgetModel(
        color: Colors.white.value.toRadixString(16).replaceRange(0, 2, '#'),
        url: '',
      );
      backgroundData.refresh();
      return;
    }
    Map prevStateData = json.decode(undoStates.last);
    //? brush state
    drawPoint.assignAll(prevStateData['brush']['drawpoint']);
    yBrush.assignAll(prevStateData['brush']['y_brush']);
    xBrush.assignAll(prevStateData['brush']['x_brush']);
    yMin.value = prevStateData['brush']['y_min'];
    xMin.value = prevStateData['brush']['x_min'];

    widgetsData.assignAll(prevStateData['state']);
    logKey('widgetsData undo', widgetsData);
    backgroundData.value =
        BackgroundWidgetModel.fromJson(prevStateData['background']);
    backgroundData.refresh();
    var different = widgetsData.length - listGlobalKey.length;
    for (var i = 0; i < different.abs(); i++) {
      listGlobalKey.removeLast();
    }
    exitModeFind();
    // objectEditMode.value = checkEditMode();
  }

  //? maju ke state canvas berikutnya
  void redo() {
    Map tempStateData = json.decode(redoStates.last);
    // logKey('redo tempStateData', tempStateData);
    widgetsData.clear();
    var key = GlobalKey();
    listGlobalKey.add(key);
    widgetsData.assignAll(tempStateData['state']);
    //? brush state
    drawPoint.assignAll(tempStateData['brush']['drawpoint']);
    yBrush.assignAll(tempStateData['brush']['y_brush']);
    xBrush.assignAll(tempStateData['brush']['x_brush']);
    yMin.value = tempStateData['brush']['y_min'];
    xMin.value = tempStateData['brush']['x_min'];

    backgroundData.value =
        BackgroundWidgetModel.fromJson(tempStateData['background']);
    backgroundData.refresh();
    if (listGlobalKey.length > widgetsData.length) {
      listGlobalKey.removeLast();
    }
    // for (var i = 0; i < different; i++) {
    //   listGlobalKey.add(key);
    // }
    redoStates.removeLast();
    isObjectEditMode.value = checkEditMode();
    exitModeFind();
    saveState(clearRedo: false);
  }

  //? untuk mendeteksi sisi widget untuk fitur magnet
  checkEdge(index) {
    var trigger = false;
    var isTop = false;
    var isBottom = false;
    var isLeft = false;
    var isRight = false;
    // logKey('widgetsData.length', widgetsData.length);
    for (var i = 0; i < widgetsData.length; i++) {
      var activeItem = CanvasItemModels.fromJson(widgetsData[index]);
      var compareItem = CanvasItemModels.fromJson(widgetsData[i]);
      if (i != index &&
          compareItem.type != CanvasItemType.BRUSH_BASE &&
          compareItem.type != CanvasItemType.BRUSH) {
        // logKey('activeItem', activeItem.rightEdge.floor());
        // logKey('compareItem $i ', compareItem.toJson());
        var size = getSize(index);
        var diffWidth = widgetsData[index]['default_width'] - size['width'];
        var diffHeight = widgetsData[index]['default_height'] - size['height'];
        //* left to left snap
        if (activeItem.leftEdge.floor() == compareItem.leftEdge.floor()) {
          widgetsData[index]['dx'] = compareItem.leftEdge - (diffWidth / 2);
          trigger = true;
          isLeft = true;
        }
        //* Right to right snap
        else if (activeItem.rightEdge.floor() ==
            compareItem.rightEdge.floor()) {
          widgetsData[index]['dx'] =
              compareItem.rightEdge - (diffWidth / 2) - size['width'];
          trigger = true;
          isRight = true;
        }

        //* left to right snap
        else if (activeItem.leftEdge.floor() == compareItem.rightEdge.floor()) {
          widgetsData[index]['dx'] = compareItem.rightEdge - (diffWidth / 2);
          trigger = true;
          isRight = true;
        }

        //* Right to left snap
        else if (activeItem.rightEdge.floor() == compareItem.leftEdge.floor()) {
          widgetsData[index]['dx'] =
              compareItem.leftEdge - (diffWidth / 2) - size['width'];
          trigger = true;
          isLeft = true;
        }
        //* Top to top snap
        else if (activeItem.topEdge.floor() == compareItem.topEdge.floor()) {
          widgetsData[index]['dy'] = compareItem.topEdge - (diffHeight / 2);
          trigger = true;
          isTop = true;
        }
        //* Top to bottom snap
        else if (activeItem.topEdge.floor() == compareItem.bottomEdge.floor()) {
          widgetsData[index]['dy'] = compareItem.bottomEdge - (diffHeight / 2);
          trigger = true;
          isTop = true;
        }
        //* Bottom to bottom snap
        else if (activeItem.bottomEdge.floor() ==
            compareItem.bottomEdge.floor()) {
          widgetsData[index]['dy'] =
              compareItem.bottomEdge - (diffHeight / 2) - size['height'];
          trigger = true;
          isBottom = true;
        }
        //* Bottom to top snap
        else if (activeItem.bottomEdge.floor() == compareItem.topEdge.floor()) {
          widgetsData[index]['dy'] =
              compareItem.topEdge - (diffHeight / 2) - size['height'];
        }
        if (activeItem.leftEdge == compareItem.leftEdge) {
          if (!Get.isSnackbarOpen) {
            // Get.snackbar('Snapped', 'left');
          }
        }
        if (activeItem.rightEdge == compareItem.rightEdge) {
          if (!Get.isSnackbarOpen) {
            // Get.snackbar('Snapped', 'right');
          }
        }
      }
    }
    // return trigger;
    return {
      'trigger': trigger,
      'isTop': isTop,
      'isBottom': isBottom,
      'isLeft': isLeft,
      'isRight': isRight,
    };
  }

  //? untuk mengkalkulasi batas sisi tiap widget
  void calcEdge(index) {
    var size = getSize(index);

    var diffHeight = size['height'] - widgetsData[index]['default_height'];
    var diffWidth = size['width'] - widgetsData[index]['default_width'];

    widgetsData[index]['top_edge'] =
        widgetsData[index]['dy'] - (diffHeight / 2);
    widgetsData[index]['bottom_edge'] =
        widgetsData[index]['top_edge'] + size['height'];
    widgetsData[index]['left_edge'] =
        widgetsData[index]['dx'] - (diffWidth / 2);
    widgetsData[index]['right_edge'] =
        widgetsData[index]['left_edge'] + size['width'];
  }

  void inserItemAtribute({required int index}) {
    logKey('giff msk');
    var defSize = getSizeByKey(listGlobalKey[index]);
    logKey('defSize', defSize);
    var diffWidth = defSize['width'] - defSize['width'];
    var diffHeight = defSize['height'] - defSize['height'];
    widgetsData[index]['width'] = defSize['width'];
    widgetsData[index]['height'] = defSize['height'];
    widgetsData[index]['default_height'] = defSize['height'];
    widgetsData[index]['default_width'] = defSize['width'];
    widgetsData[index]['top_edge'] =
        widgetsData[index]['dy'] - (diffHeight / 2);
    widgetsData[index]['bottom_edge'] =
        widgetsData[index]['top_edge'] + defSize['height'];
    widgetsData[index]['left_edge'] =
        widgetsData[index]['dx'] + (diffWidth / 2);
    widgetsData[index]['right_edge'] =
        widgetsData[index]['left_edge'] + defSize['width'];
    widgetsData[index]['insert_done'] = true;
    logKey('inserItemAtribute', widgetsData[index]);
    widgetsData.refresh();
    saveState();
    // if (Get.isDialogOpen!) {
    //   Get.back();
    // }
  }

  //? fungsi untuk menambahkan item ke canvas
  void addWidget({String type = '', dynamic data}) async {
    exitEditMode(refreshData: true);
    // dialogLoading(size: 40);
    redoStates.clear();
    // var color = (math.Random().nextDouble() * 0xFFFFFF).toInt();
    if (type == CanvasItemType.BRUSH_BASE) {
      var key = GlobalKey();
      listGlobalKey.add(key);
      widgetsData.add(
        CanvasItemModels(
          type: type,
          data: {},
          imageWidgetBool: false,
          editMode: true,
          canRotate: globalDefaultCanRotate,
          canResize: globalDefaultCanResize,
          color: kBgWhite.value,
          // matrix: Matrix4.identity().storage,
          // matrixTranslation: Matrix4.identity().storage,
          // matrixScale: Matrix4.identity().storage,
          // matrixRotaion: Matrix4.identity().storage,
        ).toJson(),
      );
      saveState(type: CanvasItemType.BRUSH);
      logKey('undo state', undoStates);
      return;
    }
    if (type == CanvasItemType.IMAGE) {
      var key = GlobalKey();
      logKey('listGlobalKey', listGlobalKey);
      String path = data;
      listGlobalKey.add(key);
      widgetsData.add(
        CanvasItemModels(
          type: type,
          data: ImageWidgetDataModels(
            path: path,
          ).toJson(),
          imageWidgetBool: false,
          editMode: true,
          canRotate: globalDefaultCanRotate,
          canResize: globalDefaultCanResize,
          color: kBgWhite.value,
          // matrix: Matrix4.identity().storage,
          // matrixTranslation: Matrix4.identity().storage,
          // matrixScale: Matrix4.identity().storage,
          // matrixRotaion: Matrix4.identity().storage,
        ).toJson(),
      );
      indexEditMode.value = widgetsData.length - 1;
      isObjectEditMode.value = true;
      isTextEditMode.value = false;
      widgetsData.refresh();
      return;
    }
    if (type == CanvasItemType.TEXT) {
      var data1 = data as CanvasTextWidgetModel;
      var key = GlobalKey();
      listGlobalKey.add(key);
      widgetsData.add(
        CanvasItemModels(
          type: type,
          data: data1.toJson(),
          imageWidgetBool: false,
          editMode: true,
          canRotate: globalDefaultCanRotate,
          canResize: globalDefaultCanResize,
          color: kBgWhite.value,
          dx: canvasKey.currentContext!.size!.width * 0.35,
          dy: canvasKey.currentContext!.size!.height * 0.35,
          configDx: canvasKey.currentContext!.size!.width * 0.35,
          configDy: canvasKey.currentContext!.size!.height * 0.35,
          // matrix: Matrix4.identity().storage,
          // matrixTranslation: Matrix4.identity().storage,
          // matrixScale: Matrix4.identity().storage,
          // matrixRotaion: Matrix4.identity().storage,
        ).toJson(),
      );
      indexEditMode.value = widgetsData.length - 1;
      isObjectEditMode.value = false;
      isTextEditMode.value = true;

      openTextEditing();
      widgetsData.refresh();
      return;
    }
    if (type == CanvasItemType.GIF) {
      var key = GlobalKey();
      listGlobalKey.add(key);
      widgetsData.add(
        CanvasItemModels(
          type: type,
          data: data,
          imageWidgetBool: false,
          editMode: false,
          canRotate: globalDefaultCanRotate,
          canResize: globalDefaultCanResize,
          color: kBgWhite.value,
          // matrix: Matrix4.identity().storage,
          // matrixTranslation: Matrix4.identity().storage,
          // matrixScale: Matrix4.identity().storage,
          // matrixRotaion: Matrix4.identity().storage,
        ).toJson(),
      );
      widgetsData.refresh();
      return;
    }
  }

//? fungsi untuk delete item

  void deleteWidget(
      {required int index,
      bool isSaveState = true,
      bool refreshWidgetsData = true}) {
    // return;
    indexEditMode.value = -1;
    isObjectEditMode.value = false;
    isTextEditMode.value = false;
    widgetsData.removeAt(index);
    closeTextEditing();
    if (refreshWidgetsData) {
      // widgetsData.refresh();
    }
    saveState();
    if (isSaveState) {}
  }

  //* Edit Mode

  var firstCalled = false.obs;
  //? boolean untuk control refresh saat gesture detector end di baseCanvas
  var isCanvasItemTouched = false.obs;

  //? boolean untuk control apakah sedang berada di gesture detector canvas base
  //? digunakan supaya gesture detector canvasItem dan canvasBase tidak bertabrakan datanya
  var isBaseCanvasScale = false.obs;
  //? untuk control scale value menggunakan base canvas gesture detector
  var baseCanvasScale = 1.0.obs;

  //? check sedang edit text mode, untuk visibility
  var isTextEditMode = false.obs;
  //? check sedang edit mode, untuk visibility
  var isObjectEditMode = false.obs;
  //? check brushMode
  var isBrushMode = false.obs;

  //? angle obs untuk edit mode agar performa lebih smooth
  var angle = 0.0.obs;
  var angleDelta = 0.0.obs;

  //? scale obs
  var scale = 1.0.obs;
  var scaleX = 1.0.obs;
  var scaleY = 1.0.obs;
  var heightx = 0.0.obs;
  var widthx = 0.0.obs;
  var asdbool = true.obs;
  var mid = 0.0.obs;

  var xxx = 0.0.obs;
  var yyy = 0.0.obs;

  //? index editmode pada widgetsData. default datanya -1
  var indexEditMode = (0 - 1).obs;
  // var oldAngle = 0.0.obs;

  //? untuk check editMode
  bool checkEditMode() {
    var isEditModeExist = false;
    for (var i = 0; i < widgetsData.length; i++) {
      if (widgetsData[i]['type'] != CanvasItemType.BRUSH_BASE &&
          widgetsData[i]['edit_mode']) {
        isEditModeExist = true;
        // indexEditMode.value = i;
        break;
      }
    }
    return isEditModeExist;
  }

  //? get index editmode
  int getIndexActiveEditMode() {
    int index = -1;
    for (var i = 0; i < widgetsData.length; i++) {
      if (widgetsData[i]['edit_mode'] ?? false) {
        index = i;
        break;
      }
    }
    // indexEditMode.value = index;
    return index;
  }

  //? keluar dari edit mode
  void exitEditMode(
      {bool refreshData = true, String? currentType, String? previousType}) {
    angle.value = 0.0;
    angleDelta.value = 0.0;
    if (indexEditMode.value != -1 && widgetsData.isNotEmpty) {
      var dataWidget =
          CanvasItemModels.fromJson(widgetsData[indexEditMode.value]);
      widgetsData[indexEditMode.value]['edit_mode'] = false;
      if (dataWidget.type == CanvasItemType.TEXT &&
          isEmpty(tempEditingText.value)) {
        deleteWidget(index: indexEditMode.value, refreshWidgetsData: false);
      }
      // if (_dataWidget.type != CanvasItemType.TEXT) {
      //   panelTextEditing.hide();
      // }
    }
    isTextEditMode.value = false;
    isObjectEditMode.value = false;
    indexEditMode.value = -1;
    xxx.value = 0.0;
    yyy.value = 0.0;
    heightx.value = 0.0;
    widthx.value = 0.0;
    scale.value = 1.0;
    asdbool.value = true;

    if (refreshData) {
      widgetsData.refresh();
    }
  }

  //? editmode false semua item canvas

  void exitModeFind() {
    for (var i = 0; i < widgetsData.length; i++) {
      if (widgetsData[i]['type'] != CanvasItemType.BRUSH_BASE) {
        widgetsData[i]['edit_mode'] = false;
      }
    }
    isTextEditMode.value = false;
    isObjectEditMode.value = false;
  }

  //? move up layer item canvas
  void moveUpWidget() {
    int activeIndex = getIndexActiveEditMode();
    if (activeIndex > 0) {
      var temp = widgetsData[activeIndex];
      widgetsData.removeAt(activeIndex);
      widgetsData.insert(activeIndex - 1, temp);
      widgetsData.refresh();
      indexEditMode.value = activeIndex - 1;
    }
  }

  //? move down layer item canvas
  void moveDownWidget() {
    int activeIndex = getIndexActiveEditMode();
    if (activeIndex == widgetsData.length - 1) {
      return;
    }
    var temp = widgetsData[activeIndex];
    widgetsData.removeAt(activeIndex);
    widgetsData.insert(activeIndex + 1, temp);
    widgetsData.refresh();
    indexEditMode.value = activeIndex + 1;
  }

  //? reset rotasi item canvas
  void resetRotation() {
    var idx = getIndexActiveEditMode();
    var temp = CanvasItemModels.fromJson(widgetsData[idx]).obs;
    if (temp.value.rotation == 0) {
      return;
    }

    var newRotation = vm.radians(0);

    widgetsData[idx]['rotation'] = newRotation;
    angle.value = newRotation;
    widgetsData.refresh();
    saveState();
    // var zxc = Matrix4Transform.from(Matrix4.fromFloat64List(temp.value.matrix)).rotate(
    //   -temp.value.rotation,
    //   origin: Offset(
    //     Get.width / 2,
    //     Get.height / 2,
    //   ),
    // );
    // var decompose = MxGestureDetector.decomposeToValues(zxc.matrix4);
    // widgetsData[idx]['matrix'] = zxc.matrix4.storage;
    // widgetsData[idx]['dx'] = decompose.translation.dx;
    // widgetsData[idx]['dy'] = decompose.translation.dy;
    // widgetsData[idx]['scale'] = decompose.scale;
    // widgetsData[idx]['rotation'] = decompose.rotation;
    // widgetsData[idx]['matrix'] = zxc.matrix4.storage;
    // widgetsData[idx]['imageWidgetBool'] = true;
    // widgetsData.refresh();
    // saveState();
  }

  //? menambah rotasi sebanyak 45 derajat
  void rotation45Degree() {
    // var idx = getIndexActiveEditMode();
    var rotation = widgetsData[indexEditMode.value]['rotation'];
    var listDegree = [0.0, 45.0, 90.0, 135.0, 180.0, 225.0, 270.0, 315.0];
    var degree = vm.degrees(rotation);
    var i = listDegree.indexWhere((element) => element == degree);

    var isContains = listDegree.contains(degree);
    late double newRotation;

    if (!isContains) {
      newRotation = vm.radians(45);
      widgetsData[indexEditMode.value]['rotation'] = newRotation;
      angle.value = newRotation;
      widgetsData.refresh();
    } else {
      if (i == listDegree.length - 1) {
        newRotation = vm.radians(listDegree[0]);
      } else {
        newRotation = vm.radians(listDegree[i + 1]);
      }
      widgetsData[indexEditMode.value]['rotation'] = newRotation;
      angle.value = newRotation;
      widgetsData.refresh();
    }

    saveState();
  }

  //? untuk crop gambar canvas
  void cropImage() async {
    var idx = getIndexActiveEditMode();
    var data = ImageWidgetDataModels.fromJson(widgetsData[idx]['data']);
    var file = await ImageCropper().cropImage(
      sourcePath: data.path,
    );
    if (file != null) {
      widgetsData[idx]['data'] =
          ImageWidgetDataModels(path: file.path).toJson();
      widgetsData.refresh();
      saveState();
    }
  }

  //? get widget height by index
  getSize(int index) {
    final RenderBox a =
        listGlobalKey[index].currentContext!.findRenderObject()! as RenderBox;
    if (widgetsData[index]['scale'] != 0) {
      var width = a.size.width * widgetsData[index]['scale'];
      var height = a.size.height * widgetsData[index]['scale'];
      return {
        'width': width,
        'height': height,
      };
    } else {
      return {
        'width': a.size.width,
        'height': a.size.height,
      };
    }
  }

  //? brush section
  //? data brush
  // RxList<DrawPoint?> drawPoint = (List<DrawPoint?>.of([])).obs;
  // var drawPoint = <DrawPoint?>[].obs;
  var drawPoint = [].obs;

  //* brush functions
  bool checkBrushExist() {
    bool isExist = false;
    for (var i = 0; i < widgetsData.length; i++) {
      if (widgetsData[i]['type'] == CanvasItemType.BRUSH_BASE) {
        isExist = true;
        break;
      }
    }
    return isExist;
  }

  var yBrush = [].obs;
  var xBrush = [].obs;

  var appDir = ''.obs;

  var yMin = 0.0.obs;
  var xMin = 0.0.obs;

  void doneBrush() async {
    var edge = 10.0;
    var yMin = getMin(yBrush.cast<num>()).toDouble();
    var yMax = getMax(yBrush.cast<num>()).toDouble();
    var xMin = getMin(xBrush.cast<num>()).toDouble();
    var xMax = getMax(xBrush.cast<num>()).toDouble();
    var height = yMax - yMin + edge;
    var width = xMax - xMin + edge;
    var temp = [];
    for (var i = 0; i < drawPoint.length; i++) {
      var a = drawPoint[i];
      if (a != null) {
        a['dy'] = a['dy'] - yMin + (edge / 2);
        a['dx'] = a['dx'] - xMin + (edge / 2);
      }
      temp.add(a);
    }

    var uuid = const Uuid().v4();
    var path = '${appDir.value}/$uuid.png';
    var resBrushImage =
        await drawerToImage(temp, height: height.round(), width: width.round());
    var newbytes = base64Decode(resBrushImage);
    File newfile = File(path);
    await newfile.writeAsBytes(newbytes);
    isBrushMode.value = false;
    isStroke.value = false;
    isDraw.value = true;

    widgetsData.last['data'] = {
      'image_brush_path': path,
    };
    widgetsData.last['dy'] = yMin;
    widgetsData.last['configDy'] = yMin;
    widgetsData.last['dx'] = xMin;
    widgetsData.last['configDx'] = xMin;
    widgetsData.last['default_height'] = height;
    widgetsData.last['default_width'] = width;
    widgetsData.last['height'] = height;
    widgetsData.last['width'] = width;
    widgetsData.last['top_edge'] = yMin;
    widgetsData.last['bottom_edge'] = yMax;
    widgetsData.last['left_edge'] = xMin;
    widgetsData.last['right_edge'] = xMax;
    widgetsData.last['insert_done'] = true;
    widgetsData.last['edit_mode'] = false;

    widgetsData.refresh();
    drawPoint.clear();
    xMin = 0.0;
    yMin = 0.0;
    yBrush.clear();
    xBrush.clear();
    saveState();
    logKey('data brush', widgetsData.last);
  }

  RxList<Color> history = (List<Color>.of([])).obs;

  void showColorPicker({bool isTextColor = false}) async {
    int tempColor = latestSelectedColor.value;
    var config = const cyc.ColorPickerConfig(
      enableEyePicker: true,
      enableLibrary: false,
      enableOpacity: false,
    );

    var res = await Get.dialog(
      Dialog(
        child: SingleChildScrollView(
          child: Column(
            children: [
              // DefText('colorPicker').normal,
              ColorPicker(
                colorHistory: history,
                pickerColor: Color(tempColor),
                onHistoryChanged: (value) => history.value = value,
                onColorChanged: (color) {
                  tempColor = color.value;
                },
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                // mainAxisSize: MainAxisSize.min,
                children: [
                  Material(
                    child: InkWell(
                      onTap: () {
                        Get.back(
                          result: true,
                        );
                      },
                      child: Container(
                        height: 30,
                        width: 70,
                        decoration: const BoxDecoration(
                          color: Colors.lightBlue,
                          borderRadius: kDefaultBorderRadius,
                        ),
                        child: Center(
                          child: DefText('Pilih').large,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 10),
                  Material(
                    child: InkWell(
                      onTap: () {
                        Get.back(
                          result: false,
                        );
                      },
                      child: Container(
                        height: 30,
                        width: 70,
                        decoration: const BoxDecoration(
                          color: Colors.lightBlue,
                          borderRadius: kDefaultBorderRadius,
                        ),
                        child: Center(
                          child: DefText('Kembali').large,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
    if (res) {
      if (isTextColor) {
        var index = getIndexActiveTextEdit();
        widgetsData[index]['data']['color'] = tempColor;
        widgetsData.refresh();
        logKey('masuk sini');
        return;
      }
      latestSelectedColor.value = tempColor;
    }
  }
  //* end brush functions

  void panUpdate(int index, double dx, double dy) {
    widgetsData[index]['dx'] = widgetsData[index]['dx'] + dx;
    widgetsData[index]['dy'] = widgetsData[index]['dy'] + dy;
    widgetsData.refresh();
  }

  void panEnd() {
    // redoStates.clear();
    saveState();
  }

  void redoBrush() {
    var res = box.read('kBrushState');
    redoStateBrush.assignAll(res);
    var a = [];
    for (var i = 0; i < redoStateBrush.length; i++) {
      if (redoStateBrush[i] == null) {
        a.add(i);
      }
    }
    logKey('isi null pointer redoState', a);
    if (a.isEmpty) {
      return;
    }
    if (a.length > 2) {
      var start = a[a.length - 2];
      var last = a.last;
      var temp = redoStateBrush.getRange(start, last);
      drawPoint.addAll(temp);
      redoStateBrush.removeRange(start, last);
      return;
    }
    if (a.length == 2) {
      redoStateBrush.removeRange(a.first, a.last);
      return;
    }
    redoStateBrush.clear();
  }

  //* undo redo v2

  Future<bool> filePermission() async {
    var a = Permission.accessMediaLocation;
    var b = await a.isGranted;
    if (await a.isDenied) {
      var newStat = await a.request();
      if (newStat.isGranted) {
        b = true;
      }
    }
    return b;
  }

  var slidingOpacity = 0.0.obs;

  //* Text variables
  Timer? timer;
  ScrollController textScrolLController = ScrollController();
  PanelController panelTextController = PanelController();
  // var testcontroller = SocialTextEditingController()..setTextStyle(DetectedType.mention, TextStyle(color: Colors.purple,backgroundColor: Colors.purple.withAlpha(50)));
  late RichTextController rcontroller;

  //! commented
  // Rx<TextEditingController> textEditingController = TextEditingController(text: 'Tap to edit').obs;

  // TextEditingController textEditingController = TextEditingController(text: 'Tap to edit');
  var listFonts = [].obs;
  //* text functions
  void getGoogleFonts() async {
    var fonts = GoogleFonts.asMap().keys.toList();
    listFonts.assignAll(fonts);
  }

  void cutText() {
    // logKey('msk selectioText');
    var index = indexEditMode.value;
    widgetsData[index]['data']['isCut'] = !widgetsData[index]['data']['isCut'];
    if (!widgetsData[index]['data']['isCut']) {
      return;
    }
    var cursorPosition = rcontroller.selection.base.offset;
    logKey('cursorPosition', cursorPosition);
    var z = TextSelection(
        baseOffset: cursorPosition, extentOffset: rcontroller.text.length);
    rcontroller.selection = z;
    widgetsData[index]['data']['trim_start'] = cursorPosition;
    // exitEditMode();
  }

  Future<void> openTextEditing() async {
    tempMinHeigh.value = 100;
    await panelTextEditing.show();
    panelTextEditing.animatePanelToSnapPoint(
      duration: kDefaultFastDuration,
      curve: kDefaultCurve,
    );
  }

  Future<void> closeTextEditing() async {
    await panelTextEditing.hide();
  }

  bool checkTextEditActive() {
    var isEditModeExist = false;
    for (var i = 0; i < widgetsData.length; i++) {
      if (widgetsData[i]['type'] == CanvasItemType.TEXT &&
          widgetsData[i]['edit_mode']) {
        isEditModeExist = true;
        break;
      }
    }
    return isEditModeExist;
  }

  int getIndexActiveTextEdit() {
    int index = -1;
    for (var i = 0; i < widgetsData.length; i++) {
      if (widgetsData[i]['type'] == CanvasItemType.TEXT &&
          widgetsData[i]['edit_mode']) {
        index = i;
        break;
      }
    }
    return index;
  }

  // void saveText() async {
  //   // isKeybordShowed.value = false;
  //   // await panelTextEditing.show();
  //   Get.focusScope!.unfocus();
  //   panelTextEditing.hide();

  //   isTextEditMode.value = !isTextEditMode.value;
  //   // var indexActive = getIndexActiveTextEdit();
  //   if (tempEditingText.value == '') {
  //     widgetsData[indexEditMode.value]['edit_mode'] = false;
  //     // widgetsData.refresh();
  //     tempEditingText.value = '';
  //     saveState();
  //     // botNavIndex.value = -1;
  //     return;
  //   }
  //   widgetsData[indexEditMode.value]['data']['long_text'] = tempEditingText.value;
  //   widgetsData[indexEditMode.value]['edit_mode'] = false;
  //   // widgetsData.refresh();
  //   tempEditingText.value = '';
  //   saveState();
  //   // botNavIndex.value = -1;
  // }

  void saveText(String value, int index) {
    // if (isEmpty(value)) {
    //   deleteWidget(index: index);
    //   panelTextEditing.hide();
    //   return;
    // }
    tempEditingText.value = value;
    widgetsData[index]['data']['long_text'] = value;
    logKey('saveText', widgetsData[index]['data']['long_text']);
    widgetsData[index]['data']['isFirstAdd'] = false;
  }

  void changedTextEditing(int index) {
    textEditIndex.value = index;
    // panelTextEditing.open();
  }

  final FilterOptionGroup _filterOptionGroup = FilterOptionGroup(
    imageOption: const FilterOption(
      sizeConstraint: SizeConstraint(ignoreSize: true),
    ),
  );
//*  Image variables
  var tempMinHeigh = 0.0.obs;
  ScrollController imageScrollController = ScrollController();
  PanelController panelImageController = PanelController();
  RxList<AssetPathEntity> paths = (List<AssetPathEntity>.of([])).obs;
  RxList<AssetEntity> listImages = (List<AssetEntity>.of([])).obs;
  var isBottom = false.obs;
  var imagePageIndex = 0.obs;

  //* Image functions
  Future<void> paginationImage() async {
    var res =
        await paths[0].getAssetListPaged(page: imagePageIndex.value, size: 40);
    listImages.addAll(res);
  }

  void openImage() async {
    // imageScrollController = ScrollController();
    // tempMinHeigh.value = Get.height * 0.3;
    tempMinHeigh.value = 100;
    await panelImageController.show();
    panelImageController.animatePanelToSnapPoint(
      duration: kDefaultFastDuration,
      curve: kDefaultCurve,
    );
    imageScrollController.addListener(() async {
      if (imageScrollController.position.pixels ==
          imageScrollController.position.maxScrollExtent) {
        imagePageIndex.value++;
        await paginationImage();
        isBottom.value = true;
      }
    });
    final List<AssetPathEntity> temp = await PhotoManager.getAssetPathList(
      onlyAll: true,
      type: RequestType.image,
      filterOption: _filterOptionGroup,
    );
    paths.addAll(temp);
    var imagesEntity =
        await paths[0].getAssetListPaged(page: imagePageIndex.value, size: 40);
    listImages.addAll(imagesEntity);
  }

  Future<void> closeImage() async {
    // imageScrollController.dispose();
    await panelImageController.hide();
    isBottom.value = false;
    imagePageIndex.value = 0;
    listImages.clear();
    // botNavIndex.value = -1;
  }

  //* Gif functions
  void getGiphy() async {
    var res = await GiphyGet.getGif(
      context: Get.context!,
      apiKey: giphyKey,
      // randomID: 'abcd',
    );
    // botNavIndex.value = -1;
    // return;
    if (res != null) {
      var url = res.images!.original!.url;
      var height = res.images!.original!.height;
      var width = res.images!.original!.width;
      logKey('gif data url', url);
      logKey('gif data height', height);
      logKey('gif data width', width);
      addWidget(
        type: CanvasItemType.GIF,
        data: GifWidgetDataModels(url: res.images!.original!.url).toJson(),
      );
    }
  }

  //* Background variable
  ScrollController bgScrollController = ScrollController();
  PanelController panelBgController = PanelController();
  PageController pageBgController = PageController();
  RxList<AssetPathEntity> pathsBg = (List<AssetPathEntity>.of([])).obs;
  RxList<AssetEntity> listImagesBg = (List<AssetEntity>.of([])).obs;
  var bgColor = ''.obs;
  var isBgImagePick = false.obs;
  var isBottomBg = false.obs;
  var bgImagePageIndex = 0.obs;
  var bgPageViewIndex = 0.obs;

  // var bgData = {}.obs;
  var bgImagePath = ''.obs;

  //* Background functions

  // var backgroundData = {}.obs;
  var backgroundData = BackgroundWidgetModel(
    // color: Colors.white.value.toRadixString(16).replaceRange(0, 2, '#'),
    color: '',
    url: '',
  ).obs;

  Future<void> paginationBg() async {
    logKey('pagination bg called');
    var res = await paths[0]
        .getAssetListPaged(page: bgImagePageIndex.value, size: 40);
    listImagesBg.addAll(res);
  }

  void selectBgColor({required String colorCode}) async {
    // backgroundData.value.url = '';
    deleteBackground();
    colorCode = colorCode.replaceRange(0, 2, '#');
    bgColor.value = colorCode;
    backgroundData.value.color = colorCode;
    backgroundData.refresh();
    // closeBg();
    await panelBgController.animatePanelToPosition(
      0,
      duration: kDefaultFastDuration,
      curve: kDefaultCurve,
    );
    // botNavIndex.value = -1;
  }

  void selectBgImage({required String path}) {
    backgroundData.value.color = '';
    backgroundData.value.url = path;
    backgroundData.refresh();
    saveState();
  }

  void openBg() async {
    bgPageViewIndex.value = 0;
    tempMinHeigh.value = 100;
    await panelBgController.show();
    panelBgController.animatePanelToSnapPoint(
      duration: kDefaultFastDuration,
      curve: kDefaultCurve,
    );
    bgScrollController.addListener(() async {
      logKey('bgScrollController.position.pixels',
          bgScrollController.position.pixels);
      logKey('bgScrollController.position.maxScrollExtent',
          bgScrollController.position.maxScrollExtent);
      if (bgScrollController.position.pixels ==
          bgScrollController.position.maxScrollExtent) {
        bgImagePageIndex.value++;
        await paginationBg();
        isBottomBg.value = true;
      }
    });
    final List<AssetPathEntity> temp = await PhotoManager.getAssetPathList(
      onlyAll: true,
      type: RequestType.image,
      filterOption: _filterOptionGroup,
    );
    pathsBg.addAll(temp);
    var bgEntity = await pathsBg[0]
        .getAssetListPaged(page: bgImagePageIndex.value, size: 40);
    listImagesBg.addAll(bgEntity);
  }

  Future<void> closeBg() async {
    await panelBgController.hide();
    isBottomBg.value = false;
    bgImagePageIndex.value = 0;
    listImagesBg.clear();
  }

  void deleteBackground() {
    backgroundData.value.url = '';
    backgroundData.refresh();
  }
}

//! commented function

//* undo redo ver 1

// void saveState({String type = 'widget'}) async {
//   logKey('saveState', widgetsData);
//   if (type == 'brush') {
//     undoStates.add('brush');
//     return;
//   }
//   undoStates.add(json.encode(widgetsData));
// }

// void undo() {
//   redoStates.add(undoStates.last);
//   undoStates.removeLast();
//   if (undoStates.isEmpty) {
//     widgetsData.clear();
//     return;
//   }
//   var temp = json.decode(undoStates.last);
//   widgetsData.assignAll(temp);
// }

// void redo() {
//   var temp = json.decode(redoStates.last);
//   widgetsData.assignAll(temp);
//   redoStates.removeLast();
//   saveState();
// }

// void undoBrush() async {
//   // test.addAll(['zzz', 'xxx', 'ccc']);
//   var undoC = Get.put(UndoController());
//   // await box.write('test', test.value);
//   var tez = box.read('test');
//   logKey('tez', tez);
//   var a = [];
//   for (var i = 0; i < drawPoint.length; i++) {
//     if (drawPoint[i] == null) {
//       a.add(i);
//     }
//   }

//   if (a.isEmpty) {
//     return;
//   }
//   if (a.length > 2) {
//     var start = a[a.length - 2];
//     var last = a.last;
//     //* buat redo
//     var temp = drawPoint.getRange(start, last);
//     undoC.redoStateBrush.addAll(temp);
//     await box.write('kBrushState', undoC.redoStateBrush);
//     Get.delete<UndoController>();
//     drawPoint.removeRange(start, last);
//     return;
//   }
//   if (a.length == 2) {
//     var temp = drawPoint.getRange(a.first, a.last);
//     // redoStateBrush.addAll(temp);
//     drawPoint.removeRange(a.first, a.last);
//     return;
//   }
//   drawPoint.clear();
// }

// void openImage() async {
//   scrollController.addListener(() {
//     if (scrollController.position.pixels == scrollController.position.maxScrollExtent) {
//       logKey("I'm Listening", scrollController.position.pixels);
//       imagePageIndex.value++;
//       paginationImage(imagePageIndex.value);
//       isBottom.value = true;
//       logKey('asd');
//     }
//   });

//   final List<AssetPathEntity> temp = await PhotoManager.getAssetPathList(
//     onlyAll: true,
//     // hasAll: true,
//     type: RequestType.image,
//     filterOption: _filterOptionGroup,
//   );
//   paths.addAll(temp);
//   var imagesEntity = await paths[0].getAssetListPaged(page: imagePageIndex.value, size: 20);
//   listImages.addAll(imagesEntity);
//   logKey('listImages length init', listImages.length);
//   await Get.bottomSheet(
//     CanvasImagesList(
//       scrollController: scrollController,
//     ),
//     // isScrollControlled: true,
//   );
//   scrollController.dispose();
//   isBottom.value = false;
//   imagePageIndex.value = 0;
//   listImages.clear();
//   botNavIndex.value = -1;
// }
/** Image End **/
