import 'package:codingtest/app/helpers/canvas_helper.dart';
import 'package:codingtest/app/modules/canvas/components/default_text.dart';
import 'package:codingtest/app/modules/canvas/controllers/canvas_controller.dart';
import 'package:codingtest/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var p = Get.put(CanvasController());
    return Scaffold(
        appBar: AppBar(
          title: const Text('HomeView'),
          centerTitle: true,
        ),
        body: Obx(
          () => SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                DefText('Meme Generator').large,
                const SizedBox(
                  height: 20.0,
                ),
                Expanded(
                  child: GridView.count(
                    crossAxisCount: 3,
                    children: controller.listImage
                        .map((data) => GestureDetector(
                              onTap: () async {
                                var file2 = await p.fileFromImageUrl(data.url);

                                p.addWidget(
                                  type: CanvasItemType.IMAGE,
                                  data: file2.path,
                                );
                                Get.toNamed(Routes.CANVAS);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: NetworkImage(data.url),
                                        fit: BoxFit.cover)),
                              ),
                            ))
                        .toList(),

                    //  Obx(
                    //   () => ListView.builder(
                    //     physics: const NeverScrollableScrollPhysics(),
                    //     shrinkWrap: true,
                    //     itemCount: controller.listMeme.value?.data.memes.length,
                    //     // controller: controller.scrollController,
                    //     itemBuilder: (context, index) {
                    //       return Column(
                    //         children: [
                    //           Container(
                    //             height: 500,
                    //             // color: Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1),
                    //             decoration: const BoxDecoration(),
                    //             child: GetX<HomeController>(
                    //               init: HomeController(),
                    //               builder: (ctrl) {
                    //                 List<Meme> dataMeme =
                    //                     ctrl.listMeme.value?.data.memes ?? [];
                    //                 return
                    //                     //  GridView.count(
                    //                     //   crossAxisCount: 3,
                    //                     //   children: dataMeme
                    //                     //       .map((data) => Container(
                    //                     //             decoration: BoxDecoration(
                    //                     //                 image: DecorationImage(
                    //                     //                     image: NetworkImage(data.url))),
                    //                     //           ))
                    //                     //       .toList(),
                    //                     // );

                    //                     Stack(
                    //                   children: dataMeme
                    //                       .map((data) => Container(
                    //                             decoration: BoxDecoration(
                    //                                 image: DecorationImage(
                    //                                     image: NetworkImage(data.url))),
                    //                           ))
                    //                       .toList(),
                    //                 );
                    //               },
                    //             ),
                    //             // child: Obx(
                    //             //   () {
                    //             //     return Stack(
                    //             //       fit: StackFit.expand,
                    //             //       children: controller.listTimeline.value.timeline[index].arrangedItems
                    //             //           .asMap()
                    //             //           .map((idx, value) {
                    //             //             return MapEntry(
                    //             //               idx,
                    //             //               TimelineItem(
                    //             //                 data: controller.listTimeline.value.timeline[index].arrangedItems[idx],
                    //             //               ),
                    //             //             );
                    //             //           })
                    //             //           .values
                    //             //           .toList(),
                    //             //     );
                    //             //   },
                    //             // ),
                    //           ),
                    //           const SizedBox(height: 10),
                    //         ],
                    //       );
                    //     },
                    //   ),
                    // ),
                  ),
                ),
              ],
            ),
          ),
        )

        //     GetX<HomeController>(
        //   init: HomeController(),
        //   builder: (ctrl) {
        //     List<Meme> dataMeme = ctrl.listMeme.value?.data.memes ?? [];
        //     return Stack(
        //       children: dataMeme.map((data) {
        //         return ListView.builder(
        //           itemCount: ,
        //             itemBuilder: (BuildContext context, index) {
        //           return Container(
        //             decoration: BoxDecoration(
        //                 image: DecorationImage(image: NetworkImage(data.url))),
        //           );
        //         });
        //       }).toList(),
        //     );
        //   },
        // ),
        );
  }
}
