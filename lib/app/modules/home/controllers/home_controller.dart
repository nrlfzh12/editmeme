import 'dart:developer';

import 'package:codingtest/app/model/image/get_list_meme_model.dart';
import 'package:codingtest/app/utils/network_utils.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart' as dio;

class HomeController extends GetxController {
  var netUtil = NetworkUtil.internal();
  var listMeme = Rxn<GetListMeme>();
  var listImage = <Meme>[].obs;

  // var listTimeline = GetListMeme(data: null).obs;

  //TODO: Implement HomeController

  final count = 0.obs;

  Future<void> getListMeme() async {
    log('masuk gaaa');
    try {
      dio.Response res = await netUtil.get('https://api.imgflip.com/get_memes');
      final Map<String, dynamic> jsonResult = res.data;

      log('getCategory res$jsonResult');
      log('getCategory res${res.statusCode}');
      listMeme.value = GetListMeme.fromJson(res.data);
      listImage.addAll(listMeme.value?.data.memes ?? []);
      log("${listImage.value}tess");

      // log(userprofil.value?.toJson().toString() ?? '' + "oooo");
      update();
    } on dio.DioError catch (e) {
      log('error getUserPost${e.message}');
    }
  }

  @override
  void onInit() {
    super.onInit();
    getListMeme();
  }

  void increment() => count.value++;

  @override
  void onClose() {}
}
