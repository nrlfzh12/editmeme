import 'dart:developer';
import 'dart:io';

import 'package:codingtest/app/helpers/canvas_helper.dart';
import 'package:codingtest/app/model/background_model/background_widget_model.dart';
import 'package:codingtest/app/model/create_post_model.dart';
import 'package:codingtest/app/model/image_model/image_widget_model.dart';
import 'package:codingtest/app/modules/canvas/components/default_text.dart';
import 'package:codingtest/app/utils/function_utils.dart';
import 'package:codingtest/app/utils/network_utils.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart' as p;
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class PostPreviewController extends GetxController
    with GetTickerProviderStateMixin {
  GlobalKey profileKey = GlobalKey();
  ScrollController sc = ScrollController();
  NetworkUtil networkUtil = NetworkUtil.internal();
  // PageController dialogPageController = PageController();
  var isLoading = false.obs;
  var canvasHeight = 0.0.obs;
  var canvasTop = 0.0.obs;
  var canvasBottom = 0.0.obs;
  var listPost = [].obs;

  var isInPosition = false.obs;
  var isAnimated = false.obs;
  var isVisible = false.obs;
  var duration = const Duration(milliseconds: 500);
  var selectedIndex = (0 - 1).obs;

  var canvasItems = [].obs;
  var canPopupImages = [].obs;
  var backgroundData = BackgroundWidgetModel(color: '', url: '').obs;

  var animatedY = 0.0.obs;
  var animatedX = 0.0.obs;
  var initialY = 0.0.obs;
  var profileHeight = 0.0.obs;
  var zoomHeight = 100.0.obs;
  var zoomWidth = 100.0.obs;
  var testScale = 1.0.obs;

  var url = ''.obs;
  var gifUrl = ''.obs;
  // var tappedIndex = 0.obs;
  var tappedIndex = (0 - 1).obs;

  late Animation<double> asd;
  late AnimationController ac;

  void initialFunction() {
    ac = AnimationController(
      vsync: this,
      duration: duration,
      value: 0.2,
    );
    asd = CurvedAnimation(
      parent: ac,
      curve: Curves.easeInOutCirc,
    );
    if (isNotEmpty(Get.arguments)) {
      var data = CreatePostModel.fromJson(Get.arguments);
      backgroundData.value = data.backgroud;
      backgroundData.refresh();
      canvasHeight.value = data.height;
      canvasTop.value = data.topHeight;
      canvasBottom.value = data.bottomHeight;
      canvasItems.addAll(data.images);
      canvasItems.addAll(data.texts);
      canvasItems.addAll(data.gifs);
      canvasItems.addAll(data.brushes);
      // if (isNotEmpty(data.brush)) {
      // canvasItems.add(BrushWidgetModel.fromJson(data.brush));
      // canvasItems.addAll(data.brush);
      // }
      canvasItems.sort((a, b) => a.index.compareTo(b.index));
      for (var e in canvasItems) {
        //* yang ada atribute canPopUp hanya tipe image dan gif
        if (e.type == CanvasItemType.IMAGE && e?.canPopUp) {
          canPopupImages.add(e);
        } else if (e.type == CanvasItemType.GIF && e?.canPopUp) {
          canPopupImages.add(e);
        }
      }
      logKey('canPOpupImages', canPopupImages);
    }
    // containsImage();
  }

  Future<File> getLocalFile(String pathFile) async {
    final root = await p.getApplicationDocumentsDirectory();
    final path = '$root/$pathFile}';
    return File(path).create(recursive: true);
  }

  void getInitialPosition(int i) {
    testScale.value = 0.9;
    // var actualHeight = canvasItems[i].height;
    var temp = canvasItems[i];
    // logKey('_temp', _temp);
    var actualHeight = canvasItems[i].height;
    var actualWidth = canvasItems[i].width;
    animatedY.value = profileHeight.value +
        canvasItems[i].top_edge -
        sc.offset -
        canvasTop.value;
    animatedX.value = canvasItems[i].left_edge;
    zoomHeight.value = actualHeight;
    zoomWidth.value = actualWidth;
    // testScale.value = 1.6;
  }

  // void onTep({required int index}) async {
  //   url.value = canvasItems[index].url;
  //   getInitialPosition(index);
  // }

  int checkIndex({required int canPopItemIndex}) {
    var res = 0;
    if (canPopItemIndex == -1) {
      return 0;
    }
    var dataFromCanvas =
        ImageWidgeModel.fromJson(canvasItems[canPopItemIndex].toJson());
    for (var i = 0; i < canPopupImages.length; i++) {
      var temp = ImageWidgeModel.fromJson(canPopupImages[i].toJson());
      if (temp.url == dataFromCanvas.url) {
        res = i;
        break;
      }
    }
    return res;
  }

  bool containsImage() {
    // logKey('msk sni');
    var res = false;
    for (var e in canvasItems) {
      if (e.type == CanvasItemType.IMAGE) {
        res = true;
        break;
      }
    }
    return res;
  }

  int canPopupCount() {
    var res = 0;
    for (var e in canPopupImages) {
      if (e.type == CanvasItemType.IMAGE) {
        res++;
      }
    }
    return res;
  }

  void onTep({required int index}) async {
    tappedIndex.value = index;
    var type = canvasItems[index].type;
    if (type == CanvasItemType.IMAGE) {
      gifUrl.value = '';
      url.value = canvasItems[index].url;
    } else {
      url.value = '';
      gifUrl.value = canvasItems[index].url;
    }
    isInPosition.value = false;
    isAnimated.value = true;
    getInitialPosition(index);
    await Future.delayed(duration);
    animatedY.value = 0.0;
    animatedX.value = 0.0;
    zoomHeight.value = Get.height;
    zoomWidth.value = Get.width;
    // checkIndex(canvasItemsIndex: index);
    // dialogPageController.jumpTo(1);
  }

  void zoomIn() {
    animatedY.value = 0.0;
  }

  @override
  void onInit() {
    super.onInit();
    initialFunction();
  }

  @override
  void onReady() {
    super.onReady();
    // var size = getSizeByKey(profileKey);
    // profileHeight.value = size['height'];
    animatedY.value = profileHeight.value;
  }

  @override
  void onClose() {
    // dialogPageController.dispose();
  }

  var percentage = 0.0.obs;

  // Future<String> uploadToAWS({required String imagePath}) async {
  //   var minio = Minio(
  //     endPoint: 's3.amazonaws.com',
  //     accessKey: awsAccessKeyId!,
  //     secretKey: awsSecretAccessKey!,
  //     region: awsRegion!,
  //   );
  //   var uuid = Uuid().v4();
  //   File _file = File(imagePath);
  //   var fileName = pathDart.basename(_file.path);
  //   var fileExtension = fileName.split('.').last;
  //   var bytes = _file.readAsBytesSync();
  //   var s = Stream.value(bytes);
  //   var res = await minio.putObject(
  //     '$s3Bucket',
  //     '$tmpFolder/$uuid.$fileExtension',
  //     s,
  //     onProgress: (currByte) {
  //       var _percentage = currByte / bytes.length * 100;
  //       // calcPercentage(a: currByte, b: bytes.length);
  //       percentage.value = _percentage;
  //       logKey('percentage', _percentage);
  //     },
  //   );
  //   logKey('minio res', res);

  //   // var key = '$tmpFolder/$res.$fileExtension';
  //   // var key = '$res.$fileExtension';
  //   var key = '$uuid.$fileExtension';
  //   return key;
  // }

  void createPost() async {
    var data = CreatePostModel.fromJson(Get.arguments);
    // var minio = Minio(
    //   endPoint: 's3.amazonaws.com',
    //   accessKey: awsAccessKeyId!,
    //   secretKey: awsSecretAccessKey!,
    //   region: awsRegion!,
    // );
    Get.dialog(
      Obx(
        () => Dialog(
          child: Container(
            child: DefText('${percentage.value}').normal,
          ),
        ),
      ),
    );

    var tempKeys = [];
    for (var img in data.images) {
      var imgData = img;

      img.key = imgData.key;
      tempKeys.add(imgData);
      percentage.value = 0.0;
    }

    // for (var brush in data.brushes) {
    //   var imgBrush = brush;
    //   var res = await uploadToAWS(imagePath: imgBrush.url);
    //   imgBrush.key = res;
    //   percentage.value = 0.0;
    // }

    // if (isNotEmpty(data.backgroud.url)) {
    //   await uploadToAWS(imagePath: data.backgroud.url);
    // }

    //* aws3_bucket works
    // try {
    //   var credential = IAMCrediental();
    //   credential.secretKey = awsAccessKeyId!;
    //   credential.secretId = awsSecretAccessKey!;
    //   var imageData = ImageData(
    //     fileName,
    //     _file.path,
    //   );
    //   var res = await Aws3Bucket.upload(
    //     '${s3Bucket!}/${tmpFolder!}',
    //     awsRegion!,
    //     awsRegion!,
    //     imageData,
    //     credential,
    //   );
    //   logKey('res', res);
    // } catch (e) {
    //   logKey('error', e.toString());
    // }

    var data2 = CreatePostModel(
      width: data.width,
      height: data.height,
      topHeight: data.topHeight,
      bottomHeight: data.bottomHeight,
      gifs: data.gifs,
      images: data.images,
      texts: data.texts,
      backgroud: data.backgroud,
      brushes: data.brushes,
      hashtags: data.hashtags,
      peoples: data.peoples,
    );
    if (GetStorage().read('post') != null) {
      listPost.value.add(GetStorage().read('post'));
    }
    //  else{
    //    listPost.value.add(GetStorage().read('post'));
    //  }

    listPost.add(data2);
    GetStorage().write('post', listPost);

    log("${GetStorage().read('post')}listt post");
    logKey('_data', data2.toJson());
    // return;
    // try {
    //   var res = await networkUtil.post(
    //     // '$baseUrl/post/create-with-tmp',
    //     '$baseUrl/post',
    //     body: _data.toJson(),
    //   );
    //   Get.close(3);
    //   logKey('res post', res);
    // } on DioError catch (e) {
    //   logKey('error post ', e.toString());
    //   showToast('create post error ${e.message}');
    //   Get.back();
    // }
  }

  // void createPost() async {
  //   isLoading.value = true;
  //   Map<String, dynamic> _body = {};
  //   var data = CreatePostModel.fromJson(Get.arguments);
  //   List<ImageWidgetModel> imageEncoded = [];
  //   for (ImageWidgetModel e in data.images) {
  //     File _file = File(e.path);
  //     List<int> _imageByts = _file.readAsBytesSync();
  //     var _base64 = base64Encode(_imageByts);
  //     var temp = ImageWidgetModel(
  //       index: e.index,
  //       // url: e.url,
  //       path: '',
  //       key: _base64,
  //       width: e.width,
  //       height: e.height,
  //       createdAt: e.createdAt,
  //       // top_edge: e.top_edge,
  //       // bottom_edge: e.bottom_edge,
  //       // left_edge: e.left_edge,
  //       // right_edge: e.right_edge,
  //       default_height: e.default_height,
  //       default_width: e.default_width,
  //     );
  //     imageEncoded.add(temp);
  //   }
  //   var a = CreatePostModel(
  //     width: Get.width,
  //     height: canvasHeight.value,
  //     topHeight: canvasTop.value,
  //     bottomHeight: canvasBottom.value,
  //     gifs: [],
  //     images: imageEncoded,
  //     texts: [],
  //     brush: data.brush,
  //     hastags: [],
  //     peoples: [],
  //     caption: 'Test Caption',
  //   );
  //   logKey('a', a.toJson());
  //   // print(a.images[0].toJson());
  //   var res = await networkUtil.post(
  //     '$baseUrl/post',
  //     body: a.toJson(),
  //   );
  //   logKey('res', res);
  //   isLoading.value = false;
  // }
}
