import 'dart:developer';
import 'dart:io';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:codingtest/app/helpers/canvas_helper.dart';
import 'package:codingtest/app/model/gif_model/gif_widget_model.dart';
import 'package:codingtest/app/model/image_model/image_widget_model.dart';
import 'package:codingtest/app/modules/canvas/components/canvas_item_global.dart';
import 'package:codingtest/app/modules/canvas/components/default_placeholder.dart';
import 'package:codingtest/app/utils/function_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../config/constants.dart';
import '../../canvas/components/default_text.dart';
import '../controllers/post_preview_controller.dart';

class PostPreviewView extends GetView<PostPreviewController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: DefText(
          'Post Preview',
          fontWeight: FontWeight.bold,
        ).large,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: const Icon(
            Icons.arrow_back,
            color: kBgBlack,
          ),
        ),
        actions: [
          Row(
            children: [
              Material(
                color: kBgBlack,
                borderRadius: BorderRadius.circular(2),
                child: InkWell(
                  onTap: () {
                    controller.createPost();
                  },
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                    child: DefText(
                      'Finish',
                      color: kBgWhite,
                    ).normal,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(width: 16),
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     // controller.createPost();
      //     controller.uploadToAWS(imagePath: controller.backgroundData.value.url);
      //   },
      // ),
      body: Stack(
        children: [
          SingleChildScrollView(
            controller: controller.sc,
            child: Column(
              children: [
                // Stack(
                //   children: [
                //     //* overlay profile
                //     // Container(
                //     //   key: controller.profileKey,
                //     //   decoration: BoxDecoration(
                //     //       // color: Colors.grey,
                //     //       ),
                //     //   //* Profile
                //     //   child: ProfileHeader(
                //     //     isPreview: true,
                //     //   ),
                //     // ),
                //     Positioned.fill(
                //       child: Opacity(
                //         opacity: 0.25,
                //         child: Container(
                //           height: double.infinity,
                //           color: Colors.grey.shade500,
                //         ),
                //       ),
                //     ),
                //   ],
                // ),
                //* Canvas
                Obx(
                  () => Container(
                    height: controller.canvasHeight.value,
                    // height: 300,
                    // height: 603.4285714285714,
                    // width: 411.42857142857144,
                    // color: Colors.amber,
                    decoration: BoxDecoration(
                      color: isNotEmpty(controller.backgroundData.value.color)
                          ? hexToColor(controller.backgroundData.value.color)
                          : kBgWhite,
                      image: isNotEmpty(controller.backgroundData.value.url)
                          ? DecorationImage(
                              image: FileImage(
                                File(
                                  controller.backgroundData.value.url,
                                ),
                              ),
                              fit: BoxFit.cover,
                            )
                          : null,
                    ),
                    child: Stack(
                      fit: StackFit.expand,
                      children: controller.canvasItems
                          .asMap()
                          .map(
                            (idx, value) {
                              return MapEntry(
                                idx,
                                Positioned(
                                  left: controller.canvasItems[idx].x_axis,
                                  top: controller.canvasItems[idx].y_axis -
                                      controller.canvasTop.value,
                                  child: Transform.rotate(
                                    angle: controller.canvasItems[idx].rotation,
                                    child: Transform.scale(
                                      scale: controller.canvasItems[idx].scale,
                                      child: GestureDetector(
                                        onTap: () {
                                          logKey(
                                              'tep',
                                              controller.canvasItems[idx]
                                                  .toJson());
                                          logKey('controller.isAnimated',
                                              controller.isAnimated);
                                          Map<String, dynamic> a = controller
                                              .canvasItems[idx]
                                              .toJson();
                                          var canPopUp =
                                              !a.containsKey('can_pop_up')
                                                  ? false
                                                  : a['can_pop_up'];
                                          logKey('canPopUp', canPopUp);
                                          if (controller.isAnimated.isFalse &&
                                              // controller.canvasItems[idx].type == CanvasItemType.IMAGE &&
                                              canPopUp) {
                                            controller.onTep(
                                              index: idx,
                                            );
                                          }
                                        },
                                        child: CanvasItemGlobal(
                                          data: controller.canvasItems[idx]
                                              .toJson(),
                                          isLocal: true,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          )
                          .values
                          .toList(),
                    ),
                  ),
                ),
              ],
            ),
          ),
          //* dialog untuk image
          if (controller.canPopupImages.isNotEmpty)
            GetX<PostPreviewController>(
              builder: (ctrl) {
                PageController pageController = PageController(
                    // initialPage: controller.checkIndex(canPopItemIndex: controller.tappedIndex.value),
                    );
                return AnimatedPositioned(
                  duration: !controller.isInPosition.value
                      ? const Duration(microseconds: 1)
                      : controller.duration,
                  height: controller.zoomHeight.value,
                  width: controller.zoomWidth.value,
                  top: controller.animatedY.value,
                  left: controller.animatedX.value,
                  onEnd: () async {
                    if (controller.isInPosition.isFalse) {
                      controller.isInPosition.value = true;
                      controller.isVisible.value = true;
                    } else {
                      controller.isAnimated.value = false;
                      controller.isVisible.value = false;
                      controller.isInPosition.value = false;
                      await Get.dialog(
                        transitionDuration: const Duration(milliseconds: 250),
                        transitionCurve: Curves.easeInOutCirc,
                        BackdropFilter(
                          filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                          child: Dialog(
                            backgroundColor: Colors.transparent,
                            insetPadding:
                                const EdgeInsets.symmetric(vertical: 25),
                            child: Container(
                              child: SingleChildScrollView(
                                physics: const NeverScrollableScrollPhysics(),
                                child: Column(
                                  children: [
                                    Obx(
                                      () => Container(
                                        constraints: BoxConstraints(
                                            maxHeight: Get.height * 0.75),
                                        child: PageView.builder(
                                          itemCount:
                                              controller.canPopupImages.length,
                                          // itemCount: controller.canPopupCount(),
                                          // controller: controller.dialogPageController,
                                          controller: pageController,
                                          scrollDirection: Axis.horizontal,
                                          itemBuilder: (context, index) {
                                            var temp = controller
                                                .canPopupImages[index]
                                                .toJson();
                                            if (temp['type'] ==
                                                CanvasItemType.IMAGE) {
                                              var data =
                                                  ImageWidgeModel.fromJson(
                                                      controller
                                                          .canPopupImages[index]
                                                          .toJson());
                                              log("${data.url} data imagee");

                                              var url = data.url;
                                              return Container(
                                                child: Image.file(
                                                  File(url),
                                                  // File.fromUri(
                                                  //   Uri.parse(url),
                                                  // ),
                                                ),
                                              );
                                            } else {
                                              var data =
                                                  GifWidgetModel.fromJson(
                                                      controller
                                                          .canPopupImages[index]
                                                          .toJson());
                                              return Container(
                                                child: CachedNetworkImage(
                                                  imageUrl: data.url,
                                                  httpHeaders: const {
                                                    'accept': 'image/*'
                                                  },
                                                  placeholder: (context, url) {
                                                    return const DefaultPlaceholder(
                                                      height: 150,
                                                      width: 150,
                                                    );
                                                  },
                                                ),
                                              );
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                    const SizedBox(height: 10),
                                    Obx(
                                      () => SizedBox(
                                        height: 100,
                                        child: ListView.separated(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10),
                                          shrinkWrap: true,
                                          itemCount:
                                              controller.canPopupImages.length,
                                          scrollDirection: Axis.horizontal,
                                          physics: const BouncingScrollPhysics(
                                            parent:
                                                AlwaysScrollableScrollPhysics(),
                                          ),
                                          separatorBuilder: (context, index) =>
                                              const SizedBox(
                                            width: 5,
                                          ),
                                          itemBuilder: (context, index) {
                                            // logKey('items $index', controller.canPopupImages[index]);
                                            var temp = controller
                                                .canPopupImages[index]
                                                .toJson();
                                            if (temp['type'] ==
                                                CanvasItemType.IMAGE) {
                                              var data =
                                                  ImageWidgeModel.fromJson(
                                                      controller
                                                          .canPopupImages[index]
                                                          .toJson());
                                              logKey('items $index', data.url);

                                              var url = data.url;
                                              return GestureDetector(
                                                onTap: () {
                                                  // controller.url.value = url;
                                                  pageController.animateToPage(
                                                    index,
                                                    duration:
                                                        controller.duration,
                                                    curve: Curves.easeInOutCirc,
                                                  );
                                                },
                                                child: Container(
                                                  child: Image.file(
                                                    File.fromUri(
                                                      Uri.parse(url),
                                                    ),
                                                  ),
                                                ),
                                              );
                                            } else {
                                              var data =
                                                  GifWidgetModel.fromJson(
                                                      controller
                                                          .canPopupImages[index]
                                                          .toJson());
                                              return GestureDetector(
                                                onTap: () {
                                                  pageController.animateToPage(
                                                    index,
                                                    duration:
                                                        controller.duration,
                                                    curve: Curves.easeInOutCirc,
                                                  );
                                                },
                                                child: Container(
                                                  child: CachedNetworkImage(
                                                    imageUrl: data.url,
                                                    httpHeaders: const {
                                                      'accept': 'image/*'
                                                    },
                                                    placeholder:
                                                        (context, url) {
                                                      return const DefaultPlaceholder(
                                                        height: 150,
                                                        width: 150,
                                                      );
                                                    },
                                                  ),
                                                ),
                                              );
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                    const SizedBox(height: 25),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                      pageController.dispose();
                    }
                  },
                  child: IgnorePointer(
                    child: AnimatedCrossFade(
                      duration: const Duration(milliseconds: 250),
                      layoutBuilder:
                          (topChild, topChildKey, bottomChild, bottomChildKey) {
                        return topChild;
                      },
                      crossFadeState: controller.isVisible.value
                          ? CrossFadeState.showFirst
                          : CrossFadeState.showFirst,
                      firstChild: Opacity(
                        opacity: controller.isInPosition.value &&
                                (controller.url.value != '' ||
                                    controller.gifUrl.value != '')
                            ? 1
                            : 0,
                        child: Obx(
                          () => SizedBox(
                            width: 250,
                            child: controller.gifUrl.value != ''
                                ? CachedNetworkImage(
                                    imageUrl: controller.gifUrl.value,
                                    placeholder: (context, url) {
                                      return const DefaultPlaceholder(
                                          height: 100, width: 100);
                                    },
                                  )
                                : Image.file(
                                    File.fromUri(
                                      Uri.parse(
                                        controller.url.value,
                                      ),
                                    ),
                                  ),
                          ),
                        ),
                      ),
                      secondChild: Container(
                        height: 100,
                        width: 100,
                        color: Colors.amber,
                      ),
                    ),
                  ),
                );
              },
            ),
        ],
      ),
    );
  }
}
