import 'package:codingtest/app/modules/canvas/bindings/canvas_binding.dart';
import 'package:codingtest/app/modules/canvas/views/canvas_view.dart';
import 'package:codingtest/app/modules/post_preview/bindings/post_preview_binding.dart';
import 'package:codingtest/app/modules/post_preview/views/post_preview_view.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    // GetPage(
    //   name: _Paths.HOME,
    //   page: () => HomeView(),
    //   binding: HomeBinding(),
    // ),
    GetPage(
      name: _Paths.CANVAS,
      page: () => const CanvasView(),
      binding: CanvasBinding(),
    ),
    // GetPage(
    //   name: _Paths.TIMELINE,
    //   page: () => TimelineView(),
    //   binding: TimelineBinding(),
    // ),
    GetPage(
      name: _Paths.CANVAS_PREVIEW,
      page: () => PostPreviewView(),
      binding: PostPreviewBinding(),
    ),
    // GetPage(
    //   name: _Paths.PROFILE,
    //   page: () => ProfileView(),
    //   binding: ProfileBinding(),
    // ),
    // GetPage(
    //   name: _Paths.LOGIN,
    //   page: () => const LoginView(),
    //   binding: LoginBinding(),
    // ),
    // GetPage(
    //   name: _Paths.SIGNUP,
    //   page: () => const SignupView(),
    //   binding: SignupBinding(),
    // ),
    // GetPage(
    //   name: _Paths.VERIFY,
    //   page: () => const VerifyView(),
    //   binding: SignupBinding(),
    // ),
    // GetPage(
    //   name: _Paths.SET_IDENTITY,
    //   page: () => const SetIdentityView(),
    //   binding: SetIdentityBinding(),
    // ),
    // GetPage(
    //   name: _Paths.SET_PROFILE_PICTURE,
    //   page: () => const ProfilePictureView(),
    //   binding: SignupBinding(),
    // ),
    // GetPage(
    //   name: _Paths.SET_FOLLOW_PEOPLE,
    //   page: () => const SetFollowPeopleView(),
    //   binding: SetFollowPeopleBinding(),
    //   children: [
    //     GetPage(
    //       name: _Paths.SET_FOLLOW_PEOPLE,
    //       page: () => const SetFollowPeopleView(),
    //       binding: SetFollowPeopleBinding(),
    //     ),
    //   ],
    // ),
    // GetPage(
    //   name: _Paths.ONBOARDING,
    //   page: () => const OnboardingView(),
    //   binding: OnboardingBinding(),
    // ),
    // GetPage(
    //   name: _Paths.ONBOARDING2,
    //   page: () => const Onboarding2View(),
    //   binding: OnboardingBinding(),
    // ),
    // GetPage(
    //   name: _Paths.NOTIFICATION,
    //   page: () => const NotificationView(),
    //   binding: NotificationBinding(),
    // ),
    // GetPage(
    //   name: _Paths.SPLASH,
    //   page: () => const SplashView(),
    //   binding: SplashBinding(),
    // ),
    // GetPage(
    //   name: _Paths.FEED,
    //   page: () => const FeedView(),
    //   binding: FeedBinding(),
    // ),
    // GetPage(
    //   name: _Paths.SEARCH,
    //   page: () => const SearchView(),
    //   binding: SearchBinding(),
    // ),
    // GetPage(
    //   name: _Paths.MYFOLLOWER,
    //   page: () => const MyfollowerView(),
    //   binding: MyfollowerBinding(),
    // ),
    // GetPage(
    //   name: _Paths.MYFOLLOWING,
    //   page: () => const MyfollowingView(),
    //   binding: MyfollowingBinding(),
    // ),
    // GetPage(
    //   name: _Paths.SETTING_PROFILE,
    //   page: () => const SettingProfileView(),
    //   binding: SettingProfileBinding(),
    // ),
    // GetPage(
    //   name: _Paths.COMMENT,
    //   page: () => const CommentView(),
    //   binding: CommentBinding(),
    // ),
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
  ];
}
