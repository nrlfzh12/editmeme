part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const CANVAS = _Paths.CANVAS;
  static const CANVAS_PREVIEW = _Paths.CANVAS_PREVIEW;
}

abstract class _Paths {
  static const HOME = '/home';
  static const CANVAS = '/canvas';
  static const CANVAS_PREVIEW = '/canvas-preview';
}
