import 'package:codingtest/app/utils/dio_utils.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:get_storage/get_storage.dart';

import 'function_utils.dart';

class NetworkUtil {
  static final NetworkUtil _instance = NetworkUtil.internal();
  NetworkUtil.internal();

  factory NetworkUtil() => _instance;

  // Dio dio = Dio();
  var dio = DioUtil.getInstance();

  var token = GetStorage().read('accessToken');

  Future<bool> checkConnection() async {
    dio.options.connectTimeout = 5000;

    var connResult = await (Connectivity().checkConnectivity());

    if (connResult == ConnectivityResult.mobile) {
      // logKey('Connect with mobile');
      return true;
    } else if (connResult == ConnectivityResult.wifi) {
      // logKey('Connect with wifi');
      return true;
    } else {
      return false;
    }
  }

  // var defaultHeader = {
  //   'content-type': 'application/json',
  // };

  // void checkToken() {
  //   if (GetStorage().read(LOGIN_BY) == 'google') {
  //     token = GetStorage().read(GOOGLE_TOKEN);
  //   } else {
  //     token = token;
  //   }
  // }

  Future<dynamic> get(
    String path, {
    Map<String, dynamic>? query,
    bool isToken = true,
  }) async {
    bool isConnect = await checkConnection();
    // checkToken();
    if (token != null) {}
    if (isConnect) {
      try {
        Response res = await dio.get(
          path,
          queryParameters: query,
          options: Options(
            contentType: 'application/json',
            headers: isToken ? {'Authorization': 'Bearer $token'} : null,
          ),
        );
        return res;
      } on DioError {
        rethrow;
      }
    }
  }

  Future<dynamic> post(
    String path, {
    required Map<String, dynamic> body,
    Map<String, dynamic>? query,
    bool isToken = true,
  }) async {
    bool isConenct = await checkConnection();
    // checkToken();
    if (token != null) {}
    if (isConenct) {
      logKey('token', token);
      try {
        Response res = await dio.post(
          path,
          data: body,
          options: Options(
            contentType: 'application/json',
            headers: isToken ? {'Authorization': 'Bearer $token'} : null,
          ),
        );
        return res;
      } on DioError {
        rethrow;
      }
    }
  }

  Future<dynamic> patch(
    String path, {
    required Map<String, dynamic> body,
    Map<String, dynamic>? query,
    bool isToken = true,
  }) async {
    bool isConenct = await checkConnection();
    // checkToken();
    if (token != null) {}
    if (isConenct) {
      logKey('token', token);
      try {
        Response res = await dio.patch(
          path,
          data: body,
          options: Options(
            contentType: 'application/json',
            headers: isToken ? {'Authorization': 'Bearer $token'} : null,
          ),
        );
        return res;
      } on DioError {
        rethrow;
      }
    }
  }

  Future<dynamic> delete(
    String path, {
    Map<String, dynamic>? body,
    Map<String, dynamic>? query,
    bool isToken = true,
  }) async {
    bool isConenct = await checkConnection();
    // checkToken();
    if (token != null) {}
    if (isConenct) {
      try {
        Response res = await dio.delete(
          path,
          data: body,
          options: Options(
            contentType: 'application/json',
            headers: isToken ? {'Authorization': 'Bearer $token'} : null,
          ),
        );
        return res;
      } on DioError {
        rethrow;
      }
    }
  }

  Future<dynamic> postFormData(
    String path, {
    FormData? formData,
    Map<String, dynamic>? query,
    bool isToken = true,
  }) async {
    bool isConenct = await checkConnection();
    // checkToken();
    if (token != null) {}
    if (isConenct) {
      try {
        // log(token);
        Response res = await dio.patch(
          path,
          data: formData,
          options: Options(
            contentType: 'application/json',
            headers: isToken ? {'Authorization': 'Bearer $token'} : null,
          ),
        );
        return res;
      } on DioError {
        rethrow;
      }
    }
  }
}
