import 'dart:convert';
import 'dart:developer' as dev;
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:codingtest/app/config/constants.dart';
import 'package:codingtest/app/model/canvas_model/draw_point.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:quiver/iterables.dart' as quiver;

Widget loading({double? size}) {
  return Center(
    child: Column(
      mainAxisSize: MainAxisSize.min,
      // crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: (size ?? 50) * 2,
          width: (size ?? 50) * 2,
          // color: kBgWhite,
          child: Center(
            child: SpinKitFoldingCube(
              size: size ?? 50,
              color: kBgBlack,
            ),
          ),
        ),
      ],
    ),
  );
}

void dialogLoading({double? size}) {
  Get.dialog(
    loading(size: size),
    barrierDismissible: false,
  );
}

Matrix4 translate(Offset translation) {
  var dx = translation.dx;
  var dy = translation.dy;

  //  ..[0]  = 1       # x scale
  //  ..[5]  = 1       # y scale
  //  ..[10] = 1       # diagonal "one"
  //  ..[12] = dx      # x translation
  //  ..[13] = dy      # y translation
  //  ..[15] = 1       # diagonal "one"
  return Matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, dx, dy, 0, 1);
}

Matrix4 scale(double scale, Offset focalPoint) {
  var dx = (1 - scale) * focalPoint.dx;
  var dy = (1 - scale) * focalPoint.dy;

  //  ..[0]  = scale   # x scale
  //  ..[5]  = scale   # y scale
  //  ..[10] = 1       # diagonal "one"
  //  ..[12] = dx      # x translation
  //  ..[13] = dy      # y translation
  //  ..[15] = 1       # diagonal "one"
  return Matrix4(scale, 0, 0, 0, 0, scale, 0, 0, 0, 0, 1, 0, dx, dy, 0, 1);
}

Matrix4 rotate(double angle, Offset focalPoint) {
  var c = cos(angle);
  var s = sin(angle);
  var dx = (1 - c) * focalPoint.dx + s * focalPoint.dy;
  var dy = (1 - c) * focalPoint.dy - s * focalPoint.dx;

  //  ..[0]  = c       # x scale
  //  ..[1]  = s       # y skew
  //  ..[4]  = -s      # x skew
  //  ..[5]  = c       # y scale
  //  ..[10] = 1       # diagonal "one"
  //  ..[12] = dx      # x translation
  //  ..[13] = dy      # y translation
  //  ..[15] = 1       # diagonal "one"
  return Matrix4(c, s, 0, 0, -s, c, 0, 0, 0, 0, 1, 0, dx, dy, 0, 1);
}

bool isEmpty(dynamic val) {
  return [
    "",
    " ",
    null,
    'null',
    '{}',
    '[]',
    '0',
    '0.0',
  ].contains(val.toString());
}

bool isNotEmpty(dynamic val) {
  return ![
    "",
    " ",
    null,
    'null',
    '{}',
    '[]',
    '0',
    '0.0',
    '0.00',
  ].contains(val.toString());
}

void logKey([key, content]) {
  String finalLog = '';
  dynamic tempContent = content ?? key;
  if (tempContent is Map || tempContent is List) {
    try {
      finalLog = json.encode(tempContent);
    } catch (e) {
      finalLog = tempContent.toString();
    }
  } else if (tempContent is String) {
    finalLog = tempContent;
  } else {
    finalLog = tempContent.toString();
  }

  if (content != null) {
    dev.log('$key => $finalLog');
  } else {
    dev.log(finalLog);
  }
}

void name(args) {
  Map b = {};
  Function(Map) a = ((p0) {});
  a(b);
}

typedef OnUpdate<T> = T Function(T oldValue, T newValue);

class ValueUpdater<T> {
  final OnUpdate<T> onUpdate;
  T value;

  ValueUpdater({
    required this.value,
    required this.onUpdate,
  });

  T update(T newValue) {
    T updated = onUpdate(value, newValue);
    value = newValue;
    return updated;
  }
}

void toJsonA(String arg) {}

//? untuk konversi string hex ke hexadesimal untuk color
Color hexToColor(String code) {
  return Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
}

void showToast(message, {bgColor, txtColor}) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: bgColor,
      // ?? kPrimaryColor,
      textColor: txtColor ?? Colors.white,
      fontSize: 12.0);
}

Map getSizeByKey(GlobalKey key) {
  final RenderBox a = key.currentContext!.findRenderObject()! as RenderBox;
  return {
    "height": a.size.height,
    "width": a.size.width,
  };
}

//? convert brush to image
Future<String> drawerToImage(List drawPoint,
    {required int width, required int height}) async {
  ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
  List<DrawPoint?> drawPoints = [];
  for (var i = 0; i < drawPoint.length; i++) {
    if (drawPoint[i] == null) {
      drawPoints.add(null);
    } else {
      DrawPoint? dp = DrawPoint.fromJson(drawPoint[i]);
      drawPoints.add(dp);
    }
    // _drawPoints.add();
    // logKey('$i', drawPoints[i]);
  }
  var paint = Paint();
  Canvas canvas = Canvas(pictureRecorder);
  for (var i = 0; i < drawPoints.length - 1; i++) {
    if (drawPoints[i] != null && drawPoints[i + 1] != null) {
      if (!drawPoints[i]!.isDraw) {
        canvas.drawLine(
          Offset(drawPoints[i]!.dx, drawPoints[i]!.dy),
          Offset(drawPoints[i + 1]!.dx, drawPoints[i + 1]!.dy),
          Paint()
            ..color = Colors.brown
            ..strokeWidth = drawPoints[i]!.stroke
            ..blendMode = BlendMode.clear,
        );
      } else {
        PaintingStyle style = PaintingStyle.fill;
        if (drawPoints[i]!.style == 'stroke') {
          style = PaintingStyle.stroke;
        }
        paint.blendMode = BlendMode.srcOver;
        paint.isAntiAlias = true;
        paint.style = style;
        paint.strokeWidth = drawPoints[i]!.stroke;
        paint.color = Color(drawPoints[i]!.color);
        Path path = Path();
        path.lineTo(drawPoints[i]!.dx, drawPoints[i + 1]!.dx);

        canvas.drawLine(
          Offset(drawPoints[i]!.dx, drawPoints[i]!.dy),
          Offset(drawPoints[i + 1]!.dx, drawPoints[i + 1]!.dy),
          paint,
        );

        if (drawPoints[i]!.stroke >= 3) {
          canvas.drawCircle(Offset(drawPoints[i]!.dx, drawPoints[i]!.dy),
              (drawPoints[i]!.stroke / 20), paint);
        } else if (drawPoints[i]!.stroke >= 10) {
          canvas.drawCircle(Offset(drawPoints[i]!.dx, drawPoints[i]!.dy),
              (drawPoints[i]!.stroke / 30), paint);
        }
      }
    }
  }
  final picture = pictureRecorder.endRecording();
  // final img = await picture.toImage(Get.width.ceil(), Get.height.ceil());
  // final img = await picture.toImage(1920, 1080);
  // final img = await picture.toImage(1920, 1080);
  final img = await picture.toImage(width, height);
  final pngBytes = await img.toByteData(format: ui.ImageByteFormat.png);
  // final pngBytes = await img.toByteData(format: ImageByteFormat.rawStraightRgba);
  final base64 = base64Encode(Uint8List.view(pngBytes!.buffer));
  return base64;
}

//? Fungsi untuk ambil gambar dari kamera.
//? Return null jika permission not granted / user tidak jadi mengambil gambar
Future<File?> getImageCamera() async {
  var status = await Permission.camera.status;
  if (!status.isGranted) {
    var newStatus = await Permission.camera.request();
    if (!newStatus.isGranted) {
      return null;
    }
  }
  var img = await ImagePicker().pickImage(
    source: ImageSource.camera,
  );
  if (img == null) {
    return null;
  }
  return File(img.path);
}

double calcPercentage({required double a, required double b}) {
  return a / b * 100;
}

double calcProgression({required double a, required double b}) {
  return a / b;
}

num getMin(List<num> list) {
  num min = quiver.min(list)!;
  return min;
}

num getMax(List<num> list) {
  num max = quiver.max(list)!;
  return max;
}

List<int> extractUint8List(Uint8List source) {
  List<int> extractedList = [];
  for (int x in source) {
    extractedList.add(x);
  }
  return extractedList;
}

// // Get the proportionate height as per screen size
// double getProportionateScreenHeight(double inputHeight) {
//   double screenHeight = SizeConfig.screenHeight;
//   // 896 is the layout height that designer use
//   // or you can say iPhone 11
//   return (inputHeight / 896.0) * screenHeight;
// }

// // Get the proportionate height as per screen size
// double getProportionateScreenWidth(double inputWidth) {
//   double screenWidth = SizeConfig.screenWidth;
//   // 414 is the layout width that designer use
//   return (inputWidth / 414.0) * screenWidth;
// }

// class SizeConfig {
//   static MediaQueryData _mediaQueryData;
//   static double screenWidth;
//   static double screenHeight;
//   static double defaultSize;
//   static Orientation orientation;

//   void init(BuildContext context) {
//     _mediaQueryData = MediaQuery.of(context);
//     screenWidth = _mediaQueryData.size.width;
//     screenHeight = _mediaQueryData.size.height;
//     orientation = _mediaQueryData.orientation;
//   }
// }

class CountDownTimer extends StatefulWidget {
  const CountDownTimer({
    Key? key,
    required int secondsRemaining,
    this.countDownTimerStyle,
    required this.whenTimeExpires,
    this.countDownFormatter,
  })  : secondsRemaining = secondsRemaining,
        super(key: key);

  final int secondsRemaining;
  final Function whenTimeExpires;
  final Function? countDownFormatter;
  final TextStyle? countDownTimerStyle;

  @override
  State createState() => _CountDownTimerState();
}

class _CountDownTimerState extends State<CountDownTimer>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  Duration duration = const Duration(seconds: 0);

  String? get timerDisplayString {
    Duration duration = _controller.duration! * _controller.value;
    if (widget.countDownFormatter != null) {
      return widget.countDownFormatter!(duration.inSeconds);
    } else {
      return formatHHMMSS(duration.inSeconds);
    }
  }

  @override
  void initState() {
    super.initState();
    duration = Duration(seconds: widget.secondsRemaining);
    _controller = AnimationController(
      vsync: this,
      duration: duration,
    );
    _controller.reverse(from: widget.secondsRemaining.toDouble());
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed ||
          status == AnimationStatus.dismissed) {
        widget.whenTimeExpires();
      }
    });
  }

  @override
  void didUpdateWidget(CountDownTimer oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.secondsRemaining != oldWidget.secondsRemaining) {
      setState(() {
        duration = Duration(seconds: widget.secondsRemaining);
        _controller.dispose();
        _controller = AnimationController(
          vsync: this,
          duration: duration,
        );
        _controller.reverse(from: widget.secondsRemaining.toDouble());
        _controller.addStatusListener((status) {
          if (status == AnimationStatus.completed) {
            widget.whenTimeExpires();
          } else if (status == AnimationStatus.dismissed) {}
        });
      });
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: AnimatedBuilder(
            animation: _controller,
            builder: (_, Widget? child) {
              return Text(
                timerDisplayString!,
                style: widget.countDownTimerStyle,
                textScaleFactor: 1.0,
              );
            }));
  }
}

String formatHHMMSS(int seconds) {
  int hours = (seconds / 3600).truncate();
  seconds = (seconds % 3600).truncate();
  int minutes = (seconds / 60).truncate();

  String hoursStr = (hours).toString().padLeft(2, '0');
  String minutesStr = (minutes).toString().padLeft(2, '0');
  String secondsStr = (seconds % 60).toString().padLeft(2, '0');

  if (hours == 0) {
    return "$secondsStr seconds";
  }
  return secondsStr;
}
