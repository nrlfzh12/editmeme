import 'package:dio/dio.dart';

class DioUtil {
  static Dio _instance = Dio();
  String token = '';

  static Dio getInstance() {
    _instance ??= createDioInstance();
    return _instance;
  }

  static Dio createDioInstance() {
    var dio = Dio();
    dio.interceptors.clear();
    dio.interceptors.add(InterceptorsWrapper(onRequest: (options, handler) {
      return handler.next(options);
    }, onResponse: (response, handler) {
      if (response != null) {
        return handler.next(response);
      } else {
        return;
      }
    }, onError: (DioError e, handler) async {
      if (e.response != null) {
        if (e.response?.statusCode == 403) {
          RequestOptions requestOptions = e.requestOptions;
          // await refreshToken();
          // var accessToken = await GetStorage().read('accessToken');
          final opts = Options(method: requestOptions.method);
          // dio.options.headers["Authorization"] = "Bearer " + accessToken;
          dio.options.headers["Accept"] = "*/*";
          final response = await dio.request(requestOptions.path,
              options: opts,
              cancelToken: requestOptions.cancelToken,
              onReceiveProgress: requestOptions.onReceiveProgress,
              data: requestOptions.data,
              queryParameters: requestOptions.queryParameters);
          handler.resolve(response);
        } else {
          handler.next(e);
        }
        handler.next(e);
      }
    }));
    return dio;
  }

  // static refreshToken() async {
  //   Response response;
  //   // Repository repository = Repository();
  //   var dio = Dio();
  //   final Uri apiUrl = Uri.parse(baseUrl + "/auth/refresh");
  //   var refreshToken = GetStorage().read('refreshToken');
  //   dio.options.headers["Authorization"] = "Bearer " + refreshToken;
  //   try {
  //     response = await dio.postUri(apiUrl);
  //     if (response.statusCode == 200) {
  //       RefreshTokenModel refreshTokenModel =
  //           RefreshTokenModel.fromJson(jsonDecode(response.toString()));
  //       var box = GetStorage();
  //       box.write('accessToken', refreshTokenModel.accessToken);
  //       box.write('rfreshToken', refreshTokenModel.refreshToken);
  //     } else {
  //       print(response.toString()); //TODO: logout
  //     }
  //   } catch (e) {
  //     print(e.toString()); //TODO: logout
  //   }
  // }
}
