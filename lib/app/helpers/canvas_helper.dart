class CanvasItemType {
  CanvasItemType._();
  static const String IMAGE = 'image';
  static const String ART = 'art';
  static const String GIF = 'gif';
  static const String TEXT = 'text';
  static const String BACKGROUND = 'background';
  static const String BRUSH = 'brush';
  static const String BRUSH_BASE = 'brush_base';

  // static String helper(String value) {
  //   if (value == IMAGE) {
  //     return 'image';
  //   }
  //   if (value == ART) {
  //     return 'art';
  //   }
  //   if (value == GIF) {
  //     return 'gif';
  //   }
  //   if (value == TEXT) {
  //     return 'text';
  //   }
  //   if (value == BACKGROUND) {
  //     return 'background';
  //   }
  //   if (value == BRUSH) {
  //     return 'brush';
  //   }
  //   if (value == BRUSH_BASE) {
  //     return 'brush base';
  //   }
  //   return '-';
  // }
}
