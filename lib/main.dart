import 'package:codingtest/app/config/constants.dart';
import 'package:codingtest/app/modules/home/controllers/home_controller.dart';
import 'package:codingtest/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController(), fenix: true);
    // Get.lazyPut<SearchController>(() => SearchController(), fenix: true);
    // Get.lazyPut<ProfileController>(() => ProfileController(), fenix: true);
    // Get.lazyPut<SettingProfileController>(() => SettingProfileController(),
    //     fenix: true);
    // Get.lazyPut<ChangePasswordController>(() => ChangePasswordController(),
    //     fenix: true);
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await dotenv.load(fileName: ".env");
  // await Firebase.initializeApp();
  AppBinding().dependencies();

  // await GetStorage.init();
  runApp(
    GetMaterialApp(
      title: "CodingTest",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      debugShowCheckedModeBanner: false,
      // initialBinding: GlobalBinding(),
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          backgroundColor: kBgWhite,
        ),
        inputDecorationTheme: const InputDecorationTheme(
            focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.black))),
      ),
    ),
  );
}
